library("readr")
library("tximport")
library("DESeq2")
library("pcaExplorer")
library("dplyr")
setwd("/Users/paulinoramirez/proj/MDA_mouse")
dir <- '/Users/paulinoramirez/proj/MDA_mouse/mapped'
dir_info <- '~/proj/MDA_mouse/sample_info'
samples <- read.csv(file.path(dir_info,"rtg_Sample_table_6month.csv"), header=TRUE)

files <- file.path(dir,samples$File_Name,"quant.sf");files
names(files) <- samples$File_Name
##
all(file.exists(files))

t2g <- read.csv(file.path(dir_info,"t2g.csv"))
txi <- tximport(files, type="salmon", tx2gene=t2g)

library("DESeq2")
ddsTxi <- DESeqDataSetFromTximport(txi,
                                   colData = samples,
                                   design = ~ Tg)

#Prefiltering for low count genes
keep <- rowSums(counts(ddsTxi)) >= 10
ddsTxi <- ddsTxi[keep,]
## Run Deseq Analysis
ddsTxi <- DESeq(ddsTxi)
res <- results(ddsTxi)
res
##Make table for Results
res<- results(ddsTxi,contrast=c("Tg", "Tg4510", "nTg_4510"));head(res)
## order table by adjusted P value
res_Ordered <- res[order(res$pvalue),];res_Ordered
##Generate a quick summary
summary(res_Ordered)
sum(res_Ordered$padj < 0.05, na.rm=TRUE)
##
res_Ordered 
##
DEG_output <- as.data.frame(res_Ordered );DEG_output  <- tibble::rownames_to_column(DEG_output)
normalizedcounts <- as.data.frame(counts(ddsTxi, normalized=TRUE));normalizedcounts  <- tibble::rownames_to_column(normalizedcounts)
DEG_count_ouput <- left_join(DEG_output, normalizedcounts)

##load TE info
G2N <- read.csv(file.path(dir_info,"Mm_only_Repbase_info.csv"), header= FALSE);colnames(G2N) <- c("rowname","Family","Organism")
DEG_count_ouput_annotated <- left_join(DEG_count_ouput,G2N)








## generate csv file
write.csv(DEG_count_ouput_annotated,"DEG_output_rtg4510_6month_TE.csv")


##DATA exploration
plotMA(res, ylim=c(-2,2))

resOrdered <- res[order(res$padj),]
resOrdered <- cbind(as.matrix(res_Ordered),counts[rownames(res_Ordered),])

##
ntd <- normTransform(ddsTxi)
rld <- rlogTransformation(ddsTxi)

##PCA EXPLORER 
pcaExplorer(dds = ddsTxi,
            dst = rld)


## Look at transformed data


raw_counts <- counts(ddsTxi)

write.csv(raw_counts,"rTG-9month-Salmon_counts.csv")
