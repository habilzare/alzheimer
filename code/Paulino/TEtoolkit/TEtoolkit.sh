#!/bin/bash                                                                                                                                                                         
#SBATCH -J TE_toolkit            # job name                                                                                                                                         
#SBATCH -o TE_toolkit.o%j        # output and error file name (%j expands to jobID)                                                                                                 
#SBATCH -N 1                # number of nodes requested                                                                                                                             
#SBATCH -n 1             # total number of mpi tasks requested                                                                                                                     
#SBATCH -p normal      # queue (partition) -- normal, development, etc.                                                                                                             
#SBATCH -t 48:00:00         # run time (hh:mm:ss) - 1.5 hours                                                                                                                       
#SBATCH -A Genetwork        # Allocation name (req'd if you have more than 1


## this is an example script for running TE tool kit on six month rtg4510 fastq files


/work/06308/pramirez/lonestar/tools/tetoolkit-2.0.3/bin/TEtranscripts \
    -t R547-181-1-Tg6_S62_L006_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    R547-183-3-Tg6_S83_L007_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    R547-184-4-Tg6_S82_L007_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    R547-192-12-Tg6_S74_L007_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    R547-193-13-Tg6_S73_L007_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    R547-195-15-Tg6_S93_L008_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    -c R547-186-6-Tg6_S80_L007_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    R547-189-9-Tg6_S77_L007_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    R547-191-11-Tg6_S75_L007_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    R547-196-16-Tg6_S92_L008_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    R547-197-17-Tg6_S91_L008_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    R547-198-18-Tg6_S90_L008_sortmerna_trimmomaticAligned.sortedByCoord.out.bam \
    --GTF /work/06308/pramirez/lonestar/tools/STAR-2.7.1a/genome_data/gencode.vM10.annotation.gtf \
    --TE /work/06308/pramirez/lonestar/tools/tetoolkit-2.0.3/TE_gtf/mm10_rmsk_TE.gtf \
    --sortByPos \
    --project TE_toolkit_rtg6
    
    
