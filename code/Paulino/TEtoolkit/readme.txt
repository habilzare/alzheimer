0.5. To clean fastq reads, the pipeline in alzheimers/code/Habil/ was used.

1. STAR-mm10-index.sh
Used to create an index for the star alignment tool.

2. STAR-allign_rtg_TE-toolkit
Used to allign cleaned fastq reads to the mm10 genome.


3. TEtoolkit.sh
Used to calculate count against Transposons on STAR output bam files.

4. Deseq2_TEtoolkit.R
used to calculate the differential expression betweeen Tau and control samples.