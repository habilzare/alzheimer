#!/bin/bash                                                                                                                                                                         
#SBATCH -J star            # job name                                                                                                                                               
#SBATCH -o star.o%j        # output and error file name (%j expands to jobID)                                                                                                       
#SBATCH -N 1                # number of nodes requested                                                                                                                             
#SBATCH -n 1             # total number of mpi tasks requested                                                                                                                      
#SBATCH -p normal      # queue (partition) -- normal, development, etc.                                                                                                        
#SBATCH -t 05:00:00         # run time (hh:mm:ss) - 1.5 hours                                                                                                                       
#SBATCH -A Genetwork        # Allocation name (req'd if you have more than 1)                                                                                                        





pwd
date

$threads = '1'


/work/06308/pramirez/lonestar/tools/STAR-2.7.1a/bin/Linux_x86_64_static/STAR \
    --runThreadN 1 \
    --runMode genomeGenerate \
    --genomeSAindexNbases 13 \
    --genomeDir /work/06308/pramirez/lonestar/tools/STAR-2.7.1a/genomeDir/m10/ \
    --genomeFastaFiles /work/06308/pramirez/lonestar/tools/STAR-2.7.1a/genome_data/GRCm38.p4.genome.fa \
    --sjdbGTFfile /work/06308/pramirez/lonestar/tools/STAR-2.7.1a/genome_data/gencode.vM10.annotation.gtf \
