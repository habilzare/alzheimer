
##This code was used to calculate the DE transposons. The input is the .cntTable that was ouputed from TEtool kit.

data <- read.table("TE_toolkit_rtg6.cntTable",header=T,row.names=1)
groups <- factor(c(rep("TGroup",6),rep("CGroup",6)))
min_read <- 1
data <- data[apply(data,1,function(x){max(x)}) > min_read,]
sampleInfo <- data.frame(groups,row.names=colnames(data))
library(DESeq2, quietly=T)
dds <- DESeqDataSetFromMatrix(countData = data, colData = sampleInfo, design = ~ groups)
dds$condition = relevel(dds$groups,"CGroup")
dds <- DESeq(dds)
res <- results(dds,independentFiltering=F)
write.table(res, file="TE_toolkit_rtg6_gene_TE_analysis.txt", sep="\t",quote=F)
resSig <- res[(!is.na(res$padj) & (res$padj < 0.050000) & (abs(res$log2FoldChange)> 0.000000)), ]
write.table(resSig, file="TE_toolkit_rtg6_sigdiff_gene_TE.txt",sep="\t", quote=F)