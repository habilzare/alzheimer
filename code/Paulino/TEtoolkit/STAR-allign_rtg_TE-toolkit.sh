#!/bin/bash                                                                                                                                                                      \                                                                                                                                                                            
#SBATCH -J star1            # job name                                                                                                                                            \
                                                                                                                                                                                 
#SBATCH -o star.o%j        # output and error file name (%j expands to jobID)                                                                                                    \
                                                                                                                                                                                  
#SBATCH -N 1                # number of nodes requested                                                                                                                          \
                                                                                                                                                                                  
#SBATCH -n 12             # total number of mpi tasks requested                                                                                                                  \
                                                                                                                                                                                  
#SBATCH -p normal      # queue (partition) -- normal, development, etc.                                                                                                          \
                                                                                                                                                                                  
#SBATCH -t 48:00:00         # run time (hh:mm:ss) - 1.5 hours                                                                                                                    \
                                                                                                                                                                                  
#SBATCH -A Genetwork        # Allocation name (req'd if you have more than 1)                                                                                                    \
                                                                                                                                                                                  
                                                                                                                                                                                 \





for i in *_R1.fastq.gz; do
/work/06308/pramirez/lonestar/tools/STAR-2.7.1a/bin/Linux_x86_64_static/STAR --runMode alignReads \
    --readFilesCommand zcat \
    --outSAMtype BAM SortedByCoordinate \
    --outSAMattributes All \
    --genomeDir /work/06308/pramirez/lonestar/tools/STAR-2.7.1a/genomeDir/m10/ \
    --readFilesIn $i ${i%_R1.fastq.gz}_R2.fastq.gz \
    --runThreadN 12 \
    --limitBAMsortRAM 3457848646 \
    --outFilterMultimapNmax 100 \
    --winAnchorMultimapNmax 100 \
--outFileNamePrefix ${i%_R1.fastq.gz}
done