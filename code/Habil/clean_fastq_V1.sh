#!/bin/bash
usage()
{
cat << EOF
usage: $0 [options] <path/to/fastq/folder> <basename>

-----------------------------
Habil Zare wrote this script based on wgs.sh to clean raw fastq files 
by a) removing rRNAs, and b) removing adapters and low quality bps.
INPUT:
The base name is the file name from the begining upto ?R*.fastq where ? can be "_" or "."
OUTPUT:
The cleaned files will  be saved in "cleaned" subdirectory
FastQC plots will be saved in qc subfolder under directories 
that are named according to each cleaning step.

 -----------------------------

OPTIONS:
   -c      Clean the intermediate files after saving the cleaded fastq files. Set to false to disable using '-c false'.
   -s      Single-end reads. By default it is false assuming the reads are paired-end. Set to true otherwise using '-s true'.
   -r 	   Use '-r true' to remove the original fastq files after cleaning is done. The default is false. Be CAREFUL! Use this option ONLY if you have a backup from the original data, otherwise important data will be lost.
   -e 	   Use '-e <fasta-file>' to extract the reads that can be mapped to the sequences in the <fasta-file> separately. Useful for publishing the raw files alongside the paper. See the 2018/06/01 not in Habil's lab notebook, in particular, the index must be <fasta-file>.index . By default, it is set to false to disable.
   -l 	   The minimum length cutoff (i.e., the MINLEN paramter used by trimmomatic).  
   -t 	   The number of threads. If not provided, the total number of threads on the machine minus 1
will be automatically used. 
   -b  	   Set to true (default) to remove the ribosomal RNA (rRNA) using sortmerna. This is needed
when analyzing RNA, but not DNA, data. To significantly reduce cleaning time when analyzing DNA
data, you can disable this step using '-b false'.
EOF
}

## On the cluster, you may need to load the module.
## module load fastqc

## Check the command:
if [ "$#" -le 2 ]; then
   usage    
   exit 64
fi
## Habil adopted the exit codes from:
##https://pocoproject.org/docs/Poco.Util.Application.html#16218.

## Default arguments:
DOCLEAN=true
SINGLEND=false
REMOVEORIGINAL=false
EXTRACT=false
MINLEN=50
THREADS=$(lscpu -p | grep -c "^[0-9]")
THREADS=$((THREADS-1))
DERIB=true ## Remove ribosomal rRNA

## Reading the input:
while getopts ":c:s:r:h:e:l:t:b:" OPT; do
    case "$OPT" in
	c) DOCLEAN=$OPTARG;;
	s) SINGLEND=$OPTARG;;
	r) REMOVEORIGINAL=$OPTARG;;
	e) EXTRACT=$OPTARG;;
	l) MINLEN=$OPTARG;;
	t) THREADS=$OPTARG;;
	b) DERIB=$OPTARG;;
	h) usage;;
	*) usage;; 
    esac
done
shift "$((OPTIND-1))"

## Timing:
echo "Clean_fastq.sh started at:"
echo $(date)

echo "Cleaning:"
BASENAME=$2
echo $BASENAME
echo "Number of threads:"
echo $THREADS
echo "Clean (i.e., delete) intermediate files?"
echo $DOCLEAN
echo "Single-end reads:"
echo $SINGLEND
echo "Will the original fastq files be removed after cleaning?"
echo $REMOVEORIGINAL
echo "Extract the reads that can be mapped to the sequences in?"
echo $EXTRACT
echo "Remove rRNA?"
echo $DERIB
echo "Minimum length:"
echo $MINLEN


## Single- or paied-end?
function adjustEnd()
{
    if [ "$SINGLEND" = true ]; then
	echo $1
    else
	echo $1  "${1//R1/R2}"
    fi
}
## If SINGLEND is true, in the first one R1 is replaced with R2
## and the results will be added with a space in between.
## E.g., blaR1 -> blaR1 blaR2
## Usage: $(adjustEnd bla_R1)
echo $(adjustEnd ${BASENAME}_R1)
## NOT used yet!

## Path:
cd $1
echo "I am at:"
pwd 
##QC: Does a fastq file exist?
echo "I see and will clean these files:"
ls ${BASENAME}?R*.fastq
if [ $(find .  -name "$BASENAME?R*.fastq"|wc -l) -lt 1 ]; then
    echo "No ${BASENAME}?R*.fastq file here!"
    exit 66
fi

## Fastqc on raw:
mkdir -p qc/raw
echo $(date)
fastqc -t $THREADS -o qc/raw ${BASENAME}?R*.fastq
## ? can be "_" or "."

## For easy cleanup:
mkdir cleaned
mkdir intermediate
mkdir extracted
cd intermediate

SORTEDBASENAME=$BASENAME
if [ $DERIB = true ]; then
    echo $(date)
    echo "Removing rRNA ..."
    SORTEDBASENAME=${SORTEDBASENAME}_sortmerna
    if [ $SINGLEND = false ]; then
    ## sortmerna needs the two files to be merged. 
	echo "Merging..."
	merge-paired-reads.sh ../${BASENAME}_R1_*.fastq ../${BASENAME}_R2_*.fastq ${BASENAME}.merged.fastq
	## rRNA databases
	## SORTMERNAREFS should be defined in ~/Install/sortmerna-2.1-mac-64-multithread/sortmernarefs.sh,
	## which is sourced by ~./bashrc_profile 
	echo "sortmerna..."
	sortmerna --ref $SORTMERNAREFS --reads ${BASENAME}.merged.fastq --paired_in -a $THREADS --log --fastx --aligned ${BASENAME}_rRNA --other ${SORTEDBASENAME}
	echo "Separating rRNAs is done."
	##
	echo "Unmerging..."
	unmerge-paired-reads.sh ${SORTEDBASENAME}.fastq ${SORTEDBASENAME}.R1.fastq ${SORTEDBASENAME}.R2.fastq 
	if [ ! "$EXTRACT" = false ]; then
	    echo "Extracting part of data (TEs or piRNA reads) for publishing alongside the Nature Neuroscience paper:"
	    EXTRACTMIDFIX=$(basename $EXTRACT)
	    echo "The extracted reads will be saved in the extracted folder as:"
	    echo ${BASENAME}_${EXTRACTMIDFIX}*.fastq
	    sortmerna --ref $EXTRACT,$EXTRACT.index --reads ${BASENAME}.merged.fastq --paired_in -a $THREADS --log --fastx --aligned ${BASENAME}_${EXTRACTMIDFIX} --other ${BASENAME}_other
	    unmerge-paired-reads.sh ${BASENAME}_${EXTRACTMIDFIX}.fastq ../extracted/${BASENAME}_${EXTRACTMIDFIX}.R1.fastq ../extracted/${BASENAME}_${EXTRACTMIDFIX}.R2.fastq 
	fi
	##
    else ## single-end
	sortmerna --ref $SORTMERNAREFS --reads ../${BASENAME}?R1*.fastq -a $THREADS --log --fastx --aligned ${BASENAME}_rRNA --other ${SORTEDBASENAME}.R1
	##sortmerna --ref $SORTMERNAREFS --reads ../${BASENAME}?R1.fastq -a $THREADS --log --fastx --aligned ${BASENAME}_rRNA --other ${SORTEDBASENAME}.R1
	if [ ! "$EXTRACT" = false ]; then
	    echo "Extracting part of small RNA data (piRNA reads) for publishing alongside the Nature Neuroscience paper:"
	    EXTRACTMIDFIX=$(basename $EXTRACT)
	    sortmerna --ref $EXTRACT,$EXTRACT.index --reads ../${BASENAME}?R1.fastq -a $THREADS --log --fastx --aligned ../extracted/${BASENAME}_${EXTRACTMIDFIX} --other ${BASENAME}_other
	fi
    fi
    ## Fastqc, rRNA excluded:
    mkdir -p ../qc/sortmerna
    fastqc -t $THREADS -o ../qc/sortmerna ${SORTEDBASENAME}.R*.fastq
    ## rRNA removed.
    FOLDER=.
else
    FOLDER=.. 
fi


## Trimming
FASTQFILE1=$(ls ${FOLDER}/${SORTEDBASENAME}*R1*.fastq)
FASTQFILE2=$(ls ${FOLDER}/${SORTEDBASENAME}*R2*.fastq)
TRIMMOMATICDRI=`which trimmomatic|xargs awk '{print $3}'|xargs dirname`
trimmomaticCommandPaired="trimmomatic PE -threads $THREADS $FASTQFILE1 $FASTQFILE2  ../cleaned/${SORTEDBASENAME}_trimmomatic_R1.fastq ${SORTEDBASENAME}_trimmomatic_unpaired_R1.fastq ../cleaned/${SORTEDBASENAME}_trimmomatic_R2.fastq ${SORTEDBASENAME}_trimmomatic_unpaired_R2.fastq ILLUMINACLIP:${TRIMMOMATICDRI}/adapters/TruSeq3-PE-2.fa:3:30:10 SLIDINGWINDOW:5:20 MINLEN:${MINLEN}"

trimmomaticCommandSingle="trimmomatic SE -threads $THREADS ${FOLDER}/${SORTEDBASENAME}.R1.fastq  ../cleaned/${SORTEDBASENAME}_trimmomatic_R1.fastq ILLUMINACLIP:${TRIMMOMATICDRI}/adapters/TruSeq3-SE.fa:3:30:10 SLIDINGWINDOW:5:20 MINLEN:${MINLEN}"

echo $(date)
echo "Trimming using:"
if [ "$SINGLEND" = true ]; then
    ##echo "Wanring: minimum length is set to 15, OK for small RNA but too low for mRNA!"
    echo $trimmomaticCommandSingle
    eval $trimmomaticCommandSingle
else
    echo $trimmomaticCommandPaired
    eval $trimmomaticCommandPaired
fi


## Fastqc, trimmed:
mkdir -p ../qc/trimmomatic
echo $(date)
fastqc -t $THREADS -o ../qc/trimmomatic ../cleaned/${SORTEDBASENAME}_trimmomatic_R*.fastq

## Cleanup:
## Only if sortmerna_trimmomatic.R*.fastq (or trimmomatic.R*.fastq) files exist.
cd ..
if ! [ -f cleaned/${SORTEDBASENAME}_trimmomatic_R1.fastq ] ; then
      echo "Cleaning was NOT complete."
else
    if ! [ -f cleaned/${SORTEDBASENAME}_trimmomatic_R2.fastq ] && ! [ "$SINGLEND" = true ] ; then
	echo "Cleaning was NOT complete."
    else
	if [ "$DOCLEAN" = true ]; then
	    echo "Cleaning up the intermediate files..."
	    rm -r intermediate/${BASENAME}*.fastq
	fi
	if $REMOVEORIGINAL ; then
	    echo "Removing the original fastq files..."
	    rm -r  ${BASENAME}?R*.fastq
	fi
    fi
fi

## End time:
echo "Clean_fastq.sh finished at:"
echo $(date)







