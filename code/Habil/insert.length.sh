#!/bin/bash
usage()
{
cat << EOF
usage: $0  <basename> 

-----------------------------
Habil wrote this script to compute the insert lenght for the data that we submitted to GEO to be published alongside the Nature Neuroscience 2018 paper. This script should be run on the samples one-by-one (not in parallel).

INPUT:
The base name is the prefix of the file name that is stored at ~/proj/alzheimer/data/Drosophila_2016-09-15/RNA/extracted/, e.g., CTRL_1 or Tau_3
OUTPUT:
A <basename>_insert.txt file will  be saved in the folder if picard is used. Otherwise, results will be printed as standard output.
-----------------------------

Following the instructions in http://www.navinlab.com/seq/seq/variant_detection.html, Habil indexed the fasta file on 2018/06/07 as follows:

habil@habilub:~/proj/alzheimer/data/Drosophila_2016-09-15/RNA/fasta$ bwa index dmel-all-transposon-r6.12_longer19.fasta 

EOF
}

## End time:
echo "insert.length.sh started at:"
echo $(date)

BASENAME=$1
cd ~/proj/alzheimer/data/Drosophila_2016-09-15/RNA/extracted/
echo "Working on: ${BASENAME}..."
FASTQ1="${BASENAME}_dmel-all-transposon-r6.12_longer19.fasta.R1.fastq.gz"
ls $FASTQ1
FASTQ2="${BASENAME}_dmel-all-transposon-r6.12_longer19.fasta.R2.fastq.gz"
ls $FASTQ2
FASTAFILE="/home/habil/proj/alzheimer/data/Drosophila_2016-09-15/RNA/fasta/dmel-all-transposon-r6.12_longer19.fasta"
ls $FASTAFILE

## Picard does not work for all samples due to the following warning, which Habil could not fix:
##WARNING	2018-06-08 11:08:14	CollectInsertSizeMetrics	All data categories were discarded because they contained < 0.05 of the total aligned paired data.
if [ usePicard = 'true' ]; then   
    echo "Aligning:"
    bwa aln $FASTAFILE $FASTQ1 > ~/temp/R1.sai
    bwa aln $FASTAFILE $FASTQ2 > ~/temp/R2.sai
    
    bwa sampe $FASTAFILE ~/temp/R1.sai ~/temp/R2.sai  "$FASTQ1" "$FASTQ2" > ~/temp/"$BASENAME".sam

    echo "CollectInsertSizeMetrics:"
    picard CollectInsertSizeMetrics I=~/temp/"$BASENAME".sam O="$BASENAME"_insert.txt H="$BASENAME"_insert.pdf
fi

## On 2018/06/08, Habil downloaded the estimate-insert-sizes script from:
##https://gist.github.com/rchikhi/7281991
~/Install/other/estimate-insert-sizes $FASTAFILE $FASTQ1 $FASTQ2 


## End time:
echo "insert.length.sh finished at:"
echo $(date)







