#!/bin/bash
## Habil wrote this script to index the all rRNA databases available in sortmerna folder.

SORTMERNADIR=$(readlink -f $(which sortmerna))
SORTME_DATA_DIR=$(dirname $(dirname $SORTMERNADIR))/data
RRNADIR=$SORTME_DATA_DIR/rRNA_databases
IDX_DIR=$SORTME_DATA_DIR/idx
if [ ! -d "$RRNADIR" ]; then
  echo "Where did you install sortmerna? Consider updating the above path accordingly."
  exit 1
fi
RRNAFASTAS=`find $RRNADIR -name "*.fasta"`
SORTMERNAREFS="--idx-dir $IDX_DIR"
for db in $RRNAFASTAS
do
echo $db
SORTMERNAREFS="$SORTMERNAREFS --ref $db"
done

## Output:
echo Use the SORTMERNAREFS as the argument for sortmerna, which is equal to:
echo $SORTMERNAREFS
SORTMERNAREFSFILE=`dirname $SORTMERNADIR`/sortmernarefs.sh
echo "Saved in:"
echo $SORTMERNAREFSFILE
echo "export SORTMERNAREFS="$SORTMERNAREFS > $SORTMERNAREFSFILE
