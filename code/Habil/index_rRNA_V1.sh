#!/bin/bash
## Habil wrote this script to index the all rRNA databases available in sortmerna folder.

SORTMERNADIR=`which sortmerna`
RRNADIR=`dirname $SORTMERNADIR`/rRNA_databases
if [ ! -d "$RRNADIR" ]; then
  echo "Where did you install sortmerna? Consider updating  the above path accordingly."
  exit 1
fi
RRNAFASTAS=`find $RRNADIR -name "*.fasta"`
SORTMERNAREFS=""
for db in $RRNAFASTAS
do
echo $db
SORTMERNAREFS=$SORTMERNAREFS":"$db,$db.index
indexdb_rna --ref $db,$db.index 
done

SORTMERNAREFS="${SORTMERNAREFS:1:${#SORTMERNAREFS}-1}"

## Output:
echo Use the SORTMERNAREFS as the --ref argument for sortmerna, which is equal to:
echo $SORTMERNAREFS
SORTMERNAREFSFILE=`dirname $SORTMERNADIR`/sortmernarefs.sh
echo "Saved in:"
echo $SORTMERNAREFSFILE
echo "export SORTMERNAREFS="$SORTMERNAREFS > $SORTMERNAREFSFILE







