The scripts should run in the following order on the Lonestar cluster: 

-1- Habil wrote wgs.sh script for practicing. Not needed.

0- index_rRNA.sh should be run to index the all rRNA databases available in sortmerna folder.

0.5- transfer.R:
For Mayo Clinic human AD data, open  a screen on Lonestar, update
the settings.R script and then source transfer.R.
organize.mouse.R:
For MD Anderson mouse tau data, run this script on Lonestar to create symbolic links to the relevant
fastq files.

0.7- Uncompress all files in the data folder using: gunzip -kf *.fastq.gz

1- clean_FastqS.R, which uses clean.fastq.files.R and settings.R and clean_template.mpi to create a
clean*.mpi for each sample. Also, it submits one job per sample. If the raw data are in bam files,
they are first automatically converted to fastq files using SamToFastq.

2- clean_*.mpi will call clean_fastq.sh, which removes the rRNAs, adaptors, low-quality reads, etc.

2.5- Index the trascripts as explained in Habil's lano on 2016/9/22. For esiRNA,
first run unique.fasta.R before this step.

3- map.R uses settings.R and map_template.mpi to create one mpi script per sample. It also, submits
the mapping job to the cluster.

3.5- After mapping is done and jobs finished, use genetwok/code/Habil/AML/read.sailfish() to
compute and save the counts as explained on 2016/9/22 in Habil's lano. Copy the counts, which are
saved in the mapped/quant.sf_NumReads.RData file, to the local computer for fruther DE analysis.

3.6- Get the Mayo Clinic human metadata using getClinical.R. It needs
sailfish matrix to filter out the samples that are not used in the TE paper. 

4- hits.R; some of the isoforms of DE gene are under expressed while some of the other isoforms of
the same DE genes are over expressed. We call these opposite transcripts. This script identifies the
opposites. Bad naming for the script.

5- network.R should be run after the DE analysis to perform network analysis and compare the results.

6- copia.R script determines that if we pull all members of the copia family of transposable
elements together, it is over expressed, as a family, in tau.

7- insert.length.sh is used to compute the insert lenght for the data that we submitted to GEO to be
published alongside the Nature Neuroscience paper.
