#!/bin/bash

usage()
{
cat << EOF
usage: $0 [options] <path/to/fastq/folder> <BASE_NAME>
You need to have SORTMERNAREFS in your environment.
-----------------------------
Mohsen S. Tabar wrote this script, improving the one written by Habil.
INPUT:
The base name is the file name from the beginning upto ?R*.fastq where ? can be "_" or "."
OUTPUT:
The cleaned files will be saved in "cleaned" subdirectory
-----------------------------
OPTIONS:
    -c <true|false>    (default true) Clean the intermediate files after saving the cleaned fastq files. Set to false to disable using '-c false'.
    -s <true|false>	   (default false) Single-end reads. By default the reads are paired-end.
    -r <true|false>    (default false) Removes the original fastq files after cleaning is done. Be CAREFUL! Use this option ONLY if you have a backup from the original data, otherwise important data will be lost.
    -b <true|false>    (default true) Removes the ribosomal RNA (rRNA) using sortmerna. This is needed when analyzing RNA, but not DNA, data. To significantly reduce cleaning time when analyzing DNA data, you can disable this step using '-b false'.
    -e <fasta-file>    Extracts the reads that can be mapped to the sequences in the <fasta-file> separately. Useful for publishing the raw files alongside the paper. See the 2018/06/01 not in Habil's lab notebook, in particular, the index must be <fasta-file>.index.
    -l <min-length>    (default 50) The minimum length cutoff (i.e., the MIN_LEN parameter used by trimmomatic).
    -o <directory>     (default \`source directory\`) The output directory (relative to the source dir, except when you start it with /).
    -t <num-threads>   The number of threads.
EOF
}

## Check the command:
if [ "$#" -le 2 ]; then
    usage
    exit 64
fi
## Habil adopted the exit codes from:
##https://pocoproject.org/docs/Poco.Util.Application.html#16218.

## Default arguments:
DO_CLEAN=true
DE_RIB=true
SINGLE_END=false
REMOVE_ORIGINAL=false
EXTRACT=false
MIN_LEN=50
OUTPUT_DIR="."

## Reading the input:
while getopts ":c:s:r:h:e:l:t:b:o:" OPT; do
    case "$OPT" in
    c) DO_CLEAN=$OPTARG;;
    s) SINGLE_END=$OPTARG;;
    r) REMOVE_ORIGINAL=$OPTARG;;
    e) EXTRACT=$OPTARG;;
    l) MIN_LEN=$OPTARG;;
    t) THREADS=$OPTARG;;
    b) DE_RIB=$OPTARG;;
    o) OUTPUT_DIR=$OPTARG;;
    h) usage;;
    *) usage;; 
    esac
done
shift "$((OPTIND-1))"

## Timing:
echo "Clean_fastq.sh started at: $(date)"

echo "Number of threads: $THREADS"
echo "Clean (i.e., delete) intermediate files? $DO_CLEAN"
echo "Single-end reads: $SINGLE_END"
echo "Will the original fastq files be removed after cleaning? $REMOVE_ORIGINAL"
echo "Extract the reads that can be mapped to the sequences in? $EXTRACT"
echo "Remove rRNA? $DE_RIB"
echo "Minimum length: $MIN_LEN"
echo "Output dir: $OUTPUT_DIR"

## single- or paired-end?
function adjustEnd()
{
    if [ "$SINGLE_END" = true ]; then
        echo "$1"
    else
        echo "$1" "${1//R1/R2}"
    fi
}
## If SINGLE_END is true, in the first one R1 is replaced with R2
## and the results will be added with a space in between.
## E.g., bla_R1 -> bla_R1 bla_R2
## Usage: $(adjustEnd bla_R1)

function createDirIfNotExist()
{
    [ -d "$1" ] || mkdir "$1"
}

## Path:
cd "$1" || (echo "The specified fastq directory does not exist!"; exit 1)
echo "I am at: $(pwd)"
BASE_NAME=$2

## ? can be "_" or "."
FASTQ_PATTERN="$BASE_NAME?R*.fastq*"

## QC: Does a fastq file exist?
if [ "$(find . -name "$FASTQ_PATTERN" | wc -l)" -lt 1 ]; then
    echo "No $FASTQ_PATTERN file here!"
    exit 66
fi

echo "I see and will clean these files:"
# shellcheck disable=SC2086
## No double quote, because we have a pattern here
ls $FASTQ_PATTERN

CLEANED_OUT_DIR="$OUTPUT_DIR/cleaned"
createDirIfNotExist "$CLEANED_OUT_DIR"

if [ ! "$EXTRACT" = "false" ]; then
    createDirIfNotExist extracted
fi

if [ "$DE_RIB" = "true" ]; then
    echo "Removing rRNA ..., started at: $(date)"
    SORTED_BASE_NAME=${BASE_NAME}_sortmerna
    SORTED_EXTENSION="fq.gz"
    SORTED_DIR="$OUTPUT_DIR/intermediate"
    createDirIfNotExist "$SORTED_DIR"
    SORT_WORK_DIR=$(mktemp -d)
    if [ "$SINGLE_END" = "true" ]; then
        # shellcheck disable=SC2086
        ## No double quote for $SORTMERNAREFS
        sortmerna --workdir "$SORT_WORK_DIR" $SORTMERNAREFS --reads "$BASE_NAME"?R1*.fastq* --threads "$THREADS" --fastx --aligned "$SORTED_DIR/${BASE_NAME}_rRNA" --other "$SORTED_DIR/$SORTED_BASE_NAME.R1"
        if [ ! "$EXTRACT" = false ]; then
            echo "Extracting part of small RNA data (piRNA reads) for publishing alongside the Nature Neuroscience paper:"
            EXTRACT_MID_FIX=$(basename "$EXTRACT")
            rm -rf "$SORT_WORK_DIR/kvdb"
            sortmerna --workdir "$SORT_WORK_DIR" --ref "$EXTRACT" --reads "$BASE_NAME"?R1.fastq* -threads "$THREADS" --fastx --aligned "$SORTED_DIR/extracted/${BASE_NAME}_${EXTRACT_MID_FIX}" --other "$SORTED_DIR/${BASE_NAME}_other"
        fi
    else ## paired-end
        ## We rule out both reads in other in case even one of them aligned by sortmerna, with the option --paired_in
        # shellcheck disable=SC2086
        ## No double quote for $SORTMERNAREFS
        sortmerna --workdir "$SORT_WORK_DIR" $SORTMERNAREFS --reads "$BASE_NAME"?R1*.fastq* --reads "$BASE_NAME"?R2*.fastq* --threads "$THREADS" --fastx --paired_in --aligned "$SORTED_DIR/${BASE_NAME}_rRNA" --other "$SORTED_DIR/$SORTED_BASE_NAME"
        ## Now we de-interleave the generated fastq file
        ## which is inspired by this link: https://gist.github.com/nathanhaigh/3521724
        gzip -c -d "$SORTED_DIR/$SORTED_BASE_NAME.$SORTED_EXTENSION" | \
            paste - - - - - - - -  | \
            tee >(cut -f 1-4 | tr "\t" "\n" | gzip > "$SORTED_DIR/$SORTED_BASE_NAME.R1.$SORTED_EXTENSION") | \
            cut -f 5-8 | tr "\t" "\n" | gzip > "$SORTED_DIR/$SORTED_BASE_NAME.R2.$SORTED_EXTENSION"
        rm "$SORTED_DIR/$SORTED_BASE_NAME.$SORTED_EXTENSION"
        if [ ! "$EXTRACT" = false ]; then
            echo "Extracting part of data (TEs or piRNA reads) for publishing alongside the Nature Neuroscience paper:"
            EXTRACT_MID_FIX=$(basename "$EXTRACT")
            echo "The extracted reads will be saved in the extracted folder as: ${BASE_NAME}_${EXTRACT_MID_FIX}*.fastq"
            rm -rf "$SORT_WORK_DIR/kvdb"
            sortmerna --workdir "$SORT_WORK_DIR" --ref "$EXTRACT" --reads "$BASE_NAME"?R1*.fastq* --reads "$BASE_NAME"?R2*.fastq* --threads "$THREADS" --fastx --paired_in --aligned "$SORTED_DIR/extracted/${BASE_NAME}_${EXTRACT_MID_FIX}" --other "$SORTED_DIR/${BASE_NAME}_other"
        fi
    fi
    rm -rf "$SORT_WORK_DIR"
    ## rRNA removed.
else
    SORTED_BASE_NAME=$(basename "$BASE_NAME")
    SORTED_EXTENSION="fastq*"
    SORTED_DIR=$(dirname "$BASE_NAME")
fi

## Trimming
echo "Trimming started at: $(date)"
FASTQ_FILE_R1=$(find "$SORTED_DIR" -name "$SORTED_BASE_NAME?R1*.$SORTED_EXTENSION" -exec basename {} \;)
FASTQ_FILE_R2=$(find "$SORTED_DIR" -name "$SORTED_BASE_NAME?R2*.$SORTED_EXTENSION" -exec basename {} \;)

# shellcheck disable=SC2016
## No double quoting, because we are dealing with awk command.
TRIMMOMATIC_DIR=$(which trimmomatic | xargs awk '{print $3}' | xargs dirname)

if [ "$SINGLE_END" = "true" ]; then
    SE_OR_PE="SE"
    TRU_SEQ_NAME="TruSeq3-SE.fa"
    TRIM_INPUTS=("$SORTED_DIR/$FASTQ_FILE_R1")
    TRIM_OUTPUTS=("$CLEANED_OUT_DIR/${SORTED_BASE_NAME}_trimmomatic_R1.fastq.gz")
else
    mkdir -p "$SORTED_DIR/unpaired"
    SE_OR_PE="PE"
    TRU_SEQ_NAME="TruSeq3-PE-2.fa"
    TRIM_INPUTS=("$SORTED_DIR/$FASTQ_FILE_R1" "$SORTED_DIR/$FASTQ_FILE_R2")
    TRIM_OUTPUTS=("$CLEANED_OUT_DIR/${SORTED_BASE_NAME}_trimmomatic_R1.fastq.gz" "$SORTED_DIR/unpaired/${SORTED_BASE_NAME}_trimmomatic_unpaired_R1.fastq.gz" "$CLEANED_OUT_DIR/${SORTED_BASE_NAME}_trimmomatic_R2.fastq.gz" "$SORTED_DIR/unpaired/${SORTED_BASE_NAME}_trimmomatic_unpaired_R2.fastq.gz")
fi

ILUMINA_PARAM="ILLUMINACLIP:$TRIMMOMATIC_DIR/adapters/$TRU_SEQ_NAME:3:30:10"
trimmomatic "$SE_OR_PE" -threads "$THREADS" "${TRIM_INPUTS[@]}" "${TRIM_OUTPUTS[@]}" "$ILUMINA_PARAM" "SLIDINGWINDOW:5:20" "MINLEN:$MIN_LEN"

## Cleanup:
## Only if sortmerna_trimmomatic.R*.fastq (or trimmomatic.R*.fastq) files exist.
if ! [ -f "$CLEANED_OUT_DIR/${SORTED_BASE_NAME}_trimmomatic_R1.fastq.gz" ] ; then
    echo "Cleaning was NOT complete."
else
    if ! [ -f "$CLEANED_OUT_DIR/${SORTED_BASE_NAME}_trimmomatic_R2.fastq.gz" ] && ! [ "$SINGLE_END" = true ] ; then
        echo "Cleaning was NOT complete."
    else
        if [ "$DO_CLEAN" = true ]; then
            echo "Cleaning up the intermediate files..."
            rm -r "$OUTPUT_DIR/intermediate/$SORTED_BASE_NAME"*."$SORTED_EXTENSION"
        fi
        if $REMOVE_ORIGINAL ; then
            echo "Removing the original fastq files..."
            # shellcheck disable=SC2086
            ## No double quote
            rm -r $FASTQ_PATTERN
        fi
    fi
fi

## End time:
echo "clean_fastq.sh finished at: $(date)"
