## Habil wrote this script to transfer Mayo Clinic data from a subfolder on Ranch to the scratch folder on Lonestar.
## This script should be run on a screen on the Lonestar cluster.
## If you have not already cloned the alzheimer repository on the $SCRATCH, do this to create the data folder.
## First update the settings.R, and then source this script.

## Settings:
library(synapseClient) ## Replaced with synapser? 2019-08-29, --Habil.
source("set.cluster.R")
source("settings.R")

## Bam files:
print(transferCommand)
dir.create(scratchPath, showWarnings=FALSE, recursive=TRUE)
startedTime <- Sys.time()
print("Transferring started at:")
print(startedTime)
system(transferCommand)
endTime <- Sys.time()
print("Transferring finished at:")
print(endTime)
print(endTime - startedTime)

## Exclude based on QC
print("Getting the QC data to exclude the low-quality samples... ")
synapseLogin('zare@txstate.edu','notValidAnyMore')
## Habil learned how to use the synGot object from the following:
## http://compbio.ucdenver.edu/Hunter_lab/Phang/resources/Teaching/BIOS6660/CS-6-Dream/DREAM_Lecture.html
## http://www.lincsproject.org/LINCS/tools/workflows/download-most-recent-data-from-mep-lincs
synGot <- synGet(id=synQcId)
toExclude  <- read.table(getFileLocation(synGot), sep="\t", header=TRUE)
toExcludeSamples <- as.character(toExclude[,"Sample.Name"])
print("The following low-quality samples should be excluded:")
print(toExcludeSamples)

## Exclude:
system(paste("mkdir", excludePath))
for(s1 in toExcludeSamples){
    f1 <- system(paste0("ls ",inPathRNA,"/",s1,"*"),intern=TRUE)
    print(paste("Moving:", f1))
    file.rename(from=f1, to=file.path(excludePath, basename(f1)))
}
print(paste(length(dir(path=inPathRNA,pattern="bam$")), "bam files are included."))

## Save:
save(toExclude,file=file.path(excludePath,"toExclude.RData"))






