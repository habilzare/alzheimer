## The esiRNA fasta file has multiple occurrence of the same sequence with different IDS (genomic locations).
## Habil wrote this script to create a fasta file of esiRNAs with unique sequences.
## The IDs corresponding to a sequence are separated with "," in the output fasta file.
##2017/07/19.

library(Biostrings)
source("set.cluster.R")
source("settings.R")

print(paste("Reading:",esiRNAMultipleFasaFile))
esi <- readDNAStringSet(esiRNAMultipleFasaFile)
idsAll <- names(esi)
sequencesAll <- paste(esi)
sequences <- unique(sequencesAll)
print(paste("Number of unique sequences:", length(sequences)))

print("Computing...")
for(i1 in 1:length(sequences)){
    s1 <- sequences[i1]
    inds <- which(sequencesAll==s1)
    names(sequences)[i1] <- paste(idsAll[inds],collapse=",")
}

print("Writing...")
writeXStringSet(x=DNAStringSet(sequences), filepath=esiRNAFastaFile)
print(paste("The fasta file is saved at:",esiRNAFastaFile))
