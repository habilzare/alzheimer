#!/bin/bash

usage()
{
cat << EOF
usage: $0 [options] <path/to/fastq/folder> <BASE_NAME>
-----------------------------
Mohsen S. Tabar wrote this script, to run fastqc in parallel.
OUTPUT:
FastQC plots will be saved in "qc" sub-folder under directories
that are named according to each cleaning step.
-----------------------------
OPTIONS:
    -s <true|false>   (default false) Single-end reads. By default the reads are paired-end.
    -b <true|false>   (default true) had sortmerna run. This is needed when analyzing RNA, but not DNA, data.
    -o <directory>    (default \`source directory\`) The output directory (relative to the source dir, except when you start it with /).
EOF
}

## On the cluster, you may need to load the module.
## module load fastqc

## Check the command:
if [ "$#" -le 1 ]; then
    usage
    exit 64
fi
## Habil adopted the exit codes from:
##https://pocoproject.org/docs/Poco.Util.Application.html#16218.

## Default arguments:
DE_RIB=true
SINGLE_END=false
OUTPUT_DIR="."

## Reading the input:
while getopts ":s:b:h:o:" OPT; do
    case "$OPT" in
    s) SINGLE_END=$OPTARG;;
    b) DE_RIB=$OPTARG;;
    o) OUTPUT_DIR=$OPTARG;;
    h) usage;;
    *) usage;; 
    esac
done
shift "$((OPTIND-1))"

if [ "$SINGLE_END" = true ]; then
    THREADS=1
else
    THREADS=2
fi

## single- or paired-end?
function adjustEnd()
{
    if [ "$SINGLE_END" = true ]; then
        echo "$1"
    else
        echo "$1" "${1//R1/R2}"
    fi
}
## If SINGLE_END is true, in the first one R1 is replaced with R2
## and the results will be added with a space in between.
## E.g., bla_R1 -> bla_R1 bla_R2
## Usage: $(adjustEnd bla_R1)

function createDirIfNotExist()
{
    [ -d "$1" ] || mkdir -p "$1"
}

BASE_NAME=$2
## Path:
cd "$1" || (echo "The specified fastq directory does not exist!"; exit 1)
echo "fastqc is at $(pwd)"

## ? can be "_" or "."
FASTQ_PATTERN="$BASE_NAME?R*.fastq*"

QC_OUT_DIR="$OUTPUT_DIR/qc"
createDirIfNotExist "$QC_OUT_DIR"

## fastqc on raw:
createDirIfNotExist "$QC_OUT_DIR/raw"
echo "Started fastqc on raw at: $(date)"
# shellcheck disable=SC2086
fastqc -t "$THREADS" -o "$QC_OUT_DIR/raw" $FASTQ_PATTERN
echo "Finished fastqc on raw at: $(date)"

if [ "$DE_RIB" = true ]; then
    ## fastqc, rRNA excluded:
    SORTED_BASE_NAME=${BASE_NAME}_sortmerna
    createDirIfNotExist "$QC_OUT_DIR/sortmerna"
    echo "Started fastqc on sortmerna at: $(date)"
    fastqc -t "$THREADS" -o "$QC_OUT_DIR/sortmerna" "$OUTPUT_DIR/intermediate/$SORTED_BASE_NAME".R*.fq.gz
    echo "Finished fastqc on sortmerna at: $(date)"
else
    SORTED_BASE_NAME=$BASE_NAME
fi

## fastqc, trimmed:
createDirIfNotExist "$QC_OUT_DIR/trimmomatic"
echo "Started fastqc on trimmed at: $(date)"
fastqc -t "$THREADS" -o "$QC_OUT_DIR/trimmomatic" "$OUTPUT_DIR/cleaned/${SORTED_BASE_NAME}"_trimmomatic_R*.fastq.gz
echo "Finished fastqc on trimmed at: $(date)"
