#!/bin/bash
## Habil wrote this script to follow the instructions by Bastian Schiffthaler 
## https://www.youtube.com/watch?v=1rNEkWSxB5s
## and clean the WGS fastq files.

## Path:
cd ~/temp/WGS10
echo "I am at:"
pwd 
##ls 
BASENAME=10.A.01_WGS.1M

## Fastqc on raw:
mkdir -p qc/raw
fastqc -t 8 -o qc/raw ${BASENAME}.R*.fastq

## For easy cleanup:
mkdir cleaned
mkdir intermediate
cd intermediate

## Remove rRNA
## sortmerna needs the two files to be merged.
echo "Merging..."
merge-paired-reads.sh ../${BASENAME}.R1.fastq ../${BASENAME}.R2.fastq ${BASENAME}.merged.fastq
## rRNA databases
## SORTMERNAREFS should be defined in ~/Install/sortmerna-2.1-mac-64-multithread/sortmernarefs.sh,
## which is sourced by ~./bashrc_profile 
## sortmerna
sortmerna --ref $SORTMERNAREFS --reads ${BASENAME}.merged.fastq --paired_in -a 8 --log --fastx --aligned ${BASENAME}_rRNA --other ${BASENAME}_sortmerna
echo "Separating rRNAs is done."

echo "Unmerging..."
unmerge-paired-reads.sh ${BASENAME}_sortmerna.fastq ${BASENAME}_sortmerna.R1.fastq ${BASENAME}_sortmerna.R2.fastq 

## Fastqc, rRNA excluded:
mkdir -p ../qc/sortmerna
fastqc -t 8 -o ../qc/sortmerna ${BASENAME}_sortmerna.R*.fastq

## Trimming
TRIMMOMATICDRI=`which trimmomatic|xargs awk '{print $3}'|xargs dirname`
trimmomatic PE -threads 8 ${BASENAME}_sortmerna.R1.fastq ${BASENAME}_sortmerna.R2.fastq  ../cleaned/${BASENAME}_sortmerna_trimmomatic.R1.fastq ${BASENAME}_sortmerna_trimmomatic_unpaired.R1.fastq  ../cleaned/${BASENAME}_sortmerna_trimmomatic.R2.fastq ${BASENAME}_sortmerna_trimmomatic_unpaired.R2.fastq ILLUMINACLIP:${TRIMMOMATICDRI}/adapters/TruSeq3-PE-2.fa:3:30:10 SLIDINGWINDOW:5:20 MINLEN:50

## Fastqc, trimmed:
mkdir -p ../qc/trimmomatic
fastqc -t 8 -o ../qc/trimmomatic ../cleaned/${BASENAME}_sortmerna_trimmomatic.R*.fastq


## Cleanup:
## Only if sortmerna_trimmomatic.R*.fastq files exist.
cd ..
if [ -f cleaned/${BASENAME}_sortmerna_trimmomatic.R1.fastq ] && [ -f cleaned/${BASENAME}_sortmerna_trimmomatic.R2.fastq ]; then
  echo "Cleaning up the intermediate files..."
  rm -r intermediate
fi




