#!/bin/zsh
## Mohsen wrote this script, 2023-06-16 to build and push oncinfo images.
## Pull alzheimer and genetwork repositories first.
## Your docker daemon must be running. In mac machines, you need to start your docker desktop app.
## It must be run from the current directory.
## After successful run and pushing, you may remove the local image from your computer
## in case you do not need it anymore. To do this, run a command like this:
## docker image rm habilzare/oncinfo:oncinfo-[TAG]
## replacing [TAG] with the actual tag you built.
## To run this scripts, one needs to create a context using such command first and once:
## docker buildx create --use --driver-opt env.BUILDKIT_STEP_LOG_MAX_SIZE=104857600
## This has the advantage over the regular docker build that
## we are not restricted to 2MB log limit.

PLATFORM="linux/x86_64"
REPO="oncinfo"
TAG="oncinfo-$1"
if [ -z "$#" ]; then
  echo "Must provide the tag as the first argument"
  exit 1
elif [ "$#" -eq 1 ]; then
  NUM_CPUS=8
else
  NUM_CPUS="$2"
fi

echo "Using $NUM_CPUS CPU(s) to build the final image"

## Since caLibraries does not include all dependencies,
## we first caLibraries and then export ALL installed packages.
## Call caLibraries in case there are missing packages in the local system.
Rscript -e "invisible(OncinfoUt::caLibraries(doCalling=FALSE))"
## Export list of installed packages in the local system to try to install them from binary in archlinux.
Rscript -e "write.table(paste0('r-', installed.packages()[,'Package']), file='./$REPO/required.txt', row.names=F, col.names=F, quote=F)"

## Github has a rate limit by default which sometimes prevent the packages to be downloaded.
## You can generate a key for yourself and use it as a build arg. You can use this link:
## https://github.com/settings/tokens/new?scopes=repo,user,gist,workflow
## and then run:
## GITHUB_PAT=your_generated_token ./build.sh ......
## PAT stands for "Personal Api Token"
if docker build --platform "$PLATFORM" \
  --build-arg GITHUB_PAT="$GITHUB_PAT" --build-arg NUM_CPUS="$NUM_CPUS" --build-arg CACHEBUST="$(date +%s)" \
  --file "./$REPO/$TAG.dockerfile" --tag "habilzare/$REPO:$TAG" --load "./$REPO"; then
    echo "pushing"
    docker push "habilzare/$REPO:$TAG"
fi
