#!/bin/zsh

## Build and copy OncinfoUt to the building context
docker run --platform "$PLATFORM" -v ~/proj:/root/proj habilzare/oncinfo:oncinfo-base-0.3 \
    Rscript -e 'source("/root/proj/alzheimer/code/utilities/makeOncinfoUt.R")'
cp -r ~/proj/alzheimer/code/utilities/OncinfoUt_*.tar.gz "./$REPO"

## Build and copy BiggSeq to the building context
## Mohsen 2023-06 is heavily developing BiggSeq_0.1.0, so remove the tar.gz file first.
rm -f ~/proj/alzheimer/code/utilities/BiggSeq_0.1.0.tar.gz
docker run --platform "$PLATFORM" -v ~/proj:/root/proj habilzare/oncinfo:oncinfo-base-0.3 \
    Rscript -e 'source("/root/proj/alzheimer/code/utilities/makeBiggSeq.R")'
cp -r ~/proj/alzheimer/code/utilities/BiggSeq_*.tar.gz "./$REPO"
