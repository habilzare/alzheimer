Habil, Mohsen, Shiva, 2023-06-19

=== What is Docker?
With Docker, we can package an application and its dependencies into an image. 
This image can then be deployed and run on any system that has Docker installed, 
regardless of the underlying operating system or infrastructure. 
Docker provides a lightweight and efficient way to distribute and run applications, 
as containers share the host system's kernel and resources while maintaining their own isolated filesystem.

See Mohsen’s slides on Docker and Singularity for more details.
https://docs.google.com/presentation/d/1TFjB16XmLk2Xo4qHJgGe1VfnIUonw9cIJmU1xCZxyL0/edit#slide=id.g242153ecf22_0_60

The docker files in this folder are used to make images we need to run experiments in the Oncinfo Lab.
We first create a "base-xx.yy" image which contains R and R packages we need,
together with the tools to analyse data, usually sequencing data, e.g. samtools.
Over this, we create a "xx.yy" image which adds python and the packages
required mostly for deep learning. e.g. pytorch.
WE FREEZE THESE IMAGES ONCE THEY ARE CREATED.
ANY CHANGES TO THEM MAY BREAK THE REPRODUCIBILITY OF THE RESULTS.
To develop an image, we first create a "xx.yy-dev" image which is basically the "xx.yy" image,
but with installing any other software or packages that are needed during our works.
Once we want to have published results, we increase the version
and create the next "base-zz.tt" and "zz.tt" images and FREEZE THEM.

********************************************************************************************************

=== How to use images on clusters (e.g. ls6)?
In order to use the images on a cluster, we use the singularity platform. Singularity supports a variety of container image formats, including Docker, so existing Docker images can be converted and used with Singularity. 
Steps on TACC clusters:

1) Do not load on the login node. Instead, request an interactive node (e.g. idev -t 2:00:00 ).

2) Load the singularity: module load tacc-apptainer

3) Login to Docker hub:  singularity remote login --username habilzare docker://docker.io 

4) Assuming you have already sourced 
/work/03270/zare/Install/oncinfo_var or /work/03270/zare/Install//work/03270/zare/Install/oncinfo_settings
which define ZARE_SING, pull an oncinfo tag from repository and save it in proper folder with a clear name using
<the tag>.sif format (e.g., oncinfo-0.2.sif). ALL the images should be pulled in $ZARE_SING:

singularity pull $ZARE_SING/oncinfo-0.2.sif docker://habilzare/oncinfo:oncinfo-0.2

5) Execute it either by starting a shell, which creates a container;

singularity shell $ZARE_SING/oncinfo-0.2.sif
 
or running a command, e.g. 

singularity exec --cleanenv $ZARE_SING/oncinfo-0.2.sif echo "Hi world!"

Another example to show R code can be run using Rscript:

singularity exec --cleanenv $ZARE_SING/oncinfo-0.2.sif Rscript -e '2+3'

************************************************************************

=== How to make and use an image?

1) Making Dockerfile.

The Dockerfile is a text file that contains a series of instructions for building the Docker image. 
The Dockerfile does not need to have a specific suffix. 
However, we use $TAG.dockerfile for ours. The work was started with this commit:
(https://bitbucket.org/habilzare/genetwork/commits/39aa5a7f16c1317ed885f0b8b37121fbc3dfe048)
and then we moved it here.

Example: Make a Dockerfile (e.g. touch ~/Dockerfile) that installs BiocManager package in R:

FROM r-base:4.3.0
RUN Rscript -e 'install.packages("BiocManager")'

2) Building and pushing the Dockerfile

Once you have created the Dockerfile, you need to build the Docker image as the following:
(It is actually easier and preferred to run the build.sh script in this folder. See below).

docker build -t habilzare/oncinfo:bioc-1.0.0 /path/to/Dockerfile

Next login to Docker:
docker login --username habilzare ##you’ll need a token

Push:
docker push habilzare/oncinfo:bioc-1.0.0

** Preferably, you can use build.sh in this folder to build and push one of the images to dockerhub,
i.e,

bash build.sh 0.2
to build and push oncinfo:oncinfo-0.2

3) Using the image with docker (see above for singularity):
docker run habilzare/oncinfo:bioc-1.0.0 Rscript -e 'library(BiocManager); BiocManager::version()'
