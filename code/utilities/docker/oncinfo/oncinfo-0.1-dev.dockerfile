## Mohsen started writing this, 2023-05-15.

FROM habilzare/oncinfo:oncinfo-0.1

## To add R packages to the image that would be common in lab projects,
## **first** add them to the defaults in proj/alzheimer/code/Habil/utilities/call.libraries.R,
## then install them here.

RUN Rscript -e 'for (nam in c("logger", "configr", "tictoc", "argparse")) { BiocManager::install(nam) }'
RUN Rscript -e 'for (nam in c("genomation", "MotifDb", "universalmotif", "memes")) { BiocManager::install(nam) }'
RUN Rscript -e 'BiocManager::install("DiffBind")'
RUN Rscript -e 'BiocManager::install("ggthemes")'
RUN Rscript -e 'BiocManager::install("EnsDb.Hsapiens.v75")'
RUN Rscript -e 'BiocManager::install("BSgenome.Hsapiens.UCSC.hg19")'
RUN Rscript -e 'BiocManager::install("biomaRt")'
## Mohsen chose danielcoral over FanLiuLab because of a bug regarding foreach in CGWAS.
RUN Rscript -e 'devtools::install_github("https://github.com/danielcoral/CGWAS")'

ENV PICARD_JAR=/opt/picard.jar

WORKDIR /opt

ENV HISAT2_VERIOSN=2.2.1
RUN wget https://cloud.biohpc.swmed.edu/index.php/s/oTtGWbWjaxsQ2Ho/download && \
    unzip download && \
    rm download && \
    ln -s "/opt/hisat2-$HISAT2_VERIOSN/hisat2" "/usr/local/bin/hisat2"

## This is to install gtc2vcf plugin for bcftools.
## Instructions are from https://raw.githubusercontent.com/freeseek/gtc2vcf
RUN wget "https://github.com/samtools/bcftools/releases/download/$BCFTOOLS_VERSION/bcftools-$BCFTOOLS_VERSION.tar.bz2" && \
    tar -xf "bcftools-$BCFTOOLS_VERSION.tar.bz2" && \
    rm "bcftools-$BCFTOOLS_VERSION.tar.bz2" && \
    cd "bcftools-$BCFTOOLS_VERSION" && \
    wget -P "plugins" https://raw.githubusercontent.com/freeseek/gtc2vcf/master/gtc2vcf.c && \
    wget -P "plugins" https://raw.githubusercontent.com/freeseek/gtc2vcf/master/gtc2vcf.h && \
    make && \
    cp bcftools plugins/gtc2vcf.so /usr/local/lib && \
    cd .. && rm -r "bcftools-$BCFTOOLS_VERSION"
ENV BCFTOOLS_PLUGINS=/usr/local/lib

## This tool is required to convert Illumina idat files to gtc files.
## Instructions are from https://raw.githubusercontent.com/freeseek/gtc2vcf
ENV IAAP_CLI_VERSION=1.1.0
RUN wget "ftp://webdata2:webdata2@ussd-ftp.illumina.com/downloads/software/iaap/iaap-cli-linux-x64-$IAAP_CLI_VERSION.tar.gz" && \
    tar -xf "iaap-cli-linux-x64-$IAAP_CLI_VERSION.tar.gz" && \
    rm "iaap-cli-linux-x64-$IAAP_CLI_VERSION.tar.gz" && \
    chmod -R 755 "/opt/iaap-cli-linux-x64-$IAAP_CLI_VERSION/iaap-cli" && \
    ln -s "/opt/iaap-cli-linux-x64-$IAAP_CLI_VERSION/iaap-cli/iaap-cli" "/usr/local/bin/iaap-cli"

ENV BWA_VERSION=0.7.12
RUN wget "https://github.com/lh3/bwa/archive/refs/tags/$BWA_VERSION.zip" && \
    unzip "$BWA_VERSION.zip" && \
    rm "$BWA_VERSION.zip" && \
    cd "bwa-$BWA_VERSION" && \
    make && mv bwa /usr/local/bin && \
    cd .. && rm -r "bwa-$BWA_VERSION"

## Habil sent an email with the subject 'Fwd: ANNOVAR download' to the lab members providing the download link
RUN wget "http://www.openbioinformatics.org/annovar/download/0wgxR2rIVP/annovar.latest.tar.gz" && \
    tar -xf annovar.latest.tar.gz && \
    rm annovar.latest.tar.gz && \
    mv annovar/*.pl /usr/local/bin && \
    rm -r annovar

## The following is to use this image as remote R.
## One needs to run the image with the following options:
## -d -t -p 22:22 -v $HOME/proj:/home/docker/proj bash -c 'service ssh start && sleep infinity'
RUN apt-get install -y ssh
RUN echo root:docker | chpasswd
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

WORKDIR /root

## Cleaning up some footprints of sortmerna index
RUN rm -r sortmerna

ENV BIGGSEQ_VERSION=0.1.0
COPY BiggSeq_$BIGGSEQ_VERSION.tar.gz /root
RUN bash -c 'R CMD INSTALL $1 && rm $1' _ BiggSeq_$BIGGSEQ_VERSION.tar.gz
## In case you have for example c code in your package,
## you have to copy the package folder and build it here before installing

## If you need to bypass the caching mechanism of docker for this part,
## use --build-arg CACHEBUST="$(date +%s)" in your docker build command.
## Keep in mind that this way, everything from here would be built again.
ARG CACHEBUST=1
