## Mohsen started writing this, 2024-07-05.

FROM habilzare/oncinfo:oncinfo-0.4

RUN pip install openai diffusers transformers sentencepiece

RUN Rscript -e 'install.packages("beepr")'

RUN --mount=target=OncinfoUt.tar.gz,source=OncinfoUt_0.8.16.tar.gz \
    R CMD INSTALL OncinfoUt.tar.gz

RUN pacman -Sy
RUN pacman -S --noconfirm r-anndata r-zellkonverter r-aroma.light r-microbenchmark r-nloptr

RUN Rscript -e 'OncinfoUt::caLibraries(c("FactoMineR", "monocle"), Ncpus=8)'

RUN Rscript -e 'install.packages("synapser", repos = c("http://ran.synapse.org", "http://cran.fhcrc.org"))'

RUN Rscript -e 'devtools::install_github("lhe17/coxmeg", Ncpus=8)'

RUN pip install snakemake torchaudio ffmpeg librosa datasets

RUN pacman -S --noconfirm openssh rsync

RUN Rscript -e 'OncinfoUt::caLibraries("ggsurvfit")'
RUN Rscript -e 'devtools::install_github("WenjianBi/SPACox")'
RUN Rscript -e 'OncinfoUt::caLibraries("gwasurvivr")'

## Mohsen tried to install METAL in this image. The tests were failed with this message:
#The following tests FAILED:
#	  4 - SAMPLESIZE_NOGC (Failed)
#	  5 - SAMPLESIZE_GC (Failed)
#	  6 - STDERR_NOGC (Failed)
#	  7 - STDERR_GC (Failed)
#	  8 - SAMPLESIZE_NOGC_HETEROGENEITY (Failed)
#	  9 - STDERR_NOGC_HETEROGENEITY (Failed)
#	 11 - TRACK_POSITIONS (Failed)
#	 12 - PRINT_PRECISION (Failed)

#RUN pacman -Sy
### Require to update gcc
#RUN pacman -S --noconfirm archlinux-keyring
#RUN pacman -S --noconfirm gcc-fortran cmake
#
#ENV METAL_VER=2020-05-05
#RUN wget https://github.com/statgen/METAL/archive/refs/tags/${METAL_VER}.tar.gz && \
#    tar -xzf ${METAL_VER}.tar.gz && \
#    rm ${METAL_VER}.tar.gz && \
#    cd METAL-${METAL_VER} && \
#    mkdir build && cd build && \
#    cmake -DCMAKE_BUILD_TYPE=Release .. && \
#    make && make install && \
#    mv bin/metal /usr/local/bin && \
#    cd ../.. && rm -r METAL-${METAL_VER}

RUN --mount=target=OncinfoUt.tar.gz,source=OncinfoUt_0.8.24.tar.gz \
    R CMD INSTALL OncinfoUt.tar.gz

RUN --mount=target=BiggSeq.tar.gz,source=BiggSeq_0.2.0.tar.gz \
    R CMD INSTALL BiggSeq.tar.gz
