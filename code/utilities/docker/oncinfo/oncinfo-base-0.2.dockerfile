## Mohsen wrote this, 2023-06-15.
## This image contains R packages and omic tools
## that we usually need in the Oncinfo lab.

FROM r-base:4.3.0

RUN apt-get update && apt-get install -y git && apt-get install -y python3.11
## required for softwares that use python instead of python3 (e.g. gatk)
RUN ln -s /usr/bin/python3 /usr/local/bin/python

## required for java-based softwares (e.g fastqc and trimmomatic)
RUN apt-get install -y openjdk-17-jre

## required for devtools package
RUN apt-get install -y \
    libfontconfig1-dev \
    libcurl4-openssl-dev \
    libxml2-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libssl-dev \
    libfreetype6-dev \
    libpng-dev \
    libtiff5-dev \
    libjpeg-dev

RUN Rscript -e 'install.packages("devtools")'

RUN Rscript -e 'devtools::install_version("BiocManager", version = "1.30.20")'
RUN Rscript -e 'BiocManager::install(version="3.17")'

RUN git clone https://habilzare@bitbucket.org/habilzare/alzheimer.git && \
    cd alzheimer && git checkout tags/oncinfo-base-0.2 && \
    Rscript -e 'source("code/Habil/utilities/call.libraries.R"); call.libraries()' && \
    cd .. && rm -r alzheimer

## required for building some softwares (e.g samtools, bcftools)
RUN apt-get install -y autoconf

## install softwares on /opt when not in /usr/local
WORKDIR /opt

ENV TRIMMOMATIC_VERSION=0.39
RUN wget "https://github.com/usadellab/Trimmomatic/files/5854859/Trimmomatic-$TRIMMOMATIC_VERSION.zip" && \
    unzip "Trimmomatic-$TRIMMOMATIC_VERSION.zip" && \
    rm "Trimmomatic-$TRIMMOMATIC_VERSION.zip" && \
    echo "java -jar /opt/Trimmomatic-$TRIMMOMATIC_VERSION/trimmomatic-$TRIMMOMATIC_VERSION.jar" ' $@' \
        > /usr/local/bin/trimmomatic && \
    chmod +x /usr/local/bin/trimmomatic

ENV FASTQC_VERSION=0.12.1
RUN wget "https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v$FASTQC_VERSION.zip" && \
    unzip "fastqc_v$FASTQC_VERSION.zip" && \
    rm "fastqc_v$FASTQC_VERSION.zip" && \
    ln -s /opt/FastQC/fastqc /usr/local/bin/fastqc

ENV SORTMERNA_VERSION=4.3.4
COPY --from=bschiffthaler/sortmerna:4.3.4 /usr/local/ /usr/local/

## prepare sortmerna refs (variable) and indices
RUN wget "https://github.com/sortmerna/sortmerna/archive/refs/tags/v$SORTMERNA_VERSION.zip" && \
    unzip "v$SORTMERNA_VERSION.zip" && \
    rm "v$SORTMERNA_VERSION.zip"
ENV BASH_ENV=/etc/profile
RUN echo 'export SORTMERNAREFS="--idx-dir /opt/sortmerna-$SORTMERNA_VERSION/data/idx $(find "/opt/sortmerna-$SORTMERNA_VERSION/data/rRNA_databases" -name "*.fasta" -exec printf " -ref '%s'" {} \;)"' >> /etc/profile
RUN ((touch empty.fq && . /etc/profile && sortmerna -reads empty.fq $SORTMERNAREFS) || true) && rm -r /root/sortmerna

ENV SALMON_VERSION=1.10.1
COPY --from=combinelab/salmon:1.10.1 /usr/local/ /usr/local/

ENV HISAT2_VERIOSN=2.2.1
RUN wget https://cloud.biohpc.swmed.edu/index.php/s/oTtGWbWjaxsQ2Ho/download && \
    unzip download && \
    rm download && \
    ln -s "/opt/hisat2-$HISAT2_VERIOSN/hisat2" "/usr/local/bin/hisat2"

ENV BEDTOOLS_VERSION=2.31.0
RUN wget "https://github.com/arq5x/bedtools2/releases/download/v$BEDTOOLS_VERSION/bedtools.static"
RUN mv bedtools.static /usr/local/bin/bedtools && chmod +x /usr/local/bin/bedtools

ENV PICARD_VERSION=3.0.0
ENV PICARD_JAR=/opt/picard.jar
RUN wget https://github.com/broadinstitute/picard/releases/download/$PICARD_VERSION/picard.jar
RUN echo 'java -jar /opt/picard.jar $@' > /usr/local/bin/picard && \
    chmod +x /usr/local/bin/picard

ENV GATK_VERSION=4.4.0.0
ENV GATK_EXEC_PATH=/usr/local/bin/gatk
RUN wget "https://github.com/broadinstitute/gatk/releases/download/$GATK_VERSION/gatk-$GATK_VERSION.zip" && \
    unzip "gatk-$GATK_VERSION.zip" && \
    rm "gatk-$GATK_VERSION.zip" && \
    ln -s /opt/gatk-$GATK_VERSION/gatk /usr/local/bin/gatk

ENV SAMTOOLS_VERSION=1.17
ENV BCFTOOLS_VERSION=$SAMTOOLS_VERSION
ENV HTSLIB_VERSION=$SAMTOOLS_VERSION
ENV BCFTOOLS_PLUGINS=/usr/local/lib
RUN wget "https://github.com/samtools/htslib/releases/download/$HTSLIB_VERSION/htslib-$HTSLIB_VERSION.tar.bz2" && \
    tar -xf "htslib-$HTSLIB_VERSION.tar.bz2" && \
    rm "htslib-$HTSLIB_VERSION.tar.bz2"
RUN wget "https://github.com/samtools/samtools/releases/download/$SAMTOOLS_VERSION/samtools-$SAMTOOLS_VERSION.tar.bz2" && \
    tar -xf "samtools-$SAMTOOLS_VERSION.tar.bz2" && \
    rm "samtools-$SAMTOOLS_VERSION.tar.bz2" && \
    cd "samtools-$SAMTOOLS_VERSION" && \
    autoheader && autoconf -Wno-syntax && \
    ./configure --prefix=/usr/local && make && make install && \
    cd .. && rm -r "samtools-$SAMTOOLS_VERSION"
RUN wget "https://github.com/samtools/bcftools/releases/download/$BCFTOOLS_VERSION/bcftools-$BCFTOOLS_VERSION.tar.bz2" && \
    tar -xf "bcftools-$BCFTOOLS_VERSION.tar.bz2" && \
    rm "bcftools-$BCFTOOLS_VERSION.tar.bz2" && \
    cd "bcftools-$BCFTOOLS_VERSION" && \
    wget -P "plugins" https://raw.githubusercontent.com/freeseek/gtc2vcf/master/gtc2vcf.c && \
    wget -P "plugins" https://raw.githubusercontent.com/freeseek/gtc2vcf/master/gtc2vcf.h && \
    autoheader && autoconf -Wno-syntax && \
    ./configure --prefix=/usr/local && make && make install && \
    cp bcftools plugins/gtc2vcf.so /usr/local/lib && \
    cd .. && rm -r "bcftools-$BCFTOOLS_VERSION"
RUN rm -r "htslib-$HTSLIB_VERSION"

## This tool is required to convert Illumina idat files to gtc files.
## Instructions are from https://raw.githubusercontent.com/freeseek/gtc2vcf
ENV IAAP_CLI_VERSION=1.1.0
RUN wget "ftp://webdata2:webdata2@ussd-ftp.illumina.com/downloads/software/iaap/iaap-cli-linux-x64-$IAAP_CLI_VERSION.tar.gz" && \
    tar -xf "iaap-cli-linux-x64-$IAAP_CLI_VERSION.tar.gz" && \
    rm "iaap-cli-linux-x64-$IAAP_CLI_VERSION.tar.gz" && \
    chmod -R 755 "/opt/iaap-cli-linux-x64-$IAAP_CLI_VERSION/iaap-cli" && \
    ln -s "/opt/iaap-cli-linux-x64-$IAAP_CLI_VERSION/iaap-cli/iaap-cli" "/usr/local/bin/iaap-cli"

ENV BWA_VERSION=0.7.12
RUN wget "https://github.com/lh3/bwa/archive/refs/tags/$BWA_VERSION.zip" && \
    unzip "$BWA_VERSION.zip" && \
    rm "$BWA_VERSION.zip" && \
    cd "bwa-$BWA_VERSION" && \
    make && mv bwa /usr/local/bin && \
    cd .. && rm -r "bwa-$BWA_VERSION"

## Habil sent an email with the subject 'Fwd: ANNOVAR download' to the lab members providing the download link
RUN wget "http://www.openbioinformatics.org/annovar/download/0wgxR2rIVP/annovar.latest.tar.gz" && \
    tar -xf annovar.latest.tar.gz && \
    rm annovar.latest.tar.gz && \
    mv annovar/*.pl /usr/local/bin && \
    rm -r annovar


## change directory to the home folder on the container
WORKDIR /root

ENV ONCINFOUT_VERSION=0.4.36
COPY OncinfoUt_$ONCINFOUT_VERSION.tar.gz .
RUN bash -c 'R CMD INSTALL $1 && rm $1' _ OncinfoUt_$ONCINFOUT_VERSION.tar.gz
## In case you have for example c code in your package,
## you have to copy the package folder and build it here before installing
