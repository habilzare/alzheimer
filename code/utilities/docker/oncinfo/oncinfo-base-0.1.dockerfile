## Mohsen wrote this, 2023-05-15.
## This image contains R packages and omic tools
## that we usually need in the Oncinfo lab.

FROM r-base:4.3.0

RUN apt-get update && apt-get install -y git && apt-get install -y python3.11
## required for softwares that use python instead of python3 (e.g. gatk)
RUN ln -s /usr/bin/python3 /usr/local/bin/python

## required for java-based softwares (e.g fastqc and trimmomatic)
RUN apt-get install -y openjdk-17-jre

## required for devtools package
RUN apt-get install -y \
    libfontconfig1-dev=2.14.1-4 \
    libcurl4-openssl-dev=7.88.1-9 \
    libxml2-dev=2.9.14+dfsg-1.2 \
    libharfbuzz-dev=6.0.0+dfsg-3 \
    libfribidi-dev=1.0.8-2.1 \
    libssl-dev=3.0.8-1 \
    libfreetype6-dev=2.12.1+dfsg-5 \
    libpng-dev=1.6.39-2 \
    libtiff5-dev=4.5.0-5 \
    libjpeg-dev=1:2.1.5-2

RUN Rscript -e 'install.packages("devtools")'

RUN Rscript -e 'devtools::install_version("BiocManager", version = "1.30.20")'
RUN Rscript -e 'BiocManager::install(version="3.17")'

RUN git clone https://habilzare@bitbucket.org/habilzare/alzheimer.git && \
        cd alzheimer && \
        git checkout tags/oncinfo-base-0.1 && \
        Rscript -e 'source("code/Habil/utilities/call.libraries.R"); call.libraries()' && \
        cd .. && \
        rm -r alzheimer

## required for building some softwares (e.g samtools, bcftools)
RUN apt-get install -y autoconf

## install softwares on /opt when not in /usr/local
WORKDIR /opt

ENV TRIMMOMATIC_VERSION=0.39
RUN wget "https://github.com/usadellab/Trimmomatic/files/5854859/Trimmomatic-$TRIMMOMATIC_VERSION.zip" && \
    unzip "Trimmomatic-$TRIMMOMATIC_VERSION.zip" && \
    rm "Trimmomatic-$TRIMMOMATIC_VERSION.zip" && \
    echo "java -jar /opt/Trimmomatic-$TRIMMOMATIC_VERSION/trimmomatic-$TRIMMOMATIC_VERSION.jar" ' $@' \
        > /usr/local/bin/trimmomatic && \
    chmod +x /usr/local/bin/trimmomatic

ENV FASTQC_VERSION=0.12.1
RUN wget "https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v$FASTQC_VERSION.zip" && \
    unzip "fastqc_v$FASTQC_VERSION.zip" && \
    rm "fastqc_v$FASTQC_VERSION.zip" && \
    ln -s /opt/FastQC/fastqc /usr/local/bin/fastqc

ENV SORTMERNA_VERSION=4.3.4
COPY --from=bschiffthaler/sortmerna:4.3.4 /usr/local/ /usr/local/

## prepare sortmerna refs (varriable) and indices
RUN wget "https://github.com/sortmerna/sortmerna/archive/refs/tags/v$SORTMERNA_VERSION.zip" && \
    unzip "v$SORTMERNA_VERSION.zip" && \
    rm "v$SORTMERNA_VERSION.zip"
ENV BASH_ENV=/etc/profile
RUN echo 'export SORTMERNAREFS="--idx-dir /opt/sortmerna-$SORTMERNA_VERSION/data/idx $(find "/opt/sortmerna-$SORTMERNA_VERSION/data/rRNA_databases" -name "*.fasta" -exec printf " -ref '%s'" {} \;)"' >> /etc/profile
RUN (touch empty.fq && . /etc/profile && sortmerna -reads empty.fq $SORTMERNAREFS) || true

ENV SALMON_VERSION=1.10.1
COPY --from=combinelab/salmon:1.10.1 /usr/local/ /usr/local/

ENV BEDTOOLS_VERSION=2.31.0
RUN wget "https://github.com/arq5x/bedtools2/releases/download/v$BEDTOOLS_VERSION/bedtools.static"
RUN mv bedtools.static /usr/local/bin/bedtools && chmod +x /usr/local/bin/bedtools

ENV PICARD_VERSION=3.0.0
RUN wget https://github.com/broadinstitute/picard/releases/download/$PICARD_VERSION/picard.jar
RUN echo 'java -jar /opt/picard.jar $@' > /usr/local/bin/picard && \
    chmod +x /usr/local/bin/picard

ENV GATK_VERSION=4.4.0.0
RUN wget "https://github.com/broadinstitute/gatk/releases/download/$GATK_VERSION/gatk-$GATK_VERSION.zip" && \
    unzip "gatk-$GATK_VERSION.zip" && \
    rm "gatk-$GATK_VERSION.zip" && \
    ln -s /opt/gatk-$GATK_VERSION/gatk /usr/local/bin/gatk

ENV SAMTOOLS_VERSION=1.17
ENV BCFTOOLS_VERSION=$SAMTOOLS_VERSION
ENV HTSLIB_VERSION=$SAMTOOLS_VERSION
RUN wget "https://github.com/samtools/htslib/releases/download/$HTSLIB_VERSION/htslib-$HTSLIB_VERSION.tar.bz2" && \
    tar -xf "htslib-$HTSLIB_VERSION.tar.bz2" && \
    rm "htslib-$HTSLIB_VERSION.tar.bz2"
RUN wget "https://github.com/samtools/samtools/releases/download/$SAMTOOLS_VERSION/samtools-$SAMTOOLS_VERSION.tar.bz2" && \
    tar -xf "samtools-$SAMTOOLS_VERSION.tar.bz2" && \
    rm "samtools-$SAMTOOLS_VERSION.tar.bz2" && \
    cd "samtools-$SAMTOOLS_VERSION" && \
    autoheader && autoconf -Wno-syntax && \
    ./configure --prefix=/usr/local && make && make install && \
    cd .. && rm -r "samtools-$SAMTOOLS_VERSION"
RUN wget "https://github.com/samtools/bcftools/releases/download/$BCFTOOLS_VERSION/bcftools-$BCFTOOLS_VERSION.tar.bz2" && \
    tar -xf "bcftools-$BCFTOOLS_VERSION.tar.bz2" && \
    rm "bcftools-$BCFTOOLS_VERSION.tar.bz2" && \
    cd "bcftools-$BCFTOOLS_VERSION" && \
    autoheader && autoconf -Wno-syntax && \
    ./configure --prefix=/usr/local && make && make install && \
    cd .. && rm -r "bcftools-$BCFTOOLS_VERSION"
RUN rm -r "htslib-$HTSLIB_VERSION"

## change directory to the home folder on the container
WORKDIR /root

RUN umask 007
