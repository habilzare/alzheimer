## Mohsen started writing this, 2023-06-15.

FROM habilzare/oncinfo:oncinfo-0.2

RUN apt-get update

RUN apt-get install -y tabix

WORKDIR /opt

## The following is to use this image as remote R.
## One needs to run the image with the following options:
## -d -t -p 22:22 -v $HOME/proj:/home/docker/proj bash -c 'service ssh start && sleep infinity'
RUN apt-get install -y ssh
RUN echo root:docker | chpasswd
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

WORKDIR /root

ENV ONCINFOUT_VERSION=0.5.44
COPY OncinfoUt_$ONCINFOUT_VERSION.tar.gz .
RUN bash -c 'R CMD INSTALL $1 && rm $1' _ OncinfoUt_$ONCINFOUT_VERSION.tar.gz
## In case you have for example c code in your package,
## you have to copy the package folder and build it here before installing

RUN Rscript -e 'OncinfoUt::call.libraries(packages=c("qqman", "sqldf", "CGWAS"))'
RUN Rscript -e 'OncinfoUt::call.libraries(packages=c("BiocParallel", "EnhancedVolcano"))'

ENV BIGGSEQ_VERSION=0.1.0
COPY BiggSeq_$BIGGSEQ_VERSION.tar.gz /root
RUN bash -c 'R CMD INSTALL $1 && rm $1' _ BiggSeq_$BIGGSEQ_VERSION.tar.gz
## In case you have for example c code in your package,
## you have to copy the package folder and build it here before installing

## If you need to bypass the caching mechanism of docker for this part,
## use --build-arg CACHEBUST="$(date +%s)" in your docker build command.
## Keep in mind that this way, everything from here would be built again.
ARG CACHEBUST=1
