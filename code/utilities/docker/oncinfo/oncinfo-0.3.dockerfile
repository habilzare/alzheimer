## Mohsen wrote this, 2023-10-18.
## Adding pytorch to the base image.

FROM python:3.11 AS builder
## cmake required by pyarrow
RUN cd /opt && python3 -m venv venv
ENV PATH="/opt/venv/bin"
RUN pip install --upgrade pip
RUN pip install numpy pandas scipy scikit-learn
RUN pip install torch
RUN pip install pytorch-ignite tensorboard torchinfo optuna
RUN pip install scanpy
RUN SKLEARN_ALLOW_DEPRECATED_SKLEARN_PACKAGE_INSTALL=True pip install pySummarizedExperiment
RUN pip install confuse dacite pyarrow
RUN pip install pyspark
RUN pip install pytorch_lightning


FROM habilzare/oncinfo:oncinfo-base-0.3

COPY --from=builder /opt/venv/ /opt/venv/
## This is necessary since in python image, we have python3 installed on /usr/local/bin, not /usr/bin.
RUN rm -r /opt/venv/bin/python3 && ln -s /usr/bin/python3 /opt/venv/bin/python3
ENV PATH="/opt/venv/bin:$PATH"
ENV TORCH_VENV_PATH="/opt/venv"

## Mohsen added this to solve the issue about the path in singularity when trying to use this venv.
## ToDo !!!!! This line is very dangerous and must be eliminated as soon as possible
RUN echo "export PATH=$PATH" > /etc/profile.d/path.sh
