## Mohsen wrote this, 2024-02-15.
## This image contains R packages and omic tools that we usually need in the Oncinfo lab.
## 'pacman' is the package manager for archlinux.
## Install softwares on /opt when not in /usr/local.

ARG NUM_CPUS
ARG GITHUB_PAT

ARG tag_bioarchlinux=20240215

ARG ONCINFOUT_VERSION=0.5.54

ARG R_VERSION=4.3.3
ARG PYTHON_VERSION=3.11.8
ARG JAVA_PACKAGE_TO_USE=jre17-openjdk

ARG TRIMMOMATIC_VERSION=0.39
ARG FASTQC_VERSION=0.12.1
ARG SORTMERNA_VERSION=4.3.4
ARG SALMON_VERSION=1.10.2
ARG HISAT2_VERIOSN=2.2.1
ARG BEDTOOLS_VERSION=2.31.0
ARG GATK_VERSION=4.5.0.0
ARG IAAP_CLI_VERSION=1.1.0
## Same versions for htslib, samtools, and bcftools
ARG HTSLIB_VERSION=1.19


FROM bioarchlinux/bioarchlinux:${tag_bioarchlinux} AS base

ARG R_VERSION
ARG PYTHON_VERSION
ARG JAVA_PACKAGE_TO_USE

ENV \
    ## Required for sortmerna
    BASH_ENV=/etc/profile \
    ## Required for bccftools plugins
    BCFTOOLS_PLUGINS=/usr/local/lib

## Add repositories for pacman
## "xtom" was the only repository of bioarchlinux in the US.
## "xtom" was not reliable as the repository of archlinux and I got some 404 errors for some packages.
RUN echo 'Server = https://arch.mirror.constant.com/$repo/os/$arch' > /etc/pacman.d/mirrorlist && \
    echo 'Server = https://mirrors.xtom.com/bioarchlinux/$arch' > /etc/pacman.d/mirrorlist.bio && \
    ## Fetch the list of packages from the repos
    pacman -Sy && \
    ## Enable parallel downloading
    sed -i 's/\#ParallelDownloads/ParallelDownloads/' /etc/pacman.conf

## Install wget, R, JAVA and python
## JAVA is required for java-based softwares (e.g fastqc and trimmomatic)
## Python is required for some tools (e.g. gatk) and be prepared for full oncinfo image based on this one
RUN pacman -S --noconfirm wget r=${R_VERSION} python=${PYTHON_VERSION} ${JAVA_PACKAGE_TO_USE}


FROM combinelab/salmon:${SALMON_VERSION} AS salmon-builder
RUN rm -r /usr/local/share/man /usr/local/man


FROM bschiffthaler/sortmerna:${SORTMERNA_VERSION} AS sortmerna-builder


FROM base as trimmomatic-builder
ARG TRIMMOMATIC_VERSION
RUN wget "https://github.com/usadellab/Trimmomatic/files/5854859/Trimmomatic-$TRIMMOMATIC_VERSION.zip" && \
    unzip "Trimmomatic-$TRIMMOMATIC_VERSION.zip" -d /opt && \
    echo "java -jar /opt/Trimmomatic-$TRIMMOMATIC_VERSION/trimmomatic-$TRIMMOMATIC_VERSION.jar" ' $@' \
        > /usr/local/bin/trimmomatic && \
    chmod +x /usr/local/bin/trimmomatic


FROM base as fastqc-builder
ARG FASTQC_VERSION
RUN wget "https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v$FASTQC_VERSION.zip" && \
    unzip "fastqc_v$FASTQC_VERSION.zip" -d /opt && \
    ln -s /opt/FastQC/fastqc /usr/local/bin/fastqc


FROM base as hisat2-builder
ARG HISAT2_VERIOSN
RUN wget https://cloud.biohpc.swmed.edu/index.php/s/oTtGWbWjaxsQ2Ho/download && \
    unzip download -d /opt && \
    ln -s "/opt/hisat2-$HISAT2_VERIOSN/hisat2" /usr/local/bin/hisat2


FROM base as bedtools-builder
ARG BEDTOOLS_VERSION
RUN wget "https://github.com/arq5x/bedtools2/releases/download/v$BEDTOOLS_VERSION/bedtools.static" && \
    mv bedtools.static /usr/local/bin/bedtools && chmod +x /usr/local/bin/bedtools


FROM base as gatk-builder
ARG GATK_VERSION
RUN wget "https://github.com/broadinstitute/gatk/releases/download/$GATK_VERSION/gatk-$GATK_VERSION.zip" && \
    unzip "gatk-$GATK_VERSION.zip" -d /opt && \
    ln -s /opt/gatk-$GATK_VERSION/gatk /usr/local/bin/gatk


FROM base as htslib-builder
ARG HTSLIB_VERSION
ARG SAMTOOLS_VERSION=${HTSLIB_VERSION}
ARG BCFTOOLS_VERSION=${HTSLIB_VERSION}
## Required for building
RUN pacman -S --noconfirm gcc make autoconf
## Download htslib. Required for building samtools and bcftools
RUN wget "https://github.com/samtools/htslib/releases/download/$HTSLIB_VERSION/htslib-$HTSLIB_VERSION.tar.bz2" && \
    tar -xf "htslib-$HTSLIB_VERSION.tar.bz2"
## Install samtools
RUN wget "https://github.com/samtools/samtools/releases/download/$SAMTOOLS_VERSION/samtools-$SAMTOOLS_VERSION.tar.bz2" && \
    tar -xf "samtools-$SAMTOOLS_VERSION.tar.bz2" && \
    cd "samtools-$SAMTOOLS_VERSION" && \
    autoheader && autoconf -Wno-syntax && \
    ./configure --prefix=/usr/local && make && make install
## Install bcftools
RUN wget "https://github.com/samtools/bcftools/releases/download/$BCFTOOLS_VERSION/bcftools-$BCFTOOLS_VERSION.tar.bz2" && \
    tar -xf "bcftools-$BCFTOOLS_VERSION.tar.bz2" && \
    cd "bcftools-$BCFTOOLS_VERSION" && \
    wget -P "plugins" https://raw.githubusercontent.com/freeseek/gtc2vcf/master/gtc2vcf.c && \
    wget -P "plugins" https://raw.githubusercontent.com/freeseek/gtc2vcf/master/gtc2vcf.h && \
    autoheader && autoconf -Wno-syntax && \
    ./configure --prefix=/usr/local && make && make install && \
    cp bcftools plugins/gtc2vcf.so /usr/local/lib


FROM base as annovar-builder
## Habil sent an email with the subject 'Fwd: ANNOVAR download' to the lab members providing the download link
RUN wget "http://www.openbioinformatics.org/annovar/download/0wgxR2rIVP/annovar.latest.tar.gz" && \
    tar -xf annovar.latest.tar.gz && \
    mv annovar/*.pl /usr/local/bin


FROM base as iaap-builder
## This tool is required to convert Illumina idat files to gtc files.
## Instructions are from https://raw.githubusercontent.com/freeseek/gtc2vcf
ARG IAAP_CLI_VERSION
RUN wget "ftp://webdata2:webdata2@ussd-ftp.illumina.com/downloads/software/iaap/iaap-cli-linux-x64-$IAAP_CLI_VERSION.tar.gz" && \
    tar -xf "iaap-cli-linux-x64-$IAAP_CLI_VERSION.tar.gz" --directory /opt && \
    chmod -R 755 "/opt/iaap-cli-linux-x64-$IAAP_CLI_VERSION/iaap-cli" && \
    ln -s "/opt/iaap-cli-linux-x64-$IAAP_CLI_VERSION/iaap-cli/iaap-cli" /usr/local/bin/iaap-cli


## Bring all tools in one image
FROM base as tools-image

COPY --from=salmon-builder       /usr/local/  /usr/local/
COPY --from=sortmerna-builder    /usr/local/  /usr/local/
COPY --from=bedtools-builder     /usr/local/  /usr/local/
COPY --from=htslib-builder       /usr/local/  /usr/local/
COPY --from=annovar-builder      /usr/local/  /usr/local/
## Using the option --parent of COPY is useful here to copy both folders at once,
## but it did not work for me. Seems that it is not implemented in the build context yet.
COPY --from=trimmomatic-builder  /opt/  /opt/
COPY --from=trimmomatic-builder  /usr/local/  /usr/local/
COPY --from=fastqc-builder       /opt/  /opt/
COPY --from=fastqc-builder       /usr/local/  /usr/local/
COPY --from=hisat2-builder       /opt/  /opt/
COPY --from=hisat2-builder       /usr/local/  /usr/local/
COPY --from=gatk-builder         /opt/  /opt/
COPY --from=gatk-builder         /usr/local/  /usr/local/
COPY --from=iaap-builder         /opt/  /opt/
COPY --from=iaap-builder         /usr/local/  /usr/local/

ARG SORTMERNA_VERSION
ENV SORTMERNA_VERSION ${SORTMERNA_VERSION}
## Preparing sortmerna refs (variable) and indices
RUN wget "https://github.com/sortmerna/sortmerna/archive/refs/tags/v$SORTMERNA_VERSION.zip" && \
    unzip "v$SORTMERNA_VERSION.zip" -d /opt && \
    echo 'export SORTMERNAREFS="--idx-dir /opt/sortmerna-$SORTMERNA_VERSION/data/idx $(find "/opt/sortmerna-$SORTMERNA_VERSION/data/rRNA_databases" -name "*.fasta" -exec printf " -ref '%s'" {} \;)"' >> /etc/profile && \
    ((touch empty.fq && . /etc/profile && sortmerna -reads empty.fq $SORTMERNAREFS) || true)

## Required for the software that use python instead of python3 (e.g. gatk)
RUN ln -s /usr/bin/python3 /usr/local/bin/python


FROM base

ARG ONCINFOUT_VERSION

## Install binary R packages that are both required and available.
## List of required packages must be created before building this image
## and must be on the build context as the file 'required.txt'
## The command 'com -12' would extract the common lines between the two 'sorted' files.
RUN --mount=target=required.txt,source=required.txt \
    comm -12 <(sort required.txt) <(pacman -Ss ^r- | grep '^bioarchlinux/r-' | awk '{print substr($1, 14)}' | sort) \
    | tr '\n' ' ' | xargs -L1 pacman -S --needed --noconfirm
## Be aware that the LATEST version of packages that are available for this
## version of R will be installed. We do not have any control on the R packages' version.
## Also if a package cannot be installed, the process does not stop.

## Configuring R
## ToDo: We may need more configurations for R
RUN <<EOF cat > ~/.Rprofile
local({
    r0 <- getOption("repos")
    r0["CRAN"] <- "https://cloud.r-project.org"
    options(repos=r0)
})
EOF

## Install OncinfoUt package which must be ready in the build context.
RUN --mount=target=OncinfoUt.tar.gz,source=OncinfoUt_${ONCINFOUT_VERSION}.tar.gz \
    R CMD INSTALL OncinfoUt.tar.gz
## In case you have for example c code in your package,
## you have to copy the package folder and build it here before installing

## Install system packages required by R packages
RUN pacman -S --noconfirm gsl gdal gcc-fortran

## Install the Oncinfo common packages
## See the notes on ../build.sh for description about GITHUB_PAT
ARG NUM_CPUS
ARG GITHUB_PAT
RUN GITHUB_PAT=${GITHUB_PAT} Rscript -e "OncinfoUt::call.libraries(doMindGithub=FALSE, Ncpus=$NUM_CPUS)"
## See the comments below the installation of binary R packages.

## Install tools
COPY --from=tools-image /opt/ /opt/
COPY --from=tools-image /usr/local/ /usr/local/
## To build index for genomic tsv files.
## I expected this to be installed when installing samtools, but apparently I was wrong.
RUN pacman -S --noconfirm tabix

RUN pacman -S --noconfirm rsync

## change directory to the home folder on the container
WORKDIR /root
