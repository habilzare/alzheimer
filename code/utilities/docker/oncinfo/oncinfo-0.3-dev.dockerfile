## Mohsen started writing this, 2023-10-18.

FROM habilzare/oncinfo:oncinfo-0.3

ARG NUM_CPUS

WORKDIR /root

RUN Rscript -e "BiocManager::install(ask=FALSE, Ncpus=$NUM_CPUS)"

ENV ONCINFOUT_VERSION=0.5.20
COPY OncinfoUt_$ONCINFOUT_VERSION.tar.gz .
RUN bash -c 'R CMD INSTALL $1 && rm $1' _ OncinfoUt_$ONCINFOUT_VERSION.tar.gz
## In case you have for example c code in your package,
## you have to copy the package folder and build it here before installing

RUN Rscript -e "OncinfoUt::call.libraries(Ncpus=$NUM_CPUS)"

ENV BIGGSEQ_VERSION=0.1.0
COPY BiggSeq_$BIGGSEQ_VERSION.tar.gz /root
RUN bash -c 'R CMD INSTALL $1 && rm $1' _ BiggSeq_$BIGGSEQ_VERSION.tar.gz
## In case you have for example c code in your package,
## you have to copy the package folder and build it here before installing

## If you need to bypass the caching mechanism of docker for this part,
## use --build-arg CACHEBUST="$(date +%s)" in your docker build command.
## Keep in mind that this way, everything from here would be built again.
ARG CACHEBUST=1
