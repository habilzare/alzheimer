import logging
import os
from pathlib import Path
import confuse
import yaml
from pyspark.sql import SparkSession


def _extract_config(conf):
    """Converts a Configuration object to a dictionary."""
    try:
        keys = conf.keys()
        res = dict()
        for k in keys:
            res[k] = _extract_config(conf[k])
        return res
    except confuse.ConfigTypeError:
        return conf.get()


def _load_from_file(filename):
    """Loads a configuration dictionary from a file."""
    config = confuse.Configuration("")
    config.set_file(filename)
    return _extract_config(config)


def load_experiment_config(config_path, experiment, proj_base=None, app_conf_file="application.yaml"):
    """Loads the experiment configuration.

    The behavior is in harmony with the OncinfoUt::loadExperimentConfig() function in R.
    :param config_path: The path to the config file(s).
    :param experiment: The experiment name.
    :param proj_base: The base directory for 'proj', if any.
    Defaults to the one that you provided in `config_path`.
    :param app_conf_file: The application config file.
    """
    try:
        if proj_base is None:
            proj_base = config_path.split("/proj/")[0]
        cfg_path = Path(config_path).expanduser()
        config = confuse.Configuration("")
        config.add(_load_from_file(cfg_path/"experiments"/(experiment + ".yaml")))
        while config['basedOn'].exists() and config['basedOn'].get() is not None:
            expr = config['basedOn'].get()
            print(f"The experiment is based on {expr}")
            expr_config = _load_from_file(cfg_path/"experiments"/(expr + ".yaml"))
            config['basedOn'].set(expr_config.get('basedOn'))
            config.add(expr_config)
        config.add(_load_from_file(cfg_path/app_conf_file))
        result = _extract_config(config)
        if 'resultMidfix' not in result.keys():
            result['resultMidfix'] = ''
        proj_path = Path(proj_base).expanduser()/"proj"/result['proj']['name']
        result['proj']['basePath'] = proj_base
        result['experiment'] = experiment
        result['experimentPath'] = str(proj_path/"result"/result['resultMidfix']/experiment)
        os.makedirs(result['experimentPath'], exist_ok=True)
        if 'dataset' in result.keys():
            dataset_name = result['dataset']
            result['dataset'] = result['datasets'][dataset_name]
            result['dataset']['name'] = dataset_name
            result['rawPath'] = str(proj_path/"data"/result['dataset']['midPath'])
    finally:
        pass
    return result


def get_task_params(config, task):
    """Loads task parameters.

    The behavior is in harmony with the OncinfoUt::getTaskParams() function in R.
    """
    params = confuse.Configuration("")
    params.add(config['params'][task])
    if config['params'].get('common') is not None:
        params.add(config['params']['common'])
    params.add({
        'task': task,
        'experiment': config['experiment'],
        'rawPath': config['rawPath'],
        'dataset': config['dataset'],
        'proj': config['proj'],
        'taskPathS': {}
    })

    for anyTask, itsParams in config[['params']].item():
        if anyTask != 'common':
            task_out_folder = task
            if itsParams.get('taskFolder') is not None:
                task_out_folder = itsParams[['taskFolder']]
            params['taskPathS'][anyTask] = os.path.join(config[['experimentPath']], task_out_folder)
    params['taskPath'] = params['taskPathS'][task]

    result = _extract_config(params)
    with open(Path(config['experimentPath'])/(task+".yaml"), 'w+') as file:
        yaml.dump(result, file)
    return result


def configure_basic_logging(config):
    logging.basicConfig(**config['log']['python'])


def add_experiment_arguments_to_parser(parser):
    """Adds the parameters which are required by any python script to be run on the cluster.
    Three arguments are:
    - --config-path: The path to the experiment config files.
    - --experiment: The name of the experiment.
    - --proj-base: The base path to the proj folder which contains the results
    (usually not the home folder in the cluster)
    """
    parser.add_argument('--config-path', required=True,
                        help="Path to the application config files.")
    parser.add_argument('--experiment', required=True,
                        help="Name of the experiment to get the parameters from.")
    parser.add_argument('--proj-base', required=True,
                        help="The base path for 'proj' folder to save the result in.")


def load_experiment_config_from_args(args):
    """Loads the experiment config from the usual arguments passed to a script."""
    return load_experiment_config(config_path=args.config_path,
                                  experiment=args.experiment,
                                  proj_base=args.proj_base)


def get_local_spark_session(app_name, spark_dict):
    """Creates a local spark session from a dictionary."""
    num_cores = spark_dict['num_cores']
    builder = SparkSession.builder.master(f"local[{num_cores}]").appName(app_name)
    for k, v in spark_dict['conf'].items():
        builder = builder.config(f"spark.{k}", v)
    return builder.getOrCreate()
