"""This package defines classes corresponding to different existing tasks.

Each module contains a config dataclass which represents the required config for that task.
Some of these configs might be partial which means that the actual task config
may contain more fields that should be parsed separately, not with a single from_dict."""

from dataclasses import dataclass
from typing import Optional

import numpy as np
import torch
from dacite import from_dict
from torch.utils.data import DataLoader

import deep
from assay import data_read, in_data
from assay.in_data import PreprocessConfig
from deep.in_data import SparseDataset, TupleDataset, SplitConfig
from deep.trainings import TrainConfig
from oncinfo_ut import get_task_params


@dataclass
class BaseConfig:
    """This is the required config for all tasks."""
    seed: int
    precision: str  ## float32 or float64
    source: data_read.SourceConfig


@dataclass
class TaskConfig(BaseConfig):
    split: SplitConfig
    preprocess: Optional[PreprocessConfig]
    model: dict
    name_pattern: str
    train: TrainConfig


class BaseTask:
    """This is the base class for all tasks.

    It has the minimal requirements of all tasks, i.e. having
    a reader object of type {DataRead} and a config object.
    Also for convenience, we include a {LossKeeper} in it.

    The task will initialize itself with the given {seed} and will use {precision}
    for all float calculations (pytorch and numpy). Also, it will read the {source}
    upon initialization. Tasks do not split or preprocess at the initialization time.
    Instead, they will have some methods to do so. This is because
    to give them a chance to actually have no source in config
    and set the input data in the code."""

    def __init__(self, experiment_config, config, device: torch.device):
        deep.set_global_seed(config.seed)
        torch.set_default_tensor_type(deep.get_tensor_type(config.precision))
        self.reader: in_data.DataRead = data_read.from_config(experiment_config, config.source)
        self.config = config
        self.device = device
        self.typ_indices: Optional[dict[str, list]] = None

    def reset_typ_indices(self, typ_indices=None):
        self.typ_indices = typ_indices if typ_indices is not None else\
            deep.in_data.split(self.reader.csr_mat.shape[0], self.config.split)

    def simple_collate(self, rows: list):
        tensor = torch.as_tensor(np.vstack(rows), device=self.device)
        return tensor, tensor

    def tuple_collate(self, rows: list):
        tensors = (torch.as_tensor(np.vstack([x[i] for x in rows]), device=self.device) for i in range(2))
        return tensors


class CellTask(BaseTask):
    """This is a concrete task and the samples are cells to be embedded"""

    def __init__(self, experiment_config: dict, task_name: str, device: torch.device):
        config = from_dict(TaskConfig, get_task_params(experiment_config, task_name))
        super().__init__(experiment_config, config, device)
        self._inp = None

    def get_data_loader(self, typ, model):
        if typ is None:
            _labels = SparseDataset(self.reader.csr_mat.astype(self.config.precision))
        else:
            _labels = SparseDataset(self.reader.csr_mat[self.typ_indices[typ]].astype(self.config.precision))
        if not hasattr(model, 'input_preparation'):
            return DataLoader(_labels, **self.config.train.loader, collate_fn=self.simple_collate)
        else:
            if self._inp is None:
                self._inp = getattr(model, 'prepare_input')(self.reader.csr_mat)
            if typ is None:
                _inp = SparseDataset(self._inp.astype(self.config.precision))
            else:
                _inp = SparseDataset(self._inp[self.typ_indices[typ]].astype(self.config.precision))
            return DataLoader(TupleDataset(_inp, _labels), **self.config.train.loader, collate_fn=self.tuple_collate)
