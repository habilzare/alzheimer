import logging
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

import numpy as np
import pandas as pd
import pyarrow.parquet
from dacite import from_dict
from scipy.io import mmread
from scipy.sparse import coo_matrix

from assay.in_data import DataRead
from oncinfo_ut import get_task_params


@dataclass
class SourceConfig:
    """Exactly one of path or task or test is mandatory.
    Here, filter_cells_query is a query which is passed to pandas.
    To see some examples, see 1970-01-01_test config in experiments"""
    path: Optional[str] = None
    task: Optional[str] = None
    test: Optional[dict] = None
    gene_total_threshold: Optional[int] = None
    filter_cells_query: Optional[str] = None
    remove_these_genes: Optional[list] = None


def from_source_of_task(experiment_config: dict, task: str):
    reader_task_config = get_task_params(experiment_config, task)
    source_config = from_dict(SourceConfig, reader_task_config['source'])
    return from_config(experiment_config, source_config)


def from_config(experiment_config: dict, source_config: SourceConfig) -> DataRead:
    """This function can read data in three modes.
      - if path is specified in source_config, it tries to read
        a portable summarize experiment format from a folder.
      - if task is specified in source_config, it tries to fall back to the source of that task.
      - if test is specified in source_config, it generates a test data to work with.

    :Authors: Mohsen
    """
    result = DataRead()
    if source_config.path is not None:
        path = Path(source_config.path).expanduser()
        summarize_experiment = read_pose(str(path))
        sparse_matrix = summarize_experiment['assays']['count'].transpose().tocsr()
        gene_meta = summarize_experiment['rowData']
        cell_meta = summarize_experiment['colData']
    elif source_config.task is not None:
        logging.info("Falling back to the source of the task '%s'", source_config.task)
        reader_task_config = get_task_params(experiment_config, source_config.task)
        source_config = from_dict(SourceConfig, reader_task_config['source'])
        result = from_config(experiment_config, source_config)
        sparse_matrix = result.csr_mat
        gene_meta = result.gene_meta
        cell_meta = result.cell_meta
        logging.info("Continue to the original source")
    elif source_config.test is not None:
        categories = ['cls1', 'cls2', 'cls3']
        test_data_config = source_config.test
        num_genes, num_cells = test_data_config['size']
        sparse_matrix = coo_matrix(np.random.randint(**test_data_config)).transpose().tocsr()
        gene_meta = pd.DataFrame({'X__index': range(num_genes)})
        cell_meta = pd.DataFrame({
            'CATEGORY': [categories[int(i)] for i in np.random.randint(0, len(categories), size=num_cells)],
            'REG1': np.random.uniform(0, 1, size=num_cells),
            'REG2': np.random.uniform(0, 1, size=num_cells),
        })
    else:
        logging.error("You should specify one of test or task or path in your source config")
        return result
    cell_meta.reset_index(drop=True, inplace=True)
    gene_meta.reset_index(drop=True, inplace=True)
    if source_config.filter_cells_query is not None:
        query = source_config.filter_cells_query
        cell_meta = cell_meta.query(query)
        sparse_matrix = sparse_matrix[cell_meta.index, :]
        cell_meta.reset_index(drop=True, inplace=True)
    if source_config.remove_these_genes is not None:
        # noinspection PyUnusedLocal
        removes = source_config.remove_these_genes
        gene_meta = gene_meta.query("X__index not in @removes")
        sparse_matrix = sparse_matrix[:, gene_meta.index]
        gene_meta.reset_index(drop=True, inplace=True)
    if source_config.gene_total_threshold is not None:
        threshold = source_config.gene_total_threshold
        good_genes = (sparse_matrix.sum(axis=0) > threshold).nonzero()[1]
        gene_meta = gene_meta.iloc[good_genes]
        sparse_matrix = sparse_matrix[:, gene_meta.index]
        gene_meta.reset_index(drop=True, inplace=True)
    logging.info("#Cells: %d,  #Genes: %d", sparse_matrix.shape[0], sparse_matrix.shape[1])
    result.gene_meta = gene_meta
    result.cell_meta = cell_meta
    result.csr_mat = sparse_matrix
    return result


def read_pose(path):
    """
    Shiva and Mohsen wrote this function on 2022-10-27 to read
    Portable SummarizedExperiment(pose) data from mtx(count data) and
    parquet (col data, row data) files and returns a dictionary which has
    the required fields to create a pySummarizedExperiment object.
    """
    logging.info("Making SummarizedExperiment ...")
    path = Path(path)
    col_data = pyarrow.parquet.read_table(path/"coldata.parquet").to_pandas()
    col_data.index = col_data["X__index"]
    row_data = pyarrow.parquet.read_table(path/"rowdata.parquet").to_pandas()
    row_data.index = row_data["X__index"]
    suffix = "_sparse.mtx.gz"
    mtx_files = [x.name for x in path.iterdir() if x.name.endswith(suffix)]
    ## returns files ending with suffix
    assays = dict()
    for f1 in mtx_files:
        assays[f1[:-len(suffix)]] = mmread(path/f1)
    logging.info(f"List of assays: {list(assays.keys())}")
    return {'assays': assays, 'colData': col_data, 'rowData': row_data}
