def cell_matrix_eda_basic(matrix, histogram_function):
    histogram_function(matrix.data, 'hist data')
    histogram_function(matrix.ceil().data, 'hist ceil data')
    histogram_function((matrix / 1e10).ceil().sum(axis=0).T, 'hist num expr over cells')
    histogram_function((matrix / 1e10).ceil().sum(axis=1), 'hist num expr over genes')
    histogram_function(matrix.ceil().sum(axis=0).T, 'hist sum ceil expr over cells')
    histogram_function(matrix.ceil().sum(axis=1), 'hist sum ceil expr over genes')
    histogram_function(matrix.sum(axis=0).T, 'hist sum expr over cells')
    histogram_function(matrix.sum(axis=1), 'hist sum expr over genes')
