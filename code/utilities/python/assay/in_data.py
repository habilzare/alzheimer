import logging
from dataclasses import dataclass
from typing import Optional

import numpy as np
import pandas as pd
import scanpy
from scipy.sparse import csr_matrix
from sklearn.base import BaseEstimator


class DataRead:
    csr_mat: Optional[csr_matrix] = None
    gene_meta: Optional[pd.DataFrame] = None
    cell_meta: Optional[pd.DataFrame] = None
    normalizer = None


@dataclass
class PreprocessConfig:
    transform: list
    normalize: dict  ## which should have the key `method`


@dataclass
class GeneAblationConfig:
    names: list
    complement: bool  ## whether ablate the names or complement of them
    method: str  ## available methods: shuffle


class NormalizeMethod:
    """Available normalize methods"""
    MakeAllCellSumMedian = 'MakeAllCellSumMedian'
    GeneMedian = 'GeneMedian'
    GeneMean = 'GeneMean'
    none = 'none'


def scanpy_transform(csr_mat, transform_config) -> csr_matrix:
    csr_mat = csr_mat.copy().astype('float')
    for transform in transform_config:
        func = getattr(scanpy.pp, transform['method'])
        if 'params' in transform:
            csr_mat = func(csr_mat, **transform['params'])
        else:
            csr_mat = func(csr_mat)
    return csr_mat


class Normalization(BaseEstimator):

    @staticmethod
    def fit(fit_to, **kwargs):
        return Normalization(kwargs['method'])._fit(fit_to)

    def __init__(self, method: str, scaler: np.ndarray = None):
        super().__init__()
        self.method = method
        self.scaler: np.ndarray = scaler

    def _fit(self, x):
        if self.method == NormalizeMethod.GeneMedian:
            _, num_genes = x.shape
            x = x.transpose().tocsr()
            res = np.zeros([1, num_genes])
            for i in range(num_genes):
                res[0, i] = 1 / np.median(x[i].data)
            self.scaler = res.reshape(1, -1)
        elif self.method == NormalizeMethod.GeneMean:
            self.scaler = 1 / x.mean(axis=0)
        elif self.method == NormalizeMethod.MakeAllCellSumMedian or self.method == NormalizeMethod.none:
            logging.warning("This normalization method does not require fitting.")
        else:
            logging.warning("normalization method not recognized.")
        return self

    def transform(self, x):
        if self.method == NormalizeMethod.none:
            return x
        if self.method == NormalizeMethod.MakeAllCellSumMedian:
            row_sum = x.sum(axis=1)
            row_sum_median = np.median(np.asarray(row_sum))
            logging.debug("cell sum median: %f", row_sum_median)
            self.scaler = (row_sum_median / row_sum).reshape(-1, 1)
        return x.multiply(self.scaler).tocsr()


def normalize(reader: DataRead, fit_data, config: dict, dtype: str):
    reader.normalizer = Normalization.fit(fit_data, **config)
    reader.csr_mat = reader.normalizer.transform(reader.csr_mat).astype(dtype)


def preprocess(reader: DataRead, normalize_fit_indices, config: PreprocessConfig, precision: str):
    """Runs preprocessing on the reader input. 1- Transform 2- Normalize

    :Authors: Mohsen
    :param reader:
    :param config:
    :param precision: Resulting data type
    :param normalize_fit_indices: Indices which are used to fit the normalizer transformer.
    """
    reader.csr_mat = scanpy_transform(reader.csr_mat, config.transform)
    normalize(reader, reader.csr_mat[normalize_fit_indices], config.normalize, precision)


class AblationMethod:
    shuffle = 'shuffle'


def ablate_genes(reader: DataRead, config: GeneAblationConfig):
    # noinspection PyUnusedLocal
    names = config.names
    query = 'X__index not in @names' if config.complement else 'X__index in @names'
    ablation_indices = reader.gene_meta.query(query).index

    if config.method == AblationMethod.shuffle:
        matrix = reader.csr_mat.tocsc()
        for i in ablation_indices:
            start = matrix.indptr[i]
            end = matrix.indptr[i + 1]
            new_indices = np.random.choice(matrix.shape[0], end - start, replace=False)
            new_indices.sort()
            matrix.indices[start:end] = new_indices
            data = matrix.data[start:end]
            np.random.shuffle(data)
            matrix.data[start:end] = data
        reader.csr_mat = matrix.tocsr()
    else:
        logging.error("unsupported ablation method")
