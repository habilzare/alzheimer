import copy
from dataclasses import dataclass
from typing import Optional

import ignite.engine
import torch
from ignite.engine import Events
from ignite.handlers import LRScheduler, EarlyStopping, ReduceLROnPlateauScheduler
from torch.optim.lr_scheduler import MultiplicativeLR


@dataclass
class OptimizerConfig:
    name: str
    params: dict


@dataclass
class SchedulerConfig:
    params: dict


@dataclass
class StoppingConfig:
    patience: int


@dataclass
class ExtraMultiplicativeConfig:
    lr_factor: float
    extra_epochs: int


@dataclass
class TrainConfig:
    optimizer: OptimizerConfig
    epochs: int
    log_every: int
    loader: Optional[dict]
    scheduler: Optional[SchedulerConfig]
    stopping: Optional[StoppingConfig]
    extra_multiplicative: Optional[ExtraMultiplicativeConfig]
    hyper_search: Optional[dict]


def apply_extra_multiplicative(train_config: TrainConfig) -> TrainConfig:
    if train_config.extra_multiplicative is not None:
        train_config = copy.deepcopy(train_config)
        total_reduction = train_config.extra_multiplicative.lr_factor**train_config.extra_multiplicative.extra_epochs
        train_config.optimizer.params['lr'] /= total_reduction
    return train_config


def setup_schedulers(val_evaluator: ignite.engine.Engine,
                     trainer: ignite.engine.Engine,
                     optimizer: torch.optim.Optimizer,
                     train_config: TrainConfig):
    if train_config.extra_multiplicative is not None:
        def lr_lambda(epoch):
            if epoch <= train_config.extra_multiplicative.extra_epochs:
                return train_config.extra_multiplicative.lr_factor
            else:
                return 1
        multiplicative = LRScheduler(MultiplicativeLR(optimizer, lr_lambda=lr_lambda))
        trainer.add_event_handler(Events.EPOCH_STARTED, multiplicative)
    if train_config.stopping is not None:
        early_stopping = EarlyStopping(train_config.stopping.patience, lambda x: -x.state.metrics['Loss'], trainer)
        val_evaluator.add_event_handler(Events.COMPLETED, early_stopping)
    if train_config.scheduler is not None:
        sch = ReduceLROnPlateauScheduler(optimizer, 'Loss', trainer, save_history=True, **train_config.scheduler.params)
        val_evaluator.add_event_handler(Events.COMPLETED, sch)


def get_optimizer(params_list, config: OptimizerConfig):
    optimizer = getattr(torch.optim, config.name)
    return optimizer(params_list, **config.params)
