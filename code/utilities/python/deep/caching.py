import typing
from typing import Iterator

import torch
from torch import nn
from torch.nn import Parameter
from torch.nn.modules.module import T
from torch.utils.data import DistributedSampler, Dataset, distributed


class DistributedSamplerWithDeferredShuffle(DistributedSampler):
    """If you want to cache the dataset, e.g. using FirstEmbeddingModule with caching,
    you can use this class as the sampler in your dataset,
    to have the same set of samples for each process of lightning.
    """
    def __init__(self, dataset, seed: int = 0):
        super(DistributedSamplerWithDeferredShuffle, self).__init__(dataset, seed=seed, shuffle=False)

    def __iter__(self):
        actual_indices = list(super(DistributedSamplerWithDeferredShuffle, self).__iter__())
        # deterministically shuffle based on epoch and seed
        g = torch.Generator()
        ## If we don't add anything related to the rank to our seed,
        ## we would shuffle the batches, but each batch (before gradient accumulation) would have the same samples.
        g.manual_seed(self.seed + self.epoch + 10000 * distributed.dist.get_rank())
        shuffled_indices = torch.randperm(self.num_samples, generator=g).tolist()
        return iter([actual_indices[i] for i in shuffled_indices])


class InMemoryDatasetWrapper(Dataset):
    def __init__(self, dataset: Dataset):
        self.dataset = dataset
        self.cache = dict()

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        if idx not in self.cache:
            self.cache[idx] = self.dataset[idx]
        return self.cache[idx]


class FirstEmbeddingModule(nn.Module):
    """To have an embedding as the first part of a model and cache it on the fly."""

    def __init__(self, encoder: nn.Module, model: nn.Module, prepare_fn: typing.Callable[[torch.Tensor], torch.Tensor] = lambda mat: mat.to_dense(), with_caching=False) -> None:
        """Initializes an EmbeddingCacherModule instance.
        :param encoder: The encoder module
        :param model: The actual model to be trained
        :param prepare_fn: How to prepare the raw input for the encoder"""
        super(FirstEmbeddingModule, self).__init__()
        self.prepare_fn = prepare_fn
        self.encoder = encoder.eval().requires_grad_(False)
        self.model = model
        self.cache = dict() if with_caching else None

    def parameters(self, recurse: bool = True) -> Iterator[Parameter]:
        return self.model.parameters(recurse=recurse)

    def train(self: T, mode: bool = True) -> T:
        self.model.train(mode)
        return self

    def forward(self, inputs):
        """
        xx: The raw patch input to the encoder.
        indices: The patch index.
        """
        xx, indices = inputs
        if self.cache is not None:
            for i0 in range(len(indices)):
                patch_index = int(indices[i0])
                if patch_index not in self.cache:
                    with torch.no_grad():
                        dense_input = self.prepare_fn(xx[i0,]).unsqueeze(0)
                        output = self.encoder(dense_input).squeeze(0)
                        del dense_input
                        self.cache[patch_index] = output.to("cpu")
                        del output
            encoder_output = torch.stack([self.cache[int(indices[i0])].to(xx.device) for i0 in range(len(indices))])
        else:
            ## ToDo: We may need to split the batch into mini batches not to get OOM error
            dense_input = self.prepare_fn(xx)
            encoder_output = self.encoder(dense_input)
        return self.model(encoder_output)
