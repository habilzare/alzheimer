import logging
import time
import typing

import pytorch_lightning as pl
import torch
from pytorch_lightning import Callback
from pytorch_lightning.utilities.types import STEP_OUTPUT
from torch import tensor, nn
from torch.nn import Parameter, ModuleDict, Module
from torch.optim import Optimizer


def grad_log(pl_module: pl.LightningModule, reduction):
    weights = dict(pl_module.named_parameters())
    pl_module.log_dict({f"grad_{reduction.__name__}/{layer}": reduction(w.grad()) for layer, w in weights.items()})


class TimeStoppingCallback(pl.Callback):
    """Use this callback to stop training before you run out of time in SLURM.
    The intention is to stop the job before it fails of a timeout
    and have the child job (which continues the training) running afterward.
    """
    def __init__(self, max_time_seconds: float, safety_factor: float = 1.5):
        """Initializes a new TimeStoppingCallback.
        :param max_time_seconds: The maximum time in seconds that we have in total for the job.
        :param safety_factor: A factor greater than 1.
        If there is not enough time for the next epoch, it would stop the trainer.
        Enough time means safety_factor * (maximum epoch time in this run).
        """
        super().__init__()
        self.safety_factor = safety_factor
        self.max_time_seconds = max_time_seconds
        self.start_time = time.time()
        self.max_epoch_time: float = 0
        self.current_epoch_start_time: float = self.start_time

    def on_train_epoch_start(self, trainer, pl_module):
        self.current_epoch_start_time = time.time()

    def on_validation_epoch_end(self, trainer: "pl.Trainer", pl_module: "pl.LightningModule") -> None:
        this_epoch_time = time.time() - self.current_epoch_start_time
        if this_epoch_time > self.max_epoch_time:
            self.max_epoch_time = this_epoch_time
        elapsed = time.time() - self.start_time
        remaining = self.max_time_seconds - elapsed
        should_stop = remaining < self.safety_factor * self.max_epoch_time
        ## The remaining logic is from EarlyStopping:
        ## stop every ddp process if any world process decides to stop
        should_stop = trainer.strategy.reduce_boolean_decision(decision=should_stop, all=False)
        trainer.should_stop = trainer.should_stop or should_stop
        if should_stop:
            logging.info("TIMEOUT! We are going to stop the training early on epoch %d"
                         " because we are running out of time."
                         " Remaining time: %d seconds,",
                         " Expected required time:",
                         " %.2f (safety_factor) * %d (longest epoch) seconds.",
                         trainer.current_epoch, remaining, self.safety_factor, self.max_epoch_time)


class SupervisedModule(pl.LightningModule):
    """This class intends to provide a generic framework to train supervised models with PyTorch Lightning.

    :Authors: Mohsen
    """

    def __init__(self, model: Module,
                 loss_fn: Module,
                 get_optimizer: typing.Callable[[typing.Iterator[Parameter]], Optimizer],
                 get_schedulers: typing.Callable[[Optimizer], typing.Any],
                 val_metrics: ModuleDict,
                 example_input_array: typing.Any = None,
                 prepare_batch_fn: typing.Callable[[typing.Any], typing.Tuple[typing.Any, typing.Any]] = None):
        """Initialize an instance of SupervisedModule
        :param model: The actual model of interest.
        :param loss_fn: The loss function as a pytorch Module.
        :param get_optimizer: A function which must take a list of learnable parameters
        and return an optimizer on those parameters.
        :param get_schedulers: A function which must take an optimizer and return a list of schedulers,
        probably an empty list.
        :param val_metrics: This is a ModuleDict which contains validation metrics.
        :param example_input_array: If you want to log the graph of the model,
        you need to set this parameter as a probably random good input.
        :param prepare_batch_fn: This is a function which must take a batch of data
        and return the input and target tensors.
        """
        super().__init__()
        self.model = model
        self.get_optimizer = get_optimizer
        self.get_schedulers = get_schedulers
        self.loss_fn = loss_fn
        self.val_metrics = val_metrics
        self.example_input_array = example_input_array
        self.prepare_batch_fn = prepare_batch_fn if prepare_batch_fn is not None else lambda x: x
        self.train_step_outputs = []
        self.val_step_outputs = {name: [] for name in self.val_metrics}

    def forward(self, *args, **kwargs):
        output = self.model(*args, **kwargs)
        return output

    def on_train_epoch_start(self) -> None:
        self.train_step_outputs = []

    def on_validation_epoch_start(self) -> None:
        logging.debug("Validation epoch started")
        self.val_step_outputs = {name: [] for name in self.val_metrics}

    def training_step(self, batch, batch_idx):
        logging.debug("Training step started for batch_idx %d", batch_idx)
        inputs, target = self.prepare_batch_fn(batch)
        output = self(inputs)
        loss = self.loss_fn(output, target)
        ## Disabled because we do not need it right now
        # self.log_dict({"training/batch_loss": loss, "step": float(self.global_step)}, sync_dist=True)
        output = {'loss': loss}
        self.train_step_outputs.append({'loss': loss.item()})
        logging.debug("Training step ended for batch_idx %d with output %s", batch_idx, output)
        return output

    def on_train_batch_end(self, outputs: STEP_OUTPUT, batch: typing.Any, batch_idx: int) -> None:
        logging.debug("Batch train ended for batch_idx %d with outputs %s", batch_idx, outputs)

    def validation_step(self, batch, batch_idx):
        logging.debug("Validation step started for batch_idx %d", batch_idx)
        inputs, target = self.prepare_batch_fn(batch)
        output = self.model(inputs)
        for name, metric in self.val_metrics.items():
            self.val_step_outputs[name].append(metric(output, target).item())

    def on_train_epoch_end(self) -> None:
        train_loss = tensor([out['loss'] for out in self.train_step_outputs]).mean().to(self.device)
        train_metrics = {"training/loss": train_loss, "step": tensor(float(self.current_epoch), device=self.device)}
        self.log_dict(train_metrics, sync_dist=True)

    def on_validation_epoch_end(self) -> None:
        val_metrics = {f"validation/{name}": tensor(out).mean().to(self.device)
                       for name, out in self.val_step_outputs.items()}
        val_metrics["step"] = tensor(float(self.current_epoch), device=self.device)
        self.log_dict(val_metrics, sync_dist=True)

    def configure_callbacks(self):
        return []

    def configure_optimizers(self):
        optimizer = self.get_optimizer(self.model.parameters())
        schedulers = self.get_schedulers(optimizer)
        return [optimizer], schedulers

    @classmethod
    def load_model_from_checkpoint(cls, model: nn.Module, checkpoint, device: torch.device):
        state_dict = torch.load(checkpoint, map_location=device)
        original_state_dict = state_dict['state_dict']
        ## In the state_dict, we have a 'model' key related to the 'model' field of this class
        ## which contains the underlying model parameters.
        cleaned_state = {key[len("model."):]: value for key, value in original_state_dict.items() if
                         key.startswith("model.")}
        model.load_state_dict(cleaned_state)


class LearningRateLogger(Callback):
    def __init__(self):
        super().__init__()

    def on_train_epoch_start(self, trainer: "pl.Trainer", pl_module: "pl.LightningModule"):
        optimizer = trainer.optimizers[0]
        lr = optimizer.param_groups[0]['lr']

        for logger in trainer.loggers:
            logger.log_metrics({'lr': lr}, step=trainer.current_epoch)
