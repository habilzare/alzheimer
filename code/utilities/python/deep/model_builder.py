import importlib
import sys
from typing import Dict, List, Any
import os
from torch import nn
from torchinfo import summary
from oncinfo_ut import _load_from_file

ArchDict = Dict[str, List[Dict[str, Dict[str, Any]]]]


def build_model_components(arch_dict: ArchDict) -> dict[str, nn.Module]:
    """To build a model from a dictionary.

    :param arch_dict: The input dictionary.
    Keys are part names, like 'encoder', 'decoder', etc.
    Each value is a list of Sequential layers.
    To specify each layer, use a 'single-key' dictionary.
    The key should be the module class, like 'nn.Conv2d'.
    The value is again a dictionary which would be passed to the class as parameters.
    Special key 'args' would be passed as positional arguments.
    :return: A dictionary which maps each key in arch_dict to a nn.Module object.
    """
    result = dict()
    for part, arch in arch_dict.items():
        layers = []
        for layer_arch in arch:
            assert len(layer_arch) == 1
            layer_class_name, kwargs = list(layer_arch.items())[0]
            ## We make a copy, not to alter the original input parameter in case it has "args" in it.
            kwargs = kwargs.copy() if kwargs is not None else dict()
            args = kwargs.pop('args', [])
            [layer_module_name, layer_class_name] = layer_class_name.split('.', maxsplit=1)
            ## If the required module is in globals(), we use it.
            ## Otherwise we first dynamically load that module.
            if layer_module_name in globals():
                model_module = globals()[layer_module_name]
            else:
                model_module = importlib.import_module(layer_module_name)
            layer_class = getattr(model_module, layer_class_name)
            layer = layer_class(*args, **kwargs)
            layers.append(layer)
        result[part] = nn.Sequential(*layers)
    return result


def load_arch_dict(yaml_file: str) -> ArchDict:
    return _load_from_file(yaml_file)


class AutoEncoder(nn.Module):
    def __init__(self, arch_dict: ArchDict):
        """To initialize an autoencoder model from a dictionary.

        :param arch_dict: The input dictionary. It must have exactly two keys: 'encoder' and 'decoder'."""
        super().__init__()
        components = build_model_components(arch_dict)
        assert set(components.keys()) == {'encoder', 'decoder'}
        self.encoder = components['encoder']
        self.decoder = components['decoder']

    def forward(self, inputs):
        encoded = self.encoder(inputs)
        decoded = self.decoder(encoded)
        return decoded


class SequentialModel(nn.Module):
    def __init__(self, arch_dict: ArchDict):
        """To initialize a sequential model from a dictionary.

        :param arch_dict: The input dictionary. It must have exactly one key. The key name does not matter."""
        super().__init__()
        components = build_model_components(arch_dict)
        assert len(components) == 1
        self.all = next(iter(components.values()))

    def forward(self, inputs):
        return self.all(inputs)




if __name__ == '__main__':
    ##Example: python ~/proj/alzheimer/code/utilities/python/deep/model_builder.py AutoEncoder ~/proj/deep/code/Shiva/architectures/cnn_ae_ks/v19.yaml
    module_directory = os.path.expanduser("~/proj/deep/code/Shiva/python")
    sys.path.append(module_directory)
    import cnnAe_oneClass as cnnae
    class_name = sys.argv[1]
    arch_file = sys.argv[2]

    model_arch_dict = load_arch_dict(arch_file)
    model = getattr(sys.modules[__name__], class_name)(model_arch_dict)
    print(model)
    ##input_size = [int(x) for x in sys.argv[3:]]
    summary(model, input_size=(1, 6078, 300, 300))
