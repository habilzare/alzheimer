"""CosMxDataset provides a custom dataset that can be integrated into the training pipeline
by utilizing PyTorch's dataloader for efficient batching.

Jan, 2024
"""

__all__ = ["CosMxSparseDataset", "CosMxDatasetConfig", "save_tx_as_sparse"]
__author__ = "Mohsen"

import logging
import typing
from dataclasses import dataclass
from pathlib import Path
from typing import Optional, Any, Callable

import pandas
import pandas as pd
import pyarrow
import pyarrow.parquet
import pyspark
import torch
from pyspark.sql.functions import col, broadcast, lit, collect_list, array
from pyspark.sql.types import IntegerType
from scipy.sparse import coo_matrix
from torch.utils.data import Dataset, default_collate


def save_tx_as_sparse(patch_size: int,
                      df_tx: pyspark.sql.DataFrame,  ## 'x', 'y', 'gene', ...
                      df_patch_label: pandas.DataFrame,  ## 'patch_index', 'center_x', 'center_y', ...
                      df_gene: pandas.DataFrame,  ## 'gene', 'gene_index'
                      save_path_dataset: Path,
                      save_path_gene_count: Path):
    ## ToDo: partitioning by patch_index (or patch_index%10 and patch_index to have fewer items in each folder)
    spark = df_tx.sparkSession
    ## Convert df_genes to spark dataframe
    df_gene_index = spark.createDataFrame(df_gene).coalesce(1)
    df_gene_index = df_gene_index.withColumn('gene_index', col('gene_index').cast(IntegerType()))
    ## Generate a dataframe of boxes around the centers as patches boundaries
    df_center = spark.createDataFrame(df_patch_label).coalesce(1)
    df_center = df_center.withColumn('patch_index', col('patch_index').cast(IntegerType()))
    df_center = (
        df_center
        .withColumn('min_x', col('center_x') - lit(patch_size / 2))
        .withColumn('max_x', col('min_x') + lit(patch_size))
        .withColumn('min_y', col('center_y') - lit(patch_size / 2))
        .withColumn('max_y', col('min_y') + lit(patch_size))
    )

    ## Do Cartesian join and filter for points inside the patches
    df_main = df_tx.join(broadcast(df_center))

    ## Add columns 'i' and 'j' as integer coordinates inside the patches
    ## We do the filtration in this way to have correct range for 'i' and 'j' columns.
    ## We will use the reverse order for j-coordinate.
    df_main = df_main.where("min_x <= x AND x < max_x").where("min_y < y AND y <= max_y")
    df_main = df_main.withColumn('i', (col('x') - col('min_x')).cast(IntegerType()))
    df_main = df_main.withColumn('j', (col('max_y') - col('y')).cast(IntegerType()))

    ## Adding gene_index and cache it to use it multiple times
    df_main = df_main.join(broadcast(df_gene_index), on='gene').cache()

    ## Computing gene counts per patch
    df_gene_count = df_main.groupBy('patch_index', 'gene_index', 'gene').count()

    ## Select required columns and save the data
    df_dataset = df_main.groupBy('patch_index').agg(
        collect_list(array(col('gene_index'), col('i'), col('j'))).alias('nz_elements')
    )

    ## We can group by columns 'patch_index', 'i', 'j', 'gene_index'
    ## and do aggregation of values (which means count here).
    ## But we postpone it for to_dense function of torch,
    ## because we would need it eventually after applying the possible reduction.

    df_dataset.printSchema()
    df_dataset.write.mode("overwrite").parquet(str(save_path_dataset))
    df_gene_count.printSchema()
    df_gene_count.write.mode("overwrite").parquet(str(save_path_gene_count))


@dataclass
class CosMxDatasetConfig:
    """Configuration of the CosMXDataset.

    num_genes : Total number of genes (the third dimension of patch tensor)
    patch_size : The patch_size of the surrounding box (width, height)
    reduction : In case of high resolution, used to reduce the patch size
    """

    num_genes: int
    patch_size: int
    dtype: torch.dtype
    reduction: Optional[int] = 1

    def __post_init__(self):
        if self.patch_size % self.reduction != 0:
            raise ValueError(
                f"Reduction({self.reduction}) is not a divisor of box's width({self.patch_size})!",
            )


class CosMxSparseDataset(Dataset):
    """
    This is a dataset originally designed to work with the CosMx data,
    but it can be used on any similar data.
    """

    def __init__(self,
                 cfg: CosMxDatasetConfig,
                 df_patch_label: pandas.DataFrame,
                 label_creator: Callable[[pandas.Series], Any],
                 load_path: Path):
        """Initialize the CosMxSparseDataset.
        :param cfg: The configuration. See CosMxDatasetConfig for details.
        :param df_patch_label: A dataframe where each row corresponds to a patch
        and may include any information or labels for that patch.
        :param label_creator: A function which gets a row of `df_patch_label` and returns anything
        supported by pytorch as the label. It can use the existing columns of `df_patch_label`.
        :param load_path: The path to the main dataframe which is created by the function `save_tx_as_sparse`.
        """
        self.df_patch_label = df_patch_label
        self.label_creator = label_creator
        self.reduction = cfg.reduction
        self.dtype = cfg.dtype
        self.patch_size = cfg.patch_size
        ## The output size of the tensors that will be generated by __getitem__
        self.out_size = [cfg.num_genes, self.patch_size // self.reduction, self.patch_size // self.reduction]
        logging.info("Reading parquet file")
        self.data = pyarrow.parquet.read_table(load_path, columns=['patch_index', 'nz_elements'])
        logging.info("Done reading parquet file")

    def __len__(self):
        return len(self.df_patch_label)

    def __getitem__(self, idx: int) -> tuple[list, list, list, Any]:
        """
        :return: A tuple. The first 3 items are lists, each one is a list of indices.
        The first one is for gene indices in the batch,
        the second one is for the i-coordinates in the patch,
        and the third one is for the j-coordinates in the patch.
        These indices are matched (specially has the same length)
        and can be used for creating a sparse tensor.
        The forth item is the label.
        """
        patch_index = self.df_patch_label.iloc[idx]['patch_index']
        patch = self.data.filter(pyarrow.compute.field('patch_index') == patch_index).to_pylist()
        patch = patch[0]['nz_elements'] if len(patch) == 1 else []
        patch_label = self.df_patch_label.iloc[idx, :]
        patch_label = self.label_creator(patch_label)
        return ([gene_index for [gene_index, _, _] in patch],
                [i // self.reduction for [_, i, _] in patch],
                [j // self.reduction for [_, _, j] in patch],
                patch_label)

    @classmethod
    def get_label_from_item(cls, item):
        return item[3]

    def get_original_item(self, idx: int) -> tuple[list, list, list]:
        """To get the original patch before reduction.
        :param idx: The index of the patch.
        :return: Something like __getitem__, but without any label.
        You can pass the output (after adding a dummy first dimension) to `collate_fn`
        to get the desired sparse_coo_tensor,
        but you can also use the output to generate scipy.sparse.coo_matrix for example.
        """
        patch_index = self.df_patch_label.iloc[idx]['patch_index']
        patch = self.data.filter(pyarrow.compute.field('patch_index') == patch_index).to_pylist()
        patch = patch[0]['nz_elements'] if len(patch) == 1 else []
        return ([gene_index for [gene_index, _, _] in patch],
                [i for [_, i, _] in patch],
                [j for [_, _, j] in patch])

    def get_dense_matrices(self, original_item, genes_of_interest):
        """Create dense matrices for all genes and then return dense matrices only for genes of interest.

        :param original_item: The result of a get_original_item function call.
        :param genes_of_interest: list, the gene indices that we're interested in

        :return: A dictionary of dense matrices for the specified genes of interest
        """
        gene_indices, i_coords, j_coords = original_item
        df = pd.DataFrame({"gene_indices": gene_indices, "i_coords": i_coords, "j_coords": j_coords})

        dense_gene_matrices = dict()
        for interested_gene in genes_of_interest:
            filtered_df = df[df["gene_indices"] == interested_gene]
            i_s = filtered_df["i_coords"]
            j_s = filtered_df["j_coords"]
            sparse_mat = coo_matrix(([1] * len(filtered_df), (i_s, j_s)), shape=(self.patch_size, self.patch_size))
            dense_gene_matrices[interested_gene] = sparse_mat.todense()
        return dense_gene_matrices

    def collate_fn(self, batch) -> (torch.Tensor, list[typing.Any]):
        """To be used as the collate_fn in DataLoader's.
        :param batch: A list containing the results of __getitem__ or get_original_item functions.
        :return: A tuple containing:
        - A sparse_coo_tensor representing the batch as the first item.
        - The collated labels as the second item.
        """
        batch_index_list = []
        gene_index_list = []  # Depth indices
        i_list = []  # Row indices
        j_list = []  # Column indices
        for k, (gene_index, i, j, _) in enumerate(batch):
            batch_index_list += [k] * len(gene_index)
            gene_index_list += gene_index
            i_list += i
            j_list += j
        indices = torch.tensor([batch_index_list, gene_index_list, i_list, j_list], dtype=torch.int32)
        values = torch.tensor([1.0] * indices[0].shape[0], dtype=self.dtype)
        ## The function to_dense would take care of coalescing the sparse tensor.
        ## This means that if we have multiple values for the same coordinate, they would sum up.
        item_tensor = torch.sparse_coo_tensor(indices, values, size=[len(batch), *self.out_size])
        return item_tensor, default_collate([patch_label for (_, _, _, patch_label) in batch])

    @classmethod
    def get_dense_tensor(cls, collated, device=None):
        return collated[0].to_dense() if device is None else collated[0].to(device).to_dense()
