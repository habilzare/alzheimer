"""This script provides a custom dataset for latents of CNN AE models that can be integrated into the training pipeline
by utilizing PyTorch's dataloader for efficient batching.

March, 2024
"""

__author__ = "Shiva"
import io
import logging
import tarfile
from dataclasses import dataclass

import pandas
import pandas as pd
import torch
from torch.utils.data import Dataset
from typing import Optional, Any, Callable


@dataclass
class LatentDataset(Dataset):
    def __init__(self, tar_path, df_patch_label: pd.DataFrame,
                 label_creator: Callable[[pd.Series], Any], label_key: str = None):
        """Initialize
        :param tar_path: path to the tar file, e,g. /scratch/07138/shivakp/proj/deep/result/2024-03-18_saveProducts/cnn_ae_ks/v18/latents.tar
        :param df_patch_label: A dataframe where each row corresponds to a patch
        and may include any information or labels for that patch. The index of this dataframe matches the name files in tar file
        :param label_creator: A function which gets a row of `df_patch_label` and returns anything
        supported by pytorch as the label. It can use the existing columns of `df_patch_label`.
        :param label_key: the name of the keys in label_creator that the user needs to be returned from this dataset.
        """
        self.tar_path = tar_path
        self.df_patch_label = df_patch_label
        self.label_creator = label_creator
        self.label_key = label_key
        #tar_file = tarfile.open(self.tar_path, "r") # cannot be copied
        # ToDo: make member.name.lstrip('./').split('.')[0] a function, then incorporate in line 39
        with tarfile.open(self.tar_path, "r") as tar_file:
            available_members = {member.name.lstrip('./').split('.')[0]: member for member in tar_file.getmembers()
                                 if member.isfile() and member.name.lstrip('./').split('.')[0].isdigit()}
        logging.info("Members of tar file retrieved successfully")
        # Filter df_patch_label to only include indices that are available in the tar file
        available_indices = list(map(int, available_members.keys()))
        self.df_patch_label = df_patch_label[df_patch_label['patch_index'].isin(available_indices)]
        self.member_dict = available_members
        self.tar = None

    def __len__(self):
        return len(self.df_patch_label)

    def __getitem__(self, idx):
        # Get patch_index for the current idx
        patch_index = self.df_patch_label.iloc[idx]['patch_index']
        # Find the corresponding tar member
        member = self.member_dict[str(patch_index)]
        if self.tar is None:
            self.tar = tarfile.open(self.tar_path, "r") #each subprocess will call getitem sequentially, not in parallel
        # Extract the .pt file from the tar and load it
        latent_tensor = torch.load(self.tar.extractfile(member), map_location='cpu')
        latent_tensor = latent_tensor.detach().requires_grad_(False)
        if latent_tensor.shape[0] == 1:
            latent_tensor = latent_tensor.squeeze(0)
        # Use the label_creator to get the label
        item = self.label_creator(self.df_patch_label.iloc[idx])
        label = item[self.label_key][0]
        return latent_tensor, label, item

    @classmethod
    def get_label_from_item(cls, item):
        return item[0]

    #def close(self):
     #   self.tar.close()

    def __del__(self):
        if self.tar is not None:
            self.tar.close()
            self.tar = None

