import logging

import numpy as np
import torch
from torch import cuda

VAL = "val"
TRN = "trn"
TST = "tst"


def set_global_seed(seed: int):
    torch.manual_seed(seed)
    np.random.seed(seed)


def get_device():
    return torch.device('cuda' if cuda.is_available() else 'cpu')


def get_tensor_type(precision: str):
    return torch.DoubleTensor if precision == 'float64' else torch.FloatTensor


def get_activation_layer(name: str):
    return getattr(torch.nn, name)()


def log_cuda_mem_info(level):
    one_gig = 1024.0 * 1024 * 1024
    logging.log(level, "allocated: %.2fGB", cuda.memory_allocated() / one_gig)
    logging.log(level, "reserved: %.2fGB", cuda.memory_reserved() / one_gig)
    logging.log(level, "max allocated: %.2fGB", cuda.max_memory_allocated() / one_gig)
    logging.log(level, "max reserved: %.2fGB", cuda.max_memory_reserved() / one_gig)


def recursive_sub(x, indices):
    """To sub_index tensors (even if {x} is a tuple or tuple of tuples or ...)"""
    return tuple(recursive_sub(y, indices) for y in x) if isinstance(x, tuple) else x[indices]
