import torch
from torch import nn

## We may use a "torchmetrics.Metric" instead of our loss
## and handle the accumulation of metrics with its abilities instead of what we did in the SupervisedModule class.
## It requires to define a Metric class.
## To do this, we need to implement some extra but simple functions.
## It would be good to name it WeightedMSEMetric.


class WeightedMSELoss(nn.Module):
    ## \sigma(weights_i * (x_i - y_i)^2)
    ## If you want your weights to actually multiply with input and target,
    ## pass its square to this module.
    def __init__(self, weights):
        super().__init__()
        self.sqrt_weights = nn.Parameter(torch.sqrt(weights), requires_grad=False)
        ## So that whenever this module goes to any device or changes its dtype,
        ## `self.weights` also gets the same effect.

    def forward(self, inputs, target):
        loss = nn.functional.mse_loss(self.sqrt_weights * inputs, self.sqrt_weights * target)
        return loss


class WeightedL1Loss(nn.Module):
    def __init__(self, weights):
        super().__init__()
        self.weights = nn.Parameter(weights, requires_grad=False)
        ## So that whenever this module goes to any device or changes its dtype,
        ## `self.weights` also gets the same effect.

    def forward(self, inputs, target):
        loss = nn.functional.l1_loss(self.weights * inputs, self.weights * target)
        return loss
