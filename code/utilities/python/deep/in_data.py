from dataclasses import dataclass
from typing import Optional

from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset

from . import TRN, TST, VAL


@dataclass
class SplitConfig:
    ratio_train: float
    ratio_test_to_val: float
    random_state: Optional[int] = None


def split(length: int, config: SplitConfig):
    """Splits indices to train, validation and test parts randomly with the given ratios.

    :Authors: Mohsen
    :param int length: Number of samples to be split.
    :param SplitConfig config:
    :return dict: A dictionary with keys {TRN}, {VAL}, {TST}
    """
    indices = dict()
    indices[TRN], indices[TST] = train_test_split(range(length),
                                                  train_size=config.ratio_train,
                                                  random_state=config.random_state)
    if config.ratio_test_to_val > 10:
        indices[VAL] = []
    elif config.ratio_test_to_val > 0:
        ratio_val = 1 / (1 + config.ratio_test_to_val)
        indices[VAL], indices[TST] = train_test_split(indices[TST],
                                                      train_size=ratio_val,
                                                      random_state=config.random_state)
    else:
        indices[VAL] = indices[TST]
        indices[TST] = []
    return indices


class SparseDataset(Dataset):
    """A dataset from a sparse matrix

    :Authors: Mohsen
    """

    def __init__(self, csr_mat):
        self.csr_matrix = csr_mat

    def __len__(self):
        return self.csr_matrix.shape[0]

    def __getitem__(self, item):
        return self.csr_matrix[item].toarray()


class TupleDataset(Dataset[tuple]):
    """New dataset from existing ones by making tuples of their rows.

    :Authors: Mohsen
    """

    def __init__(self, *datasets):
        for dataset in datasets:
            assert len(dataset) == len(datasets[0])
        self.datasets = datasets

    def __len__(self):
        return len(self.datasets[0])

    def __getitem__(self, item):
        return tuple(dataset[item] for dataset in self.datasets)
