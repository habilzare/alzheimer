## Download or create fasta files and indices for NAAS
## NAAS (no alt analysis set) is recommended by
## http://lh3.github.io/2017/11/13/which-human-reference-genome-to-use
## for alignment + variant calling pipelines.

URL_BASE := https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/001/405/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids
FASTA_FILE := GCA_000001405.15_GRCh38_no_alt_analysis_set.fna

all: $(FASTA_FILE) $(FASTA_FILE).fai $(FASTA_FILE).sa $(FASTA_FILE).dict

$(FASTA_FILE):
	wget -O $(FASTA_FILE).gz $(URL_BASE)/$(FASTA_FILE).gz
	gzip -c -d $(FASTA_FILE).gz > $(FASTA_FILE)

$(FASTA_FILE).fai: $(FASTA_FILE)
	samtools faidx $<

$(FASTA_FILE).sa: $(FASTA_FILE)
	wget -O $(FASTA_FILE).bwa_index.tar.gz $(URL_BASE)/$(FASTA_FILE).bwa_index.tar.gz
	## touch is necessary because the archive files are old when extracted
	tar --touch -xf $(FASTA_FILE).bwa_index.tar.gz
	rm $(FASTA_FILE).bwa_index.tar.gz
	mv $(FASTA_FILE).bwa_index/*.fna.* ./
	rm -r $(FASTA_FILE).bwa_index

$(FASTA_FILE).dict: $(FASTA_FILE).fa
	gatk CreateSequenceDictionary -R $<

clean:
	rm -f $(FASTA_FILE) $(FASTA_FILE).*
