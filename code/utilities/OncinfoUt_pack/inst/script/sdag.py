## https://oncinfo.org/mohsens_lab_notebook#section20230530

import os
import subprocess
import sys


class SlurmDAGMan(object):

    def __init__(self, dag_file):
        self.dag_file = dag_file
        self.jobs = []

    def parse(self):
        with open(self.dag_file) as f:
            lines = f.readlines()
            for i, line in enumerate(lines):
                if line.startswith('#') or line.strip() == '':
                    continue
                elif line.strip().startswith('JOB'):
                    try:
                        line_items = line.split()
                        job = {'name': line_items[1]}
                        script_file = line_items[2]
                    except:
                        self._error(i, 2)
                    job['script'] = script_file
                    job['parents'] = set()
                    job['children'] = set()
                    job['ID'] = None
                    self.jobs.append(job)

                elif line.strip().startswith('PARENT'):
                    try:
                        jobs = line.strip().lstrip('PARENT').split('CHILD')
                        if len(jobs) != 2:
                            self._error(i, 1)
                    except:
                        self._error(i, 1)
                    parents_names = jobs[0].split()
                    children_names = jobs[1].split()
                    parents = [self._get_job(i, name) for name in parents_names]
                    children = [self._get_job(i, name) for name in children_names]
                    for job in parents:
                        ancestors = self._get_job_ancestors(job['name'])
                        if ancestors is not None:
                            for child in children_names:
                                if child in ancestors:
                                    self._error(i, 10, 'Job ' + job['name'] +
                                                ' Cannot be a parent for job ' + child +
                                                '. Job ' + child +
                                                ' is an ancestor of job ' + job['name'])
                        job['children'].update(children_names)
                    for job in children:
                        job['parents'].update(parents_names)
                else:
                    self._error(i, 0)

    def _get_job_ancestors(self, job_name):
        job = self._get_job('#', job_name)
        a_list = set()
        parents = list(job['parents'])
        if len(parents) == 0:
            return
        for parent in parents:
            a_list.add(parent)
            g_parents = self._get_job_ancestors(parent)
            if g_parents is not None:
                a_list.update(g_parents)
        return list(a_list)

    def run(self):
        for job in self.jobs:
            if len(job['children']) == 0:
                self._submit(job['name'])

    def _submit(self, job_name):
        job = self._get_job('#', job_name)
        command_list = ['sbatch']
        if len(job['parents']) > 0:
            parents = [self._get_job('#', name) for name in list(job['parents'])]
            dependencies = '--dependency=afterok:'
            parent_ids = []
            for parent in parents:
                if parent['ID'] is None:
                    parent['ID'] = self._submit(parent['name'])
                parent_ids.append(parent['ID'])
            dependencies += ':'.join(parent_ids)
            command_list.append(dependencies)

        command_list.append(job['script'])

        p = subprocess.Popen(command_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()

        # out = subprocess.check_output(' '.join(command_list), stderr=subprocess.STDOUT)
        # print out

        if out.strip() == '':
            self._error('#', '#', 'Error submitting job ' + job_name + '\n' + str(err))
        else:
            job['ID'] = str(out).strip("'n\\").split()[-1]
            # print 'submitted:\n' + ' '.join(command_list)
            # print out
            return job['ID']

    def _get_job(self, line_no, job_name):
        for job in self.jobs:
            if job['name'] == job_name:
                return job
        self._error(line_no, 10, 'Job ' + job_name + ' is not defined')

    @staticmethod
    def _error(line_no, error_no, message=''):
        if line_no == '#':
            print(message, file=sys.stderr)
            sys.exit(0)

        if error_no == 0:
            msg = 'Invalid syntax'
        elif error_no == 1:
            msg = 'A workflow Statement must be written as:\n' + \
                  'PARENT <parent_jobs> CHILD <children_jobs>'
        elif error_no == 2:
            msg = 'A job definition Statement must be written as:\n' + \
                  'JOB <job_name> <job_submission_file>'
        else:
            msg = message

        print('Error in line [' + str(line_no) + ']: ' + msg, file=sys.stderr)
        sys.exit(0)

    @staticmethod
    def _get_job_string(job):
        string = ''
        string += 'Job ' + job['name'] + '\t' + 'ID: ' + str(job['ID']) + '\n'
        string += 'Parents: ' + ','.join(list(job['parents'])) + '\n'
        string += 'Children: ' + ','.join(list(job['children'])) + 2 * '\n'
        return string

    def __str__(self):
        string = ''
        if len(self.jobs) > 0:
            for job in self.jobs:
                string += self._get_job_string(job)
        return string


######################################################################
def main(argv):
    if len(argv) != 1:
        print('Missing argument: DAG description file')
        sys.exit()
    arg = argv[0]
    if arg in ['-h', '--help']:
        print('A simple DAG manager for SLURM.')
        sys.exit()
    else:
        dag_file = arg
        if not os.path.isfile(dag_file):
            print('Error: You must enter a valid DAG description file')
            sys.exit(1)
        dag_man = SlurmDAGMan(dag_file)
        dag_man.parse()
        dag_man.run()
        print(dag_man)


if __name__ == "__main__":
    main(sys.argv[1:])
