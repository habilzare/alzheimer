## Habil wrote this script to do DE analysis.
## It should be sourced from runall.R.
## 2022-10-25.
##
## Mohsen adapted it for this example, 2024-08-01.

outputPath <- taskParams[['taskPath']]
designExpression <- taskParams[['designExpression']]
inputFile <- file.path(taskParams[['rawPath']], "aml_mds.RData")

log_info("DE started.")
tic("DE took:")

## Getting data:
variableName <- load(inputFile)
se <- get(variableName)
Data <- assay(se, 1)
colDataDF <- colData(se)

## Design matrix
design1 <- model.matrix(as.formula(designExpression), data=colDataDF)

## Fitting:
fit <- lmFit(Data, design1)
fitB <- eBayes(fit)

log_info("DE results are saved at: ", outputPath)

## Save:
save(fitB, file=file.path(outputPath, "fitB.RData"))

toc()
