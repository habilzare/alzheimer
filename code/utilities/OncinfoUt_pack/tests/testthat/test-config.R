setwd(test_path("fixtures"))

withr::local_package("configr")
withr::local_package("logger")
withr::local_package("yaml")

test_that("loadExperimentConfig requires experiment config to exist", {
    expect_error(
        loadExperimentConfig(experiment="2000-01-01_no_conf_exp"),
        ".*2000.*"
    )
})
test_that("loadExperimentConfig successfully loads config", {
    conf <- loadExperimentConfig(experiment="2000-01-01_test")
    expect_equal(conf$experiment, "2000-01-01_test")
})
test_that("time2char converts time to character?", {
    time1 <- Sys.time()
    char1 <- time2char(timeIn=time1)
    expect_equal(char1, gsub(time1, pattern=" |:", replacement="_"))
})
test_that("saveMpi can create multiple mpi by spliting", {
    conf <- loadExperimentConfig(experiment="2000-01-01_mpi")
    params <- getTaskParams(conf, "DEAnalysis")
    writeJobScripts(params)
    expect(file.exists("DEAnalysis/dataset=apple/pval=0.01/mpi/test.mpi"), failure_message="Not exist: .mpi file")
    expect(file.exists("DEAnalysis/dataset=orange/pval=0.05/mpi/test.sh"), failure_message="Not exist: .sh file")
})
