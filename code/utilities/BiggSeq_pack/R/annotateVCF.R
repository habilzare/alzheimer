#' To annotate a vcf file using table_annovar.pl and gatk
#'
#' @param fileName The vcf file name.
#' @param inputPath The folder containing the input vcf file.
#' @param outputPath The folder for the output file(s).
#' @param dbPath The path containg annotation databases.
#' @param buildVersion One of hg19 or hg38
#' @param protocolG
#' @param protocolF
#' @param javaOptions The java options to be passed to gatk.
#' @param tempFolder A temporary folder to store the output of table_annovar.pl script.
#' @param doDeleteTemp Whether to remove the temp folder after the work is done.
#'
#' @export
#'
annotateVCF <- function(fileName, inputPath, outputPath,
                        dbPath, buildVersion, protocolG, protocolF, javaOptions,
                        tempFolder=tempdir(), doDeleteTemp=TRUE) {
    ## Write annotated results in the `tempFolder`
    dir.create(tempFolder, showWarnings=FALSE)
    logger::log_info("Starting table_annovar command ...")
    args1 <- c(
        file.path(inputPath, fileName),
        dbPath,
        '-out', file.path(tempFolder, fileName),
        '-vcfinput',
        '-buildver', buildVersion,
        '-protocol', paste0(protocolG, ",", protocolF),
        '-operation', 'g,f'
    )
    logger::log_debug("arguments: ", paste0(args1, collapse=" "))
    system2("table_annovar.pl", args=args1)

    ## Convert back to vcf.gz in the `outputPath`
    logger::log_info("Starting gatk command ...")
    args1 <- c(
        '--java-options', javaOptions,
        'VcfFormatConverter',
        '-I', file.path(tempFolder, paste0(fileName, ".", buildVersion, "_multianno.vcf")),
        '-O', file.path(outputPath, fileName),
        '--REQUIRE_INDEX', 'false'
    )
    logger::log_debug("arguments: ", paste0(args1, collapse=" "))
    system2('gatk', args=args1)

    ## Delete `tempFolder`
    if (doDeleteTemp) unlink(tempFolder)

    return(list(tempFolder=tempFolder))
}
