#' To quantize a fastq file using salmon quant
#'
#' This function must be run on a system with enough resource and time,
#' most likely on a computing cluster machine.
#'
#' @param r1FilePath The full path to the r1 file which must end with _R1.fastq.gz.
#'                   If a file with the similar name with R2 exists,
#'                   then we consider them as paired reads.
#' @param outputPath The folder for the output files.
#' @param libraryType Type of the library.
#' @param indexPath The path to the index files to be used with salmon.
#' @param threads Number of threads to be used.
#' @param salmonExtraArgs Any extra arguments to be passed to salmon.
#'
#' @export
#'
quantizeFastq <- function(r1FilePath, outputPath,
                          libraryType, indexPath, threads, salmonExtraArgs) {
    inputPath <- dirname(r1FilePath)
    r1FileName <- basename(r1FilePath)
    sampleName <- sub("_R1\\.fastq\\.gz", "", r1FileName)
    r2FileName <- paste0(sampleName, "_R2.fastq.gz")
    isPairedEnd <- file.exists(file.path(inputPath, r2FileName))
    logger::log_debug("Expected R2 file: ", file.path(inputPath, r2FileName))
    logger::log_debug("Existense of R2 file: ", isPairedEnd)
    logger::log_info("Starting 'salmon quant' command ...")
    args1 <- c(
        'quant',
        '-l', libraryType,
        '-1', r1FilePath,
        unlist(ifelse(isPairedEnd, list(c('-2', file.path(inputPath, r2FileName))), list(NULL))),
        '-o', file.path(outputPath, sampleName),
        '-i', indexPath,
        '-p', as.character(threads),
        salmonExtraArgs
    )
    logger::log_debug("arguments: ", paste0(args1, collapse=" "))
    system2('salmon', args=args1)
    return()
}
