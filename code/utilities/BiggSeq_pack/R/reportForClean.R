## Mohsen wrote this on 2023-05-13
## (copied from script/clean_report.R)
## to generates read counts for different stages of the clean task.

#' This function generates a report of read counts
#' for different stages of the clean task.
#'
#' For parallelism purposes, first create and register a doParallel cluster.
#'
#' @param rawPath Path containing input fastq files.
#' @param cleanedPath Path containing final cleaned fastq.gz files.
#' @param sortedPath Path to intermediate (output of sortmerna) fq.gz files, if any.
#'
#' @returns A dataframe where each row is for an individual file and the columns are:
#' [file]         name of the file.
#' [rawCount]     read count of input data.
#' [sortedCount]  read count of sortmerna output.
#' [sortRate]     ratio of reads remained after sortmerna (w.r.t rawCount).
#' [cleanedCount] read count of trimmomatic output.
#' [cleanedRate]  ratio of reads remained after trimmomatic (w.r.t rawCount).
#'
#' @importFrom logger log_info
#' @importFrom foreach foreach
#' @importFrom foreach %dopar%
#'
reportForClean <- function(rawPath, cleanedPath, sortedPath=NULL) {
    clnNames <- list.files(cleanedPath, pattern=paste0("*.fastq.gz"))
    clnPureNames <- substr(clnNames, 1, nchar(clnNames) - nchar(".fastq.gz"))

    df <- list()

    log_info("Start counting reads from raw files ...")
    df$file <- clnPureNames
    df$rawCount <- foreach(file=clnPureNames, .combine=c) %dopar% {
        count.fastq.reads(file.path(rawPath, paste0(file, "*.fastq.gz")))
    }
    if (! is.null(sortedPath)) {
        log_info("Start counting reads from intermediate files ...")
        df$sortedCount <- foreach(file=clnPureNames, .combine=c) %dopar% {
            withoutRnName <- substr(file, 1, nchar(file) - 3)
            RnName <- substr(file, nchar(file) - 1, nchar(file))
            count.fastq.reads(file.path(sortedPath, paste0(withoutRnName, "_sortmerna.", RnName, ".fq.gz")))
        }
        df$sortedRate <- df$sortedCount / df$rawCount
    }
    log_info("Start counting reads from cleaned files ...")
    df$cleanedCount <- foreach(file=clnNames, .combine=c) %dopar% {
        count.fastq.reads(file.path(cleanedPath, file))
    }
    df$cleanedRate <- df$cleanedCount / df$rawCount
    return(data.frame(df))
}
