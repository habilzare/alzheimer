#!/usr/bin/env Rscript
suppressMessages(library(logger))
suppressMessages(library(argparse))
suppressMessages(library(doParallel))

log_info("parsing command arguments")
parser <- ArgumentParser(description="Annotate a vcf.gz file")
parser$add_argument('-i', '--input-path',
                    help="Path to the vcf.gz files")
parser$add_argument('-s', '--pattern', default=".*",
                    help="regex for file names to be processed in this run [default: %(default)s]")
parser$add_argument('-p', '--parallelism', default=1,
                    help="Number of files to be processed concurrently")
parser$add_argument('-o', '--output-path', required=TRUE,
                    help="Output path")
parser$add_argument('-d', '--db-path',
                    help="Path to the database file")
parser$add_argument('-b', '--build-ver',
                    help="Build version to pass to annovar")
parser$add_argument('-f', '--protocol-f',
                    help="ProtocolF to pass to annovar")
parser$add_argument('-g', '--protocol-g',
                    help="ProtocolG to pass to annovar")
parser$add_argument('--java-options', required=TRUE,
                    help="java options to use with gatk")
parser$add_argument('--log-level', default="INFO",
                    help="One of ERROR, WARNING, INFO, DEBUG to specify the log level [default: %(default)s]")

args <- parser$parse_args(commandArgs(trailingOnly=TRUE))
log_threshold(args[['log_level']])

log_debug("Getting file names to process and creating the cluster (if required)")
prepared <- BiggSeq::prepareProcessPatternScript(
    parallelism=args[['parallelism']],
    inputPath=args[['input_path']],
    inputPattern=paste0(args[['pattern']], "\\.vcf\\.gz$")
)
log_debug("Found these files to process: ", prepared[['fileNames']])

invisible(foreach(fileName=prepared[['fileNames']]) %dopar% {
    sink(split=TRUE, file=BiggSeq::teeLogFile(args[['output_path']], fileName))
    BiggSeq::annotateVCF(
        fileName=fileName,
        inputPath=args[['input_path']],
        outputPath=args[['output_path']],
        dbPath=args[['db_path']],
        buildVersion=args[['build_ver']],
        protocolG=args[['protocol_g']],
        protocolF=args[['protocol_f']],
        javaOptions=args[['java_options']]
    )
    sink()
    return()
})

if (args[['parallelism']] > 1) stopCluster(prepared[['cluster']])

logger::log_info("Done running table_annovar.")
