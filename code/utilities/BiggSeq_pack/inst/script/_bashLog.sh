declare -A _log_levels=([FATAL]=0 [ERROR]=1 [WARN]=2 [INFO]=3 [DEBUG]=4 [VERBOSE]=5)
declare -i _log_level=3
_date_format="+%y-%m-%d %H:%M:%S"
function set_log_level() {
    level="${1:-INFO}"
    _log_level="${_log_levels[$level]}"
}
function set_log_extra_fields() {
    _log_extra_fields="[$*] "
}
log_fatal()   { (( _log_level >= _log_levels[FATAL] ))   && echo "$(date "$_date_format") [FATAL] $_log_extra_fields$*"; }
log_error()   { (( _log_level >= _log_levels[ERROR] ))   && echo "$(date "$_date_format") [ERROR] $_log_extra_fields$*"; }
log_warning() { (( _log_level >= _log_levels[WARNING] )) && echo "$(date "$_date_format") [WARN ] $_log_extra_fields$*"; }
log_info()    { (( _log_level >= _log_levels[INFO] ))    && echo "$(date "$_date_format") [INFO ] $_log_extra_fields$*"; }
log_debug()   { (( _log_level >= _log_levels[DEBUG] ))   && echo "$(date "$_date_format") [DEBUG] $_log_extra_fields$*"; }
log_verbose() { (( _log_level >= _log_levels[VERBOSE] )) && echo "$(date "$_date_format") [VERB ] $_log_extra_fields$*"; }
