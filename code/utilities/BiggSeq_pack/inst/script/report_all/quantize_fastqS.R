#!/usr/bin/env Rscript
suppressMessages(library(logger))
suppressMessages(library(argparse))
suppressMessages(library(doParallel))

parser <- ArgumentParser(
    description="To report mappings from quantize task and gather count matrices for all samples.
                It will produce a csv files in the output folder.
                The name is 'info.csv' which is the dataframe which has samples as its rows
                and the columns are some fields extracted from json files.
                Also would run read.sailfish function which generates some Rdata files.")

parser$add_argument('-i', '--input-path', required=TRUE,
                    help="Output folder of the quantize task
                          which contains one folder for each sample")
parser$add_argument('-o', '--output-path',
                    help="The folder to put the results.
                          If not specified, the parent directory of input path.")
parser$add_argument('-p', '--parallelism', default=1,
                    help="Number of files to be processed concurrently")

args <- parser$parse_args(commandArgs(trailingOnly=TRUE))

log_info("Starting quantize report script ...")
inputPath <- args[['input_path']]
outputPath <- args[['output_path']]
if (is.null(outputPath)) outputPath <- dirname(inputPath)
## Sys.readlink here is necessary whenever we have symlinked the result folder.
linkedInput <- Sys.readlink(inputPath)
if (linkedInput != "") inputPath <- linkedInput

samples <- list.dirs(inputPath, full.names=FALSE, recursive=FALSE)

if (args[['parallelism']] > 1) {
    cluster <- makeCluster(args[['parallelism']], type="FORK", methods=FALSE, outfile="")
    registerDoParallel(cluster)
}

info <- foreach(sample=samples, .combine=rbind) %dopar% {
    meta <- jsonlite::read_json(file.path(inputPath, sample, "aux_info", "meta_info.json"))
    return(data.frame(meta[c("num_processed", "num_mapped", "percent_mapped")]))
}
info <- cbind(info, sample=samples)
write.csv(info, file=file.path(outputPath, "info.csv"), row.names=FALSE)

## Commented on account of read.sailfish
# quant <- foreach(sample=samples, .combine=rbind) %dopar% {
#     df <- read.csv(file.path(inputPath, sample, "quant.sf"), sep="\t")
#     df[['sample']] <- sample
#     return(df)
# }
# write.csv(quant, file=file.path(outputPath, "quant.csv"), row.names=FALSE)

BiggSeq::read.sailfish(
    pathIn=inputPath,
    fileName="quant.sf",
    what=c("TPM", "NumReads"),
    suffix="",
    pathOut=outputPath,
    doParsLog=FALSE,
    doPlotRates=FALSE
)

if (args[['parallelism']] > 1) stopCluster(cluster)

log_info("Done. Successfully created 'info.csv'")
