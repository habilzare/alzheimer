## This is not a perfect test because the generated mpi is not good enough.
## We need actual sortmerna indices and also cleaned fastq files
## or at least some fastq names in accordance with the clean task's output.
test_that("mpiQuantizeFastq successfully creates mpi file", {
    fixturePath <- getResource("tests/testthat/fixtures")
    configFile  <- file.path(fixturePath, "application.yaml")
    userConfig <- tryCatch(yaml::read_yaml(configFile), warning=function(.){})
    inputPath   <- file.path(fixturePath, "toy_fastq")
    outputPath  <- file.path(tempdir(), "quantize_test")
    parallelism <- list(
        files = 4,
        threads = 8,
        report = 4
    )
    params <- mpiQuantizeFastq(inputPath=inputPath,
                               outputPath=outputPath,
                               isSingleEnd=FALSE,
                               indexPath="/some/dummy/path",
                               parallelism=parallelism,
                               mpi=userConfig[['slurm']],
                               refsPath=userConfig[['refsPath']])

    expect_equal(params[['task']], "quantizeFastq")
    expect_true(file.exists(file.path(outputPath, "mpi", "qntz_all.mpi")))
})
