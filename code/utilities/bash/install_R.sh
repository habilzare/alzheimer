#!/usr/bin/env bash

usage()
{
cat << EOF
usage: $0 <cluster-name> [--r-version <r-version>] [--pcre2 <pcre2-version>]

---------------------------------------------------------------------
Mohsen S. Tabar start writing this script; 2023-07; to install R on TACC clusters.
---------------------------------------------------------------------

OPTIONS:
    <cluster-name> is one of 'frontera', or empty to get help.

    --r-version
    The version of R to be installed. default: '4.3.1'

    --pcre2 <pcre2-version>
    If required, the version of PCRE2 to install. default: '10.42'
EOF
}

if [ "$#" -eq "0" ]; then
  usage; exit 0
fi

INSTALL_FOLDER=${ZARE_STOCKYARDINSTALL}/$1; shift
USR_FOLDER=${INSTALL_FOLDER}/usr/local
R_VERSION="4.3.1"
PCRE2_VERSION="10.42"
CRAN_REPO="http://cran.us.r-project.org"

while [ "$#" -ne "0" ]; do
    case "$1" in
        --pcre2) PCRE2_VERSION="$2"; shift 2 ;;
        --r-version) R_VERSION="$2"; shift 2 ;;
        *) usage; exit 64 ;;
    esac
done

set -e

## Installing pcre2 if required
if [[ "$TACC_DOMAIN" == "frontera" ]]; then
  wget "https://github.com/PCRE2Project/pcre2/releases/download/pcre2-${PCRE2_VERSION}/pcre2-${PCRE2_VERSION}.tar.bz2"
  tar -xjf "pcre2-${PCRE2_VERSION}.tar.bz2"  && rm "pcre2-${PCRE2_VERSION}.tar.bz2"
  cd "pcre2-${PCRE2_VERSION}"
  ./configure --prefix="${USR_FOLDER}"
  make && make install && cd .. && rm -r "pcre2-${PCRE2_VERSION}"
fi

## Downloading R and installing it from source
curl -O "https://cran.rstudio.com/src/base/R-${R_VERSION/.*/}/R-${R_VERSION}.tar.gz"
tar -xf "R-${R_VERSION}.tar.gz" && rm "R-${R_VERSION}.tar.gz"
cd "R-${R_VERSION}" || exit
CPPFLAGS="-I${USR_FOLDER}/include" \
LDFLAGS="-L${USR_FOLDER}/lib" \
PKG_CONFIG_PATH="${USR_FOLDER}/lib/pkgconfig:${USR_FOLDER}/lib64/pkgconfig" \
./configure \
  --prefix="${INSTALL_FOLDER}/R/${R_VERSION}" \
  --enable-memory-profiling

make && make install && cd .. && rm -r "R-${R_VERSION}"

RSCRIPT_CMD=${INSTALL_FOLDER}/R/${R_VERSION}/bin/Rscript
## required to set the repo without interaction
REPOS_OPTION_R="options(repos=c(\"CRAN\"=\"${CRAN_REPO}\"))"
## prerequisites for making OncinfoUt
$RSCRIPT_CMD -e "$REPOS_OPTION_R; install.packages(c('devtools', 'logger', 'tictoc'))"
## making OncinfoUt
$RSCRIPT_CMD -e "$REPOS_OPTION_R; source('~/proj/alzheimer/code/utilities/makeOncinfoUt.R')"
## installing the oncinfo libraries
$RSCRIPT_CMD -e "$REPOS_OPTION_R; OncinfoUt::caLibraries()"
