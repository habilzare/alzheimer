## Habil wrote this script to make, install, and load the OncinfoUt package on 2023-05-22.
## Mohsen used logger and tictoc instead of print and Sys.time

## required by this script
for (pack in c("logger", "tictoc", "devtools"))
    if (!requireNamespace(pack, quietly=TRUE))
        install.packages(pack)

logger::log_info("Making the OncinfoUt package ...")
tictoc::tic("Ellapsed time")

## We need the following functions from the public alzheimer repository:
source("~/proj/alzheimer/code/Habil/utilities/make.package.R")
source("~/proj/alzheimer/code/utilities/installFromSource.R")
source("~/proj/alzheimer/code/utilities/installOncinfoUt.R")

## If you updated the package, you need to increase the version in DESCRIPTION, and then run:
installOncinfoUt()

## Calling:
library(OncinfoUt)

## Done
logger::log_info("Done making the OncinfoUt package.")
tictoc::toc()
