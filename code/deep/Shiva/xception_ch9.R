## Shiva copied this script which is from section 9.3.5.
### 2022-07-05


## Settings:
modelFile <- file.path(doCatPath,  "mini_xception.keras")

## Data
got <- get_cats_dogs_small(inPath=file.path(kaggleDataPath,"cats_vs_dogs_small"))
train_dataset <- got$train_dataset
validation_dataset <- got$validation_dataset
test_dataset <- got$test_dataset

data_augmentation <- keras_model_sequential() %>%
    layer_random_flip("horizontal") %>%
    layer_random_rotation(0.1) %>%
    layer_random_zoom(0.2)

inputs <- layer_input(shape = c(180, 180, 3))

x <- inputs %>%
    data_augmentation() %>%
    layer_rescaling(scale = 1 / 255) ## Donn't forget input scaling

x <- x %>%
    layer_conv_2d(32, 5, use_bias = FALSE)

for (size in c(32, 64, 128, 256, 512)) { ##apply series of
    ##convolutional blocks with increasing feature depth. Each block
    ##consists of two batch normalized depthwise separable convolution
    ##layers and a max-pooling layer

    residual <- x
    x <- x %>%
        layer_batch_normalization() %>%
        layer_activation("relu") %>%
        layer_separable_conv_2d(size, 3, padding = "same",
                                use_bias=FALSE) %>%

    layer_batch_normalization() %>%
    layer_activation("relu") %>%
    layer_separable_conv_2d(size, 3, padding = "same", use_bias = FALSE) %>%
    layer_max_pooling_2d(pool_size=3, strides=2, padding = "same")
    ##why using stride in max_pooling?

    residual <- residual %>%
        layer_conv_2d(size, 1, strides=2, padding = "same", use_bias = FALSE)

    x <- layer_add(list(x, residual))
}

outputs <- x %>%
    layer_global_average_pooling_2d() %>% ##Global average pooling operation for spatial data.
    layer_dropout(0.5) %>%
    layer_dense(1, activation = "sigmoid")

model <- keras_model(inputs, outputs)
png(file.path(chPath, "xception935.png"), width=4*480, height=10*480)
plot(model, show_shapes=TRUE, dpi=96*20)
dev.off()
                                                  

## Compile:
model %>%
    compile(
        loss = "binary_crossentropy",
        optimizer = "rmsprop",
        metrics = "accuracy"
    )

## To save the best model:
callbacks <- list(
    callback_model_checkpoint(
        filepath=modelFile,
        save_best_only=TRUE,
        monitor="val_loss"
    )
)

## Fit:
hist1 <- fit(model, train_dataset,
             epochs = 50,
             validation_data = validation_dataset,
             callbacks = callbacks)
