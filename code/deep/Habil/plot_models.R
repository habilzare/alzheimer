## Habil wrote this script to plot some pretrained models using Keras.
## 2022-06-30.
## This script must be sourced from runall.R.


## Timing:
moTime <- Sys.time()
print("Plotting models started at:")
print(moTime)

## Where to save?
plotPath <- file.path(experimentPath, "models")
dir.create(plotPath)


## Plots:
conv_base <- application_vgg16(weights = "imagenet",
  include_top = FALSE, input_shape = c(180, 180, 3))
png(file.path(plotPath, "vgg16_model.png"), width=480*4, height=480*10)
plot(conv_base, show_shapes=TRUE, dpi=96*10)
dev.off()


## Plots:
png(file.path(plotPath, "vgg19_model.png"), width=480*4, height=480*10)
plot(application_vgg19(), show_shapes=TRUE, dpi=96*10)
dev.off()


## Plot the Inception model for fun:
png(file.path(plotPath, "Inception_model_v3.png"), width=480*16, height=480*40)
plot(application_inception_v3(), dpi=96*20)
dev.off()

## Plot the Inception model for fun:
png(file.path(plotPath, "xception.png"), width=480*16, height=480*40)
plot(application_xception(), dpi=96*20)
dev.off()


## Concluding:
print("Plotting models took:")
print(Sys.time()-moTime)


