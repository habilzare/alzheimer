import argparse
import logging
import os
from pathlib import Path

import dacite
import numpy as np
import optuna
import torch
from ignite.engine import create_supervised_evaluator
from ignite.metrics import Loss
from matplotlib import pyplot as plt
from optuna import Trial
from tensorboard.backend.event_processing.event_accumulator import EventAccumulator

import deep
import deep_zipo
from assay.task import CellTask
from deep import TRN, VAL
from deep_zipo import ZiPoModel, ZiPoModelConfig
from oncinfo_ut import configure_basic_logging, add_experiment_arguments_to_parser, \
    load_experiment_config_from_args


def main(args_list):
    parser = argparse.ArgumentParser(description="""
    This script trains one ZiPo model.
    """)
    add_experiment_arguments_to_parser(parser)
    parser.add_argument('--cuda-id', type=int,
                        help="The id of the cuda device to use. Starts from zero.")
    parser.add_argument('--hyper-iter', type=int,
                        help="Number of iterations for hyperparameter search.")
    parser.add_argument('--hyper-storage',
                        help="The path to the storage for hyperparameter study. See optuna.load_study()")
    parser.add_argument('--seed', type=int,
                        help="The seed to use (and for reproducibility of the results).")
    parser.add_argument('--enc-sizes', nargs='+', type=int,
                        help="Sizes for the encoder layers.")
    parser.add_argument('--dec-sizes', nargs='+', type=int,
                        help="Sizes for the decoder layers.")
    parser.add_argument('--enc-residuals', type=int,
                        help="Number of residuals to use for the encoder part.")
    parser.add_argument('--enc-activation',
                        help="Name of the activation function to use in the encoder.")
    parser.add_argument('--reg-zero', type=float,
                        help="Coefficient of zero probability regularization term in the loss function.")
    parser.add_argument('--reg-weights', type=float,
                        help="Coefficient of weight regularization term in the loss function.")
    parser.add_argument('--reg-mse-rate', type=float,
                        help="Coefficient of MSE regularization term in the loss function.")
    parser.add_argument('--use-nb', action='store_true',
                        help="If set, we use Negative Binomial instead of Poisson.")
    parser.set_defaults(use_nb=False)
    args = parser.parse_args(args_list)

    experiment_config = load_experiment_config_from_args(args)
    configure_basic_logging(experiment_config)

    res_path = Path(experiment_config['experimentPath'])
    ## It is important not to import torch before choosing the cuda device
    if args.cuda_id is not None:
        os.environ['CUDA_VISIBLE_DEVICES'] = str(args.cuda_id)
    device = deep.get_device()
    logging.info(f"Using torch device: {device}")

    task = CellTask(experiment_config, "Train", device)
    logging.info(f"after removal #Genes: {task.reader.csr_mat.shape[1]}")
    model_config = dacite.from_dict(ZiPoModelConfig, task.config.model)

    if args.seed is not None:
        task.config.seed = args.seed
    if args.enc_sizes is not None:
        model_config.encoder_channel_sizes = args.enc_sizes
    if args.dec_sizes is not None:
        model_config.decoder_channel_sizes = args.dec_sizes
    if args.enc_residuals is not None:
        model_config.encoder_residuals = args.enc_residuals
    if args.enc_activation is not None:
        model_config.encoder_activation = args.enc_activation
    if args.reg_zero is not None:
        model_config.regularization['l2_zero_prob'] = args.reg_zero
    if args.reg_weights is not None:
        model_config.regularization['weights'] = args.reg_weights
    if args.reg_mse_rate is not None:
        model_config.regularization['mse_rate'] = args.reg_mse_rate
    if args.use_nb:
        model_config.use_nb = True

    if args.hyper_iter is not None:
        ## create (or load a previous) study
        study = optuna.load_study(study_name="zip", storage=args.hyper_storage)
        hypers = task.config.train.hyper_search

        ## objective function for hyperparameter search
        def objective(trial: Trial):
            if 'reg_weights' in hypers:
                model_config.regularization['weights'] =\
                    trial.suggest_float('reg_weights', *hypers['reg_weights'], log=True)
            if 'reg_l2_zero_prob' in hypers:
                model_config.regularization['l2_zero_prob'] =\
                    trial.suggest_float('reg_l2_zero_prob', *hypers['reg_l2_zero_prob'], log=True)
            if 'encoder_channel_sizes' in hypers:
                suggestion = trial.suggest_categorical('encoder_channel_sizes', hypers['encoder_channel_sizes'])
                model_config.encoder_channel_sizes = [int(size) for size in suggestion.split(' ')]
            model_name = train_and_save(task, model_config, res_path)
            acc = EventAccumulator(str(res_path / "tb_logs" / model_name)).Reload()
            return acc.Scalars('val/NLL')[-1].value
        ## optimize the hyperparameters
        study.optimize(objective, n_trials=args.hyper_iter, show_progress_bar=True)
    else:
        train_and_save(task, model_config, res_path)


def train_and_save(task: CellTask, model_config: ZiPoModelConfig, res_path):
    """
    To train and save a ZiPo model
    :param task
    :param model_config The config object for the ZiPo model
    :param res_path The path to the result folder
    """
    deep.set_global_seed(task.config.seed)
    task.reset_typ_indices()
    model = ZiPoModel(task.reader.gene_meta.shape[0], model_config).to(task.device)

    model_name = task.config.name_pattern.format(**{
        'enc_residuals': model_config.encoder_residuals,
        'enc_activation': model_config.encoder_activation,
        'reg_zero': model_config.regularization['l2_zero_prob'],
        'reg_weights': model_config.regularization['weights'],
        'dist': "ZINB" if model_config.use_nb else "ZIP",
        'enc_sizes_joined': '_'.join([str(x) for x in model_config.encoder_channel_sizes]),
        'dec_sizes_joined': '_'.join([str(x) for x in model_config.decoder_channel_sizes]),
        'enc_sizes_last': model_config.encoder_channel_sizes[-1],
    })
    model_name = f"{model_name}-{task.config.seed}"

    val_metrics = {
        'Loss': Loss(model.loss_function),
        'NLL': Loss(model.loss_log_likelihood),
        'MSErate': Loss(ZiPoModel.loss_mse_rate),
        'MSENZrate': Loss(ZiPoModel.loss_mse_nonzero_rate),
        'RegW': Loss(model.loss_reg_weights),
    }

    deep_zipo.train_and_log(
        train_config=task.config.train,
        model=model,
        val_metrics=val_metrics,
        val_data_loader=task.get_data_loader(VAL, model),
        trn_data_loader=task.get_data_loader(TRN, model),
        log_dir=str(res_path / "tb_logs" / model_name),
    )

    ## save the model
    (res_path / "models").mkdir(parents=True, exist_ok=True)
    with open(res_path / "models" / model_name, 'wb+') as file_:
        torch.save(model, file_)

    logging.info("Done!")
    return model_name


def weight_reg_metrics(res_path, task_params, model, name, seed, loader):
    first_layer = model.encoder[0][0]
    result = {}
    weights = first_layer.weight.detach()
    weights_abs_max_normalized = weights.abs() / weights.abs().max(axis=1)[0][:, None]
    weights_abs_mean_normalized = weights.abs() / weights.abs().mean(axis=1)[:, None]
    if seed == 6224:
        plt.hist(np.log10(weights_abs_max_normalized.numpy().reshape(-1)), bins=200)
        plt.title(f"{name} max normalized")
        plt.savefig(str(res_path / f"{name}_max_normalized.png"))
        plt.clf()
        plt.hist(np.log10(weights_abs_mean_normalized.mean(axis=0).numpy()), bins=200)
        plt.title(f"{name} mean normalized")
        plt.savefig(str(res_path / f"{name}_mean_normalized.png"))
        plt.clf()
    for eps in task_params['portion_cuts']:
        weights_abs_mean_normalized[weights_abs_mean_normalized < eps] = 0
        result[f"gene_importance_cut{eps:f}".rstrip('0')] = \
            ",".join(weights_abs_mean_normalized.mean(axis=0).numpy().astype('str'))
        result[f"portion_less{eps:f}".rstrip('0')] = (weights_abs_max_normalized < eps).float().mean().item()

    original_weights = first_layer.weight
    weights_abs_normalized = original_weights.abs() / original_weights.abs().max(axis=1)[0][:, None]
    val_metrics = {
        'NLL': Loss(model.loss_log_likelihood),
        'MSE_rate': Loss(ZiPoModel.loss_mse_rate),
    }
    for eps in task_params['eval_cuts']:
        weights = torch.clone(original_weights.detach())
        weights[weights_abs_normalized < eps] = 0
        first_layer.weight.data = weights
        val_evaluator = create_supervised_evaluator(model, metrics=val_metrics)
        val_evaluator.run(loader)
        for _name, _value in val_evaluator.state.metrics.items():
            result[f"{_name}_cut{eps:f}".rstrip('0')] = _value
        logging.info(f"{name}, {seed}, {eps}")
    return result


def zero_reg_metrics(res_path, task_params, model, name, seed, loader):
    val_metrics = {
        'mean_lib_size': Loss(lambda y, x: torch.mean(torch.exp(ZiPoModel.get_log_lib_size(y, x)))),
        'mean_log_lib_size': Loss(lambda y, x: torch.mean(ZiPoModel.get_log_lib_size(y, x))),
        'ratio_nz_floor': Loss(lambda y, x: torch.mean((ZiPoModel.get_log_absolute_rate(y, x) >= 0).float())),
        'ratio_nz_round': Loss(lambda y, x: torch.mean((ZiPoModel.get_log_absolute_rate(y, x) >= -0.301).float())),
        'mean_zero_prob': Loss(lambda y, _: torch.mean(ZiPoModel.get_zero_prob(y))),
        'mean_zero_rate': Loss(lambda y, _: -torch.mean(ZiPoModel.get_non_zero_rate(y))),
    }
    val_evaluator = create_supervised_evaluator(model, metrics=val_metrics)
    val_evaluator.run(loader)
    logging.info(f"{name}, {seed}")
    return val_evaluator.state.metrics
