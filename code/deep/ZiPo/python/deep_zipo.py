import logging
from dataclasses import dataclass
from typing import Optional

import pandas as pd
import torch
from ignite.contrib.handlers import TensorboardLogger
from ignite.contrib.handlers.tensorboard_logger import GradsScalarHandler
from ignite.engine import create_supervised_trainer, create_supervised_evaluator, Events
from ignite.handlers import global_step_from_engine
from ignite.metrics import RunningAverage
from torch import nn

from assay.in_data import scanpy_transform
from deep import get_activation_layer
from deep.trainings import TrainConfig, get_optimizer, setup_schedulers, apply_extra_multiplicative


@dataclass
class ZiPoModelConfig:
    """
    This is the config class for the architecture of a ZiPo model.
    """
    use_nb: bool  ## negative binomial
    encoder_residuals: int  ## number of residual from the beginning layers to the latents
    encoder_channel_sizes: list
    encoder_batch_normalization: bool
    encoder_activation: str  ## name of the activation function, like Sigmoid, ReLU, GELU, ...
    decoder_channel_sizes: list
    decoder_activation: str  ## similar to encoder_activation

    regularization: dict  ## dictionary containing regularization coefficients. Supported keys:
    ## 1- l2_zero_prob: L2 for the inferred zero inflation
    ## 2- weights: Look at the formula on the paper. The loss is only for the first encoder layer
    ## 3- mse_rate: MSE between inferred and actual absolute rates

    lib_hidden_sizes: Optional[list]  ## sizes of the hidden layers for library size prediction
    ## If none, we won't infer library size

    input_preparation: Optional[list]  ## list of dictionaries of consecutive transform | normalization(s)
    ## to be applied to the raw input before feeding to the model.
    ## Each one must have 'method' and 'params' keys
    ## which then we pass the params to the corresponding method on scanpy.pp.
    ## NOTE that you can not have any filtration here.
    ## The size of the resulting matrices must be the same as the original ones.


class ZiPoModel(nn.Module):
    """
    This class encapsulates a ZiPo model with the given config
    """

    def __init__(self, input_channel_size: int, conf: ZiPoModelConfig):
        super().__init__()
        self.regularization = conf.regularization
        self.use_nb = conf.use_nb
        if conf.input_preparation is not None:
            self.input_preparation = conf.input_preparation

        def get_activated_linear(inp_size, out_size, activation):
            if conf.encoder_batch_normalization:
                layers = [
                    nn.Linear(inp_size, out_size, bias=False),
                    nn.BatchNorm1d(num_features=out_size),
                    get_activation_layer(activation)
                ]
            else:
                layers = [
                    nn.Linear(inp_size, out_size, bias=True),
                    get_activation_layer(activation)
                ]
            return nn.Sequential(*layers)

        encoder_channel_sizes = conf.encoder_channel_sizes.copy()
        encoder_channel_sizes.insert(0, input_channel_size)
        self.embedding_dim = encoder_channel_sizes[-1]
        self.encoder = nn.Sequential(*[
            get_activated_linear(encoder_channel_sizes[i - 1], encoder_channel_sizes[i], conf.encoder_activation)
            for i in range(1, len(encoder_channel_sizes))
        ])
        self.encoder_residuals = nn.ModuleList()
        num_residuals = conf.encoder_residuals
        if num_residuals > len(encoder_channel_sizes) - 3:
            num_residuals = len(encoder_channel_sizes) - 3
        for i in range(num_residuals):
            if conf.encoder_batch_normalization:
                self.encoder_residuals.append(nn.Sequential(
                    nn.Linear(encoder_channel_sizes[i+1], self.embedding_dim, bias=False),
                    nn.BatchNorm1d(num_features=self.embedding_dim),
                    get_activation_layer(conf.encoder_activation)
                ))
            else:
                self.encoder_residuals.append(nn.Sequential(
                    nn.Linear(encoder_channel_sizes[i+1], self.embedding_dim, bias=True),
                    get_activation_layer(conf.encoder_activation)
                ))
        decoder_channel_sizes = conf.decoder_channel_sizes.copy()
        decoder_channel_sizes.insert(0, encoder_channel_sizes[-1])
        self.decoder = nn.Sequential(*[
            get_activated_linear(decoder_channel_sizes[i - 1], decoder_channel_sizes[i], conf.encoder_activation)
            for i in range(1, len(decoder_channel_sizes))
        ])
        if conf.lib_hidden_sizes[0] != 0:
            lib_hidden_sizes = conf.lib_hidden_sizes.copy()
            lib_hidden_sizes.insert(0, self.embedding_dim)
            self.log_lib_size = nn.Sequential(*[
                get_activated_linear(lib_hidden_sizes[i - 1], lib_hidden_sizes[i], conf.encoder_activation)
                for i in range(1, len(lib_hidden_sizes))
            ], nn.Linear(lib_hidden_sizes[-1], 1))
        self.nonzero_rate = nn.Linear(decoder_channel_sizes[-1], input_channel_size)
        self.log_lambda = nn.Sequential(nn.Linear(decoder_channel_sizes[-1], input_channel_size), nn.LogSoftmax(dim=-1))
        if conf.use_nb:
            self.log_dispersion = nn.Linear(decoder_channel_sizes[-1], input_channel_size)

    def prepare_input(self, csr_mat):
        if self.input_preparation is not None:
            csr_mat = scanpy_transform(csr_mat, self.input_preparation)
        return csr_mat

    def forward(self, x):
        if len(self.encoder_residuals) == 0:
            encoded = self.encoder(x)
        else:
            temp = [x]
            for layer in self.encoder:
                temp.append(layer(temp[-1]))
            encoded = temp[-1]
            for i, layer in enumerate(self.encoder_residuals):
                encoded = encoded + layer(temp[i+1])
            encoded = encoded / (1 + len(self.encoder_residuals))
        decoded = self.decoder(encoded) if len(self.decoder) > 0 else encoded
        log_dispersion = self.log_dispersion(decoded) if self.use_nb else None
        log_lib_size = self.log_lib_size(encoded) if hasattr(self, 'log_lib_size') else None
        return log_lib_size, encoded, self.nonzero_rate(decoded), self.log_lambda(decoded), log_dispersion

    @staticmethod
    def get_log_lib_size(y, x):
        """Calculates the predicted log library size from the output of the model."""
        return y[0] if y[0] is not None else torch.log(x.sum(axis=1, keepdim=True))

    @staticmethod
    def get_log_absolute_rate(y, x):
        """Calculates the predicted log absolute rate from the output of the model."""
        return ZiPoModel.get_log_lib_size(y, x) + y[3]

    @staticmethod
    def get_log_dispersion(y):
        """Calculates the predicted log dispersion from the output of the model."""
        return y[4]

    @staticmethod
    def get_non_zero_rate(y):
        """Calculates the predicted non-zero-inflation rate from the output of the model."""
        return y[2]

    @staticmethod
    def get_zero_prob(y):
        """Calculates the predicted zero-inflation probability from the output of the model."""
        return torch.sigmoid(-ZiPoModel.get_non_zero_rate(y))

    def loss_log_likelihood(self, y, x):
        """Calculates the log likelihood for zero-inflated Poisson model
        for prediction 'y' and input 'x'."""
        nonzero_rate = ZiPoModel.get_non_zero_rate(y)
        log_dispersion = ZiPoModel.get_log_dispersion(y)
        log_mean = ZiPoModel.get_log_absolute_rate(y, x)
        mean = torch.exp(log_mean)
        if self.use_nb:
            rate_for_zero = torch.exp(log_dispersion) * nn.functional.logsigmoid(log_dispersion - log_mean)
        else:
            rate_for_zero = -mean
        ## res = torch.log(torch.exp(rate_for_zero) + torch.exp(-nonzero_rate))
        ## But it is better to write it in the following way for numerical stability:
        res = - nonzero_rate - nn.functional.logsigmoid(- rate_for_zero - nonzero_rate)
        if self.use_nb:
            log_q = nn.functional.logsigmoid(log_mean - log_dispersion)
        else:
            log_q = log_mean
        common = rate_for_zero[x != 0] + x[x != 0] * log_q[x != 0] - torch.lgamma(1 + x[x != 0])
        if self.use_nb:
            dispersion = torch.exp(log_dispersion[x != 0])
            res[x != 0] = common + torch.lgamma(x[x != 0] + dispersion) - torch.lgamma(dispersion)
        else:
            res[x != 0] = common
        res = res + nn.functional.logsigmoid(nonzero_rate)
        return -res.mean()

    @staticmethod
    def loss_mse_rate(y, x):
        inferred_abs_rate = torch.exp(ZiPoModel.get_log_absolute_rate(y, x))
        return nn.functional.mse_loss(inferred_abs_rate, x)

    @staticmethod
    def loss_mse_nonzero_rate(y, x):
        inferred_abs_rate = torch.exp(ZiPoModel.get_log_absolute_rate(y, x))
        return nn.functional.mse_loss(inferred_abs_rate[x > 0], x[x > 0])

    @staticmethod
    def loss_reg_l2_zero_prob(y, x):
        return (ZiPoModel.get_zero_prob(y) ** 2).mean()

    @staticmethod
    def loss_reg_dispersion(y, x):
        return torch.exp(ZiPoModel.get_log_dispersion(y)).mean()

    def loss_reg_weights(self, y=None, x=None):
        first_weights = self.encoder[0][0].weight
        return (first_weights.abs().mean(axis=1) / (first_weights ** 2).mean(axis=1).sqrt()).mean()

    def loss_total(self, y, x):
        result = self.loss_log_likelihood(y, x) + \
                 self.regularization['l2_zero_prob'] * ZiPoModel.loss_reg_l2_zero_prob(y, x)
        if self.regularization['mse_rate'] > 0:
            result = result + self.regularization['mse_rate'] * ZiPoModel.loss_mse_rate(y, x)
        return result

    def loss_function(self, y, x):
        result = self.loss_total(y, x)
        if self.regularization['weights'] > 0:
            result = result + self.regularization['weights'] * self.loss_reg_weights()
        return result

    @torch.no_grad()
    def as_embedding(self, x):
        """Applies the encoder part only on its input and returns the corresponding embedding

        :Authors: Mohsen
        :param x: The input tensor that must be embedded (row-wise)
        :return: An embedding module with zero as pad and embedded values of `x` rows
        """
        result = nn.Embedding(num_embeddings=1 + x.shape[0], embedding_dim=self.embedding_dim, padding_idx=0)
        result.weight[1:] = self.encoder(x)
        return result


def train_and_log(train_config: TrainConfig, model: ZiPoModel,
                  val_metrics, val_data_loader, trn_data_loader, log_dir):
    """Train a ZiPoModel and log using tensorboard

    :Authors: Mohsen
    :param train_config:
    :param model: An object of type ZiPoModel. The weights would be changed after training.
    :param log_dir: The directory to put tensorboard logs
    :param trn_data_loader: A data loader for train data. It must return a tuple for each index (input, label).
    :param val_data_loader: A data loader for validation data. It must return a tuple for each index (input, label).
    :param val_metrics: A dictionary with string keys and ignite Metric values. It must have the key 'Loss'.
    """
    train_config = apply_extra_multiplicative(train_config)

    ## prepare the parameters for the optimizer
    ## We do this to set bigger learning rate for the log_lib_size layers
    params_list = []
    for name, layer in model.named_children():
        if name == 'log_lib_size':
            params_list.append({'params': layer.parameters(), 'lr': 10 * train_config.optimizer.params['lr']})
        else:
            params_list.append({'params': layer.parameters()})
    optimizer = get_optimizer(params_list, train_config.optimizer)

    ## create and configure trainer and evaluator
    trainer = create_supervised_trainer(model, optimizer, model.loss_function)
    trainer.logger.setLevel(logging.ERROR)
    val_evaluator = create_supervised_evaluator(model, metrics=val_metrics)
    val_evaluator.logger.setLevel(logging.ERROR)
    RunningAverage(output_transform=lambda x: x).attach(trainer, 'Loss')
    trainer.add_event_handler(Events.EPOCH_COMPLETED, lambda engine: val_evaluator.run(val_data_loader))

    ## converts losses to a string to print them in terminal during training
    def losses_to_string():
        metrics = {**{'trn/'+key: val for key, val in trainer.state.metrics.items()}, **val_evaluator.state.metrics}
        return f" lr: {optimizer.state_dict()['param_groups'][0]['lr']:.2e}   " \
               f"{pd.DataFrame(metrics, index=[0]).to_string(index=False, header=False)}"
    trainer.add_event_handler(Events.EPOCH_COMPLETED(every=train_config.log_every),
                              lambda engine: logging.info(f"Ep{engine.state.epoch:2d}:{losses_to_string()}"))

    ## create tensorboard logger and attach suitable handlers to it
    tb_logger = TensorboardLogger(log_dir=log_dir)
    tb_logger.attach_output_handler(trainer, event_name=Events.ITERATION_COMPLETED, tag="training",
                                    output_transform=lambda loss: {"batch_loss": loss})
    tb_logger.attach_output_handler(val_evaluator, event_name=Events.COMPLETED, tag="val",
                                    metric_names="all", global_step_transform=global_step_from_engine(trainer))
    for i, layer in enumerate(model.encoder):
        tb_logger.attach(trainer, event_name=Events.EPOCH_COMPLETED,
                         log_handler=GradsScalarHandler(layer, tag=f"enc_lay{i}"))
    tb_logger.attach_opt_params_handler(trainer, event_name=Events.EPOCH_STARTED, optimizer=optimizer)

    ## schedulers and early stopping
    setup_schedulers(val_evaluator=val_evaluator, trainer=trainer, optimizer=optimizer, train_config=train_config)

    ## run the training and close the tensorboard logger
    trainer.run(trn_data_loader, max_epochs=train_config.epochs)
    tb_logger.close()
