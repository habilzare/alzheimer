import os
import tempfile
import unittest
from pathlib import Path

import dacite
import deep
import optuna
import zipo_train
from assay.task import CellTask
from deep_zipo import ZiPoModelConfig, ZiPoModel, train_and_log
from ignite.metrics import Loss
from oncinfo_ut import load_experiment_config, configure_basic_logging


class Test(unittest.TestCase):

    def setUp(self) -> None:
        proj_base = Path("~").expanduser()
        self.config_path = str(proj_base / "proj/alzheimer/code/deep/ZiPo")
        self.experiment = "1970-01-01_test"
        self.experiment_config = load_experiment_config(self.config_path, self.experiment)
        configure_basic_logging(self.experiment_config)

    def test_zipo_train(self):
        device = deep.get_device()
        cell_task = CellTask(self.experiment_config, "Train", device)
        cell_task.reset_typ_indices()
        num_genes = cell_task.reader.gene_meta.shape[0]
        model_config = dacite.from_dict(ZiPoModelConfig, cell_task.config.model)
        model = ZiPoModel(num_genes, model_config).to(device)
        print(model)
        val_metrics = {
            "Loss": Loss(model.loss_log_likelihood),
            "Z": Loss(model.loss_reg_l2_zero_prob),
        }
        train_and_log(
            train_config=cell_task.config.train,
            model=model,
            val_metrics=val_metrics,
            val_data_loader=cell_task.get_data_loader(deep.VAL, model),
            trn_data_loader=cell_task.get_data_loader(deep.TRN, model),
            log_dir=str(tempfile.mkdtemp()),
        )

    def test_hyper(self):
        res_path = self.experiment_config['experimentPath']
        storage_file = f"{res_path}/hyper.db"
        if os.path.exists(storage_file):
            os.remove(storage_file)
        storage = f"sqlite:///{storage_file}"
        optuna.create_study(study_name="zip", storage=storage)
        zipo_train.main(['--config-path', self.config_path,
                         '--experiment', self.experiment,
                         '--hyper-iter', "3",
                         '--hyper-storage', storage])


if __name__ == '__main__':
    unittest.main()
