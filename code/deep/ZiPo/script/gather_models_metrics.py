import argparse
import logging
import os
from pathlib import Path

import pandas as pd

import deep
import zipo_train
from assay.task import CellTask
from oncinfo_ut import get_task_params, configure_basic_logging, \
    add_experiment_arguments_to_parser, load_experiment_config_from_args

## Mohsen started developing this script on 2023-04-15.
## This script gets an experiment and save some metrics as a csv file.


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="gather_models_metrics")
    add_experiment_arguments_to_parser(parser)
    parser.add_argument('--cuda-id', type=int)
    parser.add_argument('--seeds', nargs='+', type=int)
    parser.add_argument('--output')
    args = parser.parse_args()

    experiment_config = load_experiment_config_from_args(args)
    configure_basic_logging(experiment_config)
    res_path = Path(experiment_config['experimentPath'])
    configure_basic_logging(experiment_config)
    task_params = get_task_params(experiment_config, 'ModelMetrics')
    ## It is important not to import torch before choosing the cuda device
    if args.cuda_id is not None:
        os.environ['CUDA_VISIBLE_DEVICES'] = str(args.cuda_id)
    device = deep.get_device()
    task = CellTask(experiment_config, "Train", device)
    if 'batch_size' in task_params:
        task.config.train.loader['batch_size'] = task_params['batch_size']
    metrics = task_params['metrics']
    if metrics == 'weight_reg':
        metrics = zipo_train.weight_reg_metrics
    elif metrics == 'zero_reg':
        metrics = zipo_train.zero_reg_metrics
    else:
        metrics = None

    temp = []
    for model_name in os.listdir(str(res_path / "models")):
        name, seed = model_name.split('-')
        seed = int(seed)
        if seed not in args.seeds:
            continue
        deep.set_global_seed(seed)
        task.reset_typ_indices()
        import torch
        model = torch.load(res_path / "models" / model_name, map_location=device)
        loader = task.get_data_loader(deep.VAL, model)
        temp.append({'name': name, 'seed': seed,
                     **metrics(res_path, task_params, model, name, seed, loader)})
    df = pd.DataFrame(temp)
    df.to_csv(res_path / f"{args.output}.csv")
    logging.info("Done!")
