import argparse
import logging
import os
from pathlib import Path

import pandas as pd

import deep
from assay.task import CellTask
from oncinfo_ut import get_task_params, configure_basic_logging, \
    add_experiment_arguments_to_parser, load_experiment_config_from_args

## Mohsen started developing this script on 2023-07-17.
## This script get an experiment and save its tensorboard metrics as csv files.
## It takes some time to load the metrics for many runs.


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="gather_latents")
    add_experiment_arguments_to_parser(parser)
    args = parser.parse_args()

    experiment_config = load_experiment_config_from_args(args)
    configure_basic_logging(experiment_config)
    res_path = Path(experiment_config['experimentPath'])
    (res_path / "latents").mkdir(parents=True, exist_ok=True)
    configure_basic_logging(experiment_config)
    task_params = get_task_params(experiment_config, 'ModelLatents')
    ## It is important not to import torch before choosing the cuda device
    if hasattr(args, "cuda_id"):
        os.environ['CUDA_VISIBLE_DEVICES'] = str(args.cuda_id)
    device = deep.get_device()
    task = CellTask(experiment_config, "Train", device)
    if 'batch_size' in task_params:
        task.config.train.loader['batch_size'] = task_params['batch_size']

    for model_name in os.listdir(str(res_path / "models")):
        import torch
        model = torch.load(res_path / "models" / model_name, map_location=device)
        loader = task.get_data_loader(None, model)
        with torch.no_grad():
            result = torch.empty(0, model.embedding_dim)
            for batch in loader:
                result = torch.vstack([result, model.encoder(batch[0])])
        df = pd.DataFrame(result.numpy())
        df.to_csv(res_path / "latents" / f"{model_name}.csv", header=False, index=False)
        logging.info(f"Done for model {model_name}")
    logging.info("Done!")
