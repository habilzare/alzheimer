import argparse
from itertools import product

## Mohsen started developing this script on 2023-04-04.
## This script generates suitable switches to pass to train.py script with xargs.


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--num-gpu-cores', type=int, required=True)
    parser.add_argument('--hyper-iter-total', type=int)
    parser.add_argument('--hyper-storage')
    parser.add_argument('--hyper-parallel', type=int)
    parser.add_argument('--seed', nargs='+', type=int)
    parser.add_argument('--enc-sizes', nargs='+')
    parser.add_argument('--dec-sizes', nargs='+')
    parser.add_argument('--enc-residuals', nargs='+', type=int)
    parser.add_argument('--enc-activation', nargs='+')
    parser.add_argument('--reg-zero', nargs='+', type=float)
    parser.add_argument('--reg-weights', nargs='+', type=float)
    parser.add_argument('--dist', nargs='+')
    parser.add_argument('--reg-mse-rate', type=float)  ## in case of dist=ZINB
    args = parser.parse_args()

    def nos(arr):  ## none or self
        return [None] if arr is None else arr

    if args.hyper_iter_total is not None:
        num_tasks = args.hyper_parallel * args.num_gpu_cores
        hyper_iter = int(args.hyper_iter_total/num_tasks)
        for i in range(num_tasks):
            args_to_pass = f"--cuda-id {i % args.num_gpu_cores} --hyper-iter {hyper_iter}"
            args_to_pass += f" --hyper-storage {args.hyper_storage}"
            print(args_to_pass)
            i += 1
    else:
        i = 0
        for (enc_sizes, dec_sizes, enc_residuals,
             enc_activation, reg_zero, reg_weights,
             dist,
             seed) in \
            product(nos(args.enc_sizes), nos(args.dec_sizes), nos(args.enc_residuals),
                    nos(args.enc_activation), nos(args.reg_zero), nos(args.reg_weights),
                    nos(args.dist),
                    nos(args.seed)):
            args_to_pass = f"--cuda-id {i%args.num_gpu_cores}"
            if seed is not None:
                args_to_pass += f" --seed {seed}"
            if enc_sizes is not None:
                args_to_pass += f" --enc-sizes {enc_sizes}"
            if dec_sizes is not None and dec_sizes != "0":
                args_to_pass += f" --dec-sizes {dec_sizes}"
            if enc_residuals is not None:
                args_to_pass += f" --enc-residuals {enc_residuals}"
            if enc_activation is not None:
                args_to_pass += f" --enc-activation {enc_activation}"
            if reg_zero is not None:
                args_to_pass += f" --reg-zero {reg_zero}"
            if reg_weights is not None:
                args_to_pass += f" --reg-weight {reg_weights}"
            if dist is not None:
                if dist == "ZINB":
                    args_to_pass += f" --use-nb --reg-mse-rate {args.reg_mse_rate}"
            print(args_to_pass)
            i += 1
