import argparse
import os
from multiprocessing import Pool
from pathlib import Path

import pandas as pd
from oncinfo_ut import load_experiment_config, configure_basic_logging, add_experiment_arguments_to_parser, \
    load_experiment_config_from_args
from tensorboard.backend.event_processing.event_accumulator import EventAccumulator


## Mohsen started developing this script on 2023-04-04.
## This script get an experiment and save its tensorboard metrics as csv files.
## It takes some time to load the metrics for many runs.


def parallel_load(x):
    name, seed = x[0].split('-')
    acc = EventAccumulator(x[1]).Reload()

    ## The evolution of metrics over the epochs
    df = pd.DataFrame()
    df['val/Loss'] = [scalar.value for scalar in acc.Scalars('val/Loss')]
    ## We need to restrict other metrics to #epochs in case of no early stopping
    epochs = len(df)
    for tag in acc.Tags()['scalars']:
        if tag != 'val/Loss' and tag != 'training/batch_loss' and 'grads_norm' not in tag:
            df[tag] = [scalar.value for scalar in acc.Scalars(tag)[0:epochs]]
    df['name'], df['seed'] = name, seed
    df['epoch'] = df.index

    ## The final basic metrics
    nll_loss = acc.Scalars('val/NLL')
    mse_rate_loss = acc.Scalars('val/MSErate')
    weights_loss = acc.Scalars('val/RegW')
    final_dict = {
        'name': name,
        'seed': seed,
        'wall_time': int(nll_loss[-1].wall_time - nll_loss[0].wall_time),
        'epochs': nll_loss[-1].step - nll_loss[0].step,
        'NLL': nll_loss[-1].value,
        'MSE_rate': mse_rate_loss[-1].value,
        'WeightsLoss': weights_loss[-1].value,
    }
    return df, final_dict


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="gather_tb_metrics")
    add_experiment_arguments_to_parser(parser)
    args = parser.parse_args()

    experiment_config = load_experiment_config_from_args(args)
    configure_basic_logging(experiment_config)
    res_path = Path(experiment_config['experimentPath'])

    inp = [(run, str(res_path / "tb_logs" / run))
           for run in os.listdir(str(res_path / "tb_logs"))]
    with Pool(8) as pool:
        _temp = pool.map(parallel_load, inp)

    df_final = pd.DataFrame([x[1] for x in _temp])
    print(df_final.groupby(['name']).size())
    df_final.to_csv(res_path / "tb_metrics_final.csv")

    df_evolution = pd.DataFrame()
    for temp, _ in _temp:
        df_evolution = pd.concat([df_evolution, temp])
    df_evolution.to_csv(res_path / "tb_metrics_evolution.csv")
