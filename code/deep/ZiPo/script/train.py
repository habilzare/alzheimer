import sys

import zipo_train

## Run this script with -h through python to see available options for it
if __name__ == '__main__':
    zipo_train.main(sys.argv[1:])
