This folder contains different scripts for running ZiPo on single-cell RNA-seq data and
reproducing the results that were presented in the paper with the title
"A deep neural network to de-noise single-cell RNA sequencing data"

## How to run ZiPo?
1. To run this pipeline,
   You need to have a basic knowledge of "yaml" files.
   Write a config for your experiment, e.g. using 2023-04-04_base.yaml and
   2023-04-04_invest_sizes.yaml in the "experiments" folder.
   This is particularly important for the code to know your cluster settings. 
1. You can use a "SLURM" cluster equipped with "Singularity" (see task Train)
   or run it on your local machine (see task TensorboardMetrics)
1. Clone this repository on "~/proj", whether it is your local machine or SLURM.
   Then checkout to the tag "ZiPo-2023" using this command:

        git checkout ZiPo-2023

1. If you want to run the training locally, you will need to first run
   (in your virtual or conda environment)

        pip install -r ~/proj/alzheimer/code/deep/ZiPo/requirements.txt

1. Source "runall.R" to save task slurm files.
   Mind the comments at the top of the runall.R script.
   Now run or submit the slurm files according to the instructions you get.
   When a docker image is needed, it will be automatically pulled.
1. Mohsen has written script/report.R to gather the results of his experiments for the 2023 paper.
   Anybody else is welcomed to write similar scripts that put results from different experiments together.

## Available tasks

### Hyper
This task is optional and is for creating a new sqlite database to use with 'Optuna'
and hyperparameter search. Available keys for `train.hyper_search` parameter in config file:
- encoder_channel_sizes: list of strings (space separated sizes)
- reg_l2_zero_prob: [min, max] of l2_zero_prob regularization with log uniform selection.
- reg_weights: [min, max] of weights regularization with log uniform selection.

### Train
This task is about training a ZIP (or ZINB) model.
The config is of class `assay.task.TaskConfig`
and the `model` field is of class `deep_zipo.ZiPoModelConfig`.

The main function running is `zipo_train.train_and_save`.
The main script for this task is `train.py`.
It loads task config file, but at the same time,
has the ability to override some options with its input switches.

To loop over some parameters or searching in a hyperparameter space,
we have the `generate_zip_train.py` script.
We use `param_generation` from the config file to configure it.
We use `name_pattern` field for the model naming pattern.
It uses the usual string interpolation
(variables inside curly braces would be replaced by their values)
and this is the list of available variables:
- enc_residuals
- enc_activation
- reg_zero: regularization['l2_zero_prob'] parameter
- reg_weights: regularization['weights'] parameter 
- dist: "ZINB" if use_nb else "ZIP"
- enc_sizes_joined: joined encoder_channel_sizes by '_'
- dec_sizes_joined: joined decoder_channel_sizes by '_'
- enc_sizes_last: last element of encoder_channel_sizes

### TensorboardMetrics
This task is about extracting dataframes to plot from tensorboard logs.
The main script is `generate_tb_metrics.py`.

### ModelMetrics
This task is optional and about extracting dataframes to plot from predictions or weights of the models.
The main script is `generate_model_metrics.py`.
