plot.sigVar <- function(sigVar, naccList, subNacCdr, barPath, variablePath){
    print("Loading all CDRs..:")
    missings <- c("", NA, -4, -4.4, 8, 9,
                                             99,
                                             99.9, 999, 9999,
                                             88,
                                             88.8, 888, 8888)
    all <- get(load(file.path(dataPath,
    "NACC/nacc_cdr0_FALSE_24_TRUE.RData"), v=TRUE)) ##subNacc
    for(v1 in names(sigVar)){
##browser()
        sigVar[[v1]] <- sigVar[[v1]][-which(rownames(sigVar[[v1]])
        %in% "braak"), ]##noplot for braak
        resiSamples <- naccList[[v1]][naccList[[v1]]["braak"]==1,
                                      "NACCID"]
        subNaccRes <- subNacCdr[subNacCdr[, "NACCID"] %in% resiSamples,]
        ## ^Res samples

        subSample <- subNacCdr[!subNacCdr[, "NACCID"] %in% resiSamples, ]
        ##^all cdr=0 sample without res (e.g. non-resilient)
        ##browser()
        for(i1 in rownames(sigVar[[v1]])){
            ##var <- c("NACCAC", "NACCACEI", "NACCADEP", "NACCLIPL", "NACCNSD")
            ##for(i1 in var){
            print(i1)
            ##browser()
            dat <- tab <- NULL
            ## Can be improved.
            subNaccRes <- subNaccRes[!subNaccRes[, i1]%in% missings, ]
            others <- all[!all[, i1]  %in% missings, ]
            if(i1=="NACCBRAA"){
                others <- others[!others[, i1] %in% 7,]
            }
            others <- others[others[, "CDRGLOB"]>0,]
            cleanSub <- subSample[!subSample[, i1]  %in% missings, ]
            dat <- rbind(subNaccRes[, c("NACCID", i1)],
                         cleanSub[, c("NACCID", i1)],
                         others[, c("NACCID", i1)])
            dat <- cbind(dat, Group=c(rep("Res", nrow(subNaccRes)),
                                      rep("non-res", nrow(cleanSub)),
                                      rep("CDR>0", nrow(others))))

            tab <- data.frame(table(dat[, i1], dat[, "Group"]))
            colnames(tab) <- c(i1, "Group", "Percentage")
            ##browser()
            gg1 <- ggplot(tab, aes(x=factor(Group, level=c("Res", "non-res",
                                                           "CDR>0")),
                                   fill=factor(tab[, i1],
                                               level=rev(levels(tab[, i1]))),
                                   y=Percentage))+
                geom_bar(position="fill", stat="identity")
            gg1 <- gg1+labs(fill=i1, x="Groups")+
            ggtitle(paste("pvalue:", signif(sigVar[[v1]][i1,
            "pvalue"], digits=3)))
            ggsave(file.path(barPath, paste0(v1, "_", i1, "_", "bar.png")),
                   width=8, height=8, units="in", dpi=300)
            ##graphics.off()
##browser()
            g1 <- ggplot(dat, aes(x=factor(Group, level=c("Res", "non-res",
                                                          "CDR>0")),
                                  y=dat[, i1],
                                  fill=Group))+geom_violin()
            g1 <- g1+
                geom_boxplot(width=.2, outlier.size=3)+
                stat_summary(geom="crossbar",
                             color="black", size=1.5,
                             width=0.6, fatten=0.5,
                             fun.data=
                                 function(x){c(y=median(x),
                                               ymin=median(x),
                                               ymax=median(x))})+
                labs(y=i1)
            g1 <- g1+theme(axis.text=element_text(size=10),
                           axis.title=element_text(size=12))+
                theme(legend.position = "none")
            ggsave(file.path(variablePath, paste0(v1, "_", i1, "_", "comparison.png")),
                   width=8, height=8, units="in", dpi=300)
            graphics.off()
        }
    }
    }
