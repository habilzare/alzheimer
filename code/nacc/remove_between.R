## Shiva wrote this script to remove 90-95% upper and lower samples
## and then do the select features.
## Always run this tasks with predLevel=0.9
##2021-27-04
predLevel <- 0.9
p9Path <- file.path(experimentPath, "predLevel_0.9",
                    paste0("cdrZero_", doZeroCdr),
                    paste0("less24_", do24))
p8Path <- file.path(experimentPath, "predLevel_0.8",
                    paste0("cdrZero_", doZeroCdr),
                    paste0("less24_", do24))
resil9 <- get(load(file=file.path(p9Path, paste0("resilients_ID_",
                                                 predLevel,".RData"))))
resis9 <- get(load(file=file.path(p9Path, paste0("resistants_iD_",
                                                 predLevel, ".RData"))))

resil8 <- get(load(file=file.path(p8Path, "resilients_ID_0.8.RData")))
resis8 <- get(load(file=file.path(p8Path, "resistants_ID_0.8.RData")))

resilDiff <- setdiff(resil8$NACCID, resil9$NACCID) ##those that are in
##resil8$NACCID but not in resil9$NACCID
resisDiff <- setdiff(resis8$NACCID, resis9$NACCID)
diff <- data.frame(diff=c(resilDiff, resisDiff))
print(paste("Number of differences from 90 to 95 is", nrow(diff)))
##^the difference is 46
##save(diff, file=file.path(path24, "diff.RData"))

load(file=file.path(p9Path, "subNacc_uds_np.RData"), v=TRUE) ##subNacCdr
subNacCdr <- subNacCdr[!subNacCdr[, "NACCID"] %in% diff$diff,]
if(jamieVars){
    subNacCdr <- subNacCdr[, c("NACCID", "NACCINT", "NACCDAGE", "NPHIPSCL", "NPTDPB", "NPTDPC",
                               "NPTDPE")]
}


##browser()
if(!doZeroCdr){
    p9PathTr <- file.path(experimentPath, "predLevel_0.9",
                          "cdrZero_TRUE/less24_TRUE")
    file.copy(from=file.path(p9PathTr, "age_plots",
                             "nacc_NACCBRAA_phase1.RData"),
              to=file.path(p9Path, "age_plots"))
    resData <- get(load(file=file.path(p9Path, "age_plots",
                                       "nacc_NACCBRAA_phase1.RData")))
}else{
    resData <- get(load(file=file.path(p9Path, "age_plots",
                                       "nacc_NACCBRAA_phase1.RData")))
}

##when doZeroCdr=FALSE move the RData for resisitant/resilitant from true folder here.
resData <- resData[!resData[, "NACCID"] %in% diff$diff,] ##remove the
##borderline from resData as well.
##warning("The next two commands are redundant!!")
resilient <- resData[resData[, "Resilient"]== TRUE, c("NACCID",
                                                      "Resilient")]
resistant <- resData[resData[, "Resistant"]== TRUE, c("NACCID",
                                                      "Resistant")]

compResis <- cbind(subNacCdr, braak=0)
ind1 <- which(compResis[, "NACCID"] %in% resistant[, "NACCID"])
compResis[ind1, "braak"] <- 1

compResil <- cbind(subNacCdr, braak=0)
ind2 <- which(compResil[, "NACCID"] %in% resilient[, "NACCID"])
compResil[ind2, "braak"] <- 1

##Separate resilient sample in nacc with cdr=0
##subNaccResil <- subNacCdr[subNacCdr[, "NACCID"] %in% resilient$NACCID, ]
##print("Dim of resilient before narrowing down the feature:")
##print(dim(subNaccResil))
##save(subNaccResil, file=file.path(path24, "resilients.RData"))
##subNaccResis <- subNacCdr[subNacCdr[, "NACCID"] %in% resistant$NACCID, ]
##print("Dim of resistants before narrowing down the feature:")
##print(dim(subNaccResis))
##save(subNaccResis, file=file.path(path24, "resistants.RData"))
##browser()
naccList <- list(subNaccResis=compResis, subNaccResil=compResil)
vars <- list(subNaccResis=NULL, subNaccResil=NULL)
sf <- select.features(naccList=naccList, vars=vars,
                      path1=zeroCdrPath, missVal=missVal)
##sigVar <- list(subNaccResis=sf[["subNaccResis"]][sf[["subNaccResis"]][,
##                                                                   "pvalue"]<0.##01,], subNaccResil=sf[["subNaccResil"]][sf[["subNaccResil"]][,
##                                                         "pvalue"]<0.01,])
##browser()
sigVar <- sf
if(doZeroCdr){
    p1 <- plot.sigVar(sigVar=sigVar, naccList=naccList,
                      subNacCdr=subNacCdr,
                      barPath=barPath, variablePath=variablePath)
}
if(FALSE){
    v2 <- data.frame(vars=c("NPHIPSCL", "NPTDPB", "NPTDPC", "NPTDPE", "braak"))
    rownames(v2) <- v2[, "vars"]
    v2 <- cbind(v2, pvalue=1:nrow(v2))
    sigVar <- list(subNaccResil=v2, subNaccResis=v2)
    p2 <- plot.sigVar(sigVar=sigVar, naccList=naccList,
    subNacCdr=subNacCdr, barPath=zeroCdrPath,
    variablePath=zeroCdrPath)
    ##NPHIPSCL(Hippocampal sclerosis):19
    ##NPTDPB:21
    ##NPTDPC:20
    ##NPTDPE (Distribution of TDP-43 immunoreactive inclusions—neocortex):20

}
