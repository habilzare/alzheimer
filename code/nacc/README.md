Version 1.0

The content:

- This directory contains different scripts for analyzing NACC dataset and reproducing the results that were presented in:
Walker et al. "The Spectrum of Alzheimer Disease Neuropathologic Change in Cognitively Normal Individuals" .

How to access the data?

- UDS and NP data should be requested and downloaded from NACC (https://naccdata.org/).

In order to reproduce the results for paper, please follow these steps: 

-In order to reproduce the results for paper, please follow these steps: 
1. The name of the data file is "investigator_nacc50_2020-09-11.csv". After accessing the data please rename 
it to this name and put this file in data/2020-10-20 folder. Also, please add the three dictionary files 
(i.e., rdd-gen.csv, rdd-np.csv, uds3-rdd.csv) in the folder.

2. Open R in the code directory and just run this line:
source("reproduce.R")

3. The pipeline will generate a results folder. You can access the plots, csv, etc files in the "2022-08-12_reproduceNACC" folder.

The analysis was done based on three filters:

1. Prediction level of 0.9 and 0.8 for identifying resilient and resistant

2. CDR=0 and all CDRs

3. NACCINT< 24 months or all including cases regardles of NACCINT

