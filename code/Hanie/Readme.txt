--Hanie, 2017-07-19


>>DESeq2analysis.R : does all tasks related to DE analysis for small-RNAs(micro,pi and esi RNA) and mRNA.

>>FastaCreate.R : creates fasta file from RNA-seq 3' and 5' UTR files. It removes those RNAs which do not match to any UTR sequences and concatenate them as a unique fasta file.  

>>IsoDivider.R : is based on DR. Frosts email about isoforms.
This script divided DE transcripts into up-regulated and downregulated groups. Then mapped these transcripts to those isoforms that DR. Frost mentioned.

>>motfiDEtable.R : It takes 2016-09-12_full.csv(all DEtranscripts) and res.txt(transcripts which has common motif in their sequence) as input and makes a full table as its output. The output table contains information about DEtranscripts which have the common motif in their sequences.

>>drosophila_pir.R : it creates fasta file for piRNA data + creates additional tables for piRNAs and flamenco region.  

>>uSeqesiRNA.R : creates uSeqId.RData from esiRNA.fasta. The uSeqId is input for DESeq2analysis.R (when using esiRNA data) 
>> practice.R : I wrote this based on Dr. Zare's scripts to clean each fastq file on server (one job per each fastq files). 

>>ForHeatmaps.R : creates pheatmaps just for RNA-seq data. (phase #1)

>>converter.R : Does intermediate steps for UTR analysis(not important) 

>>tRna_labeledAs_piRna.R : identfies those tRNAs that are wrongly named piRNAs in our data. we wanted to check if any of DE piRNAs are tRNAs or not.(based on the email from Dr. Frost, 2017-07-25). Hanie modified it on 2017-Sep-12. This scripts should be source before DESeq2analysis.R

>>heatmaps_for_paper.R : creates pheatmaps for paper based on Dr. Frost's email on 2017-07-27,heatmap request.

>>Hanie downloaded labels for mRNA from http://flybase.org/static_pages/downloads/ID.html.
The file name is transposableElementsNames.tsv and it is accesible from oncinfo 
In order to get DE transpolsable elements real names :

1) load ~/proj/alzheimer/result/Drosophila_2016-09-15/RNA/mapped/de/resAdjPvalue.RData in an R session
2) from rownames of resAdjPvalue, get the list of transposale elements names. Their names start with FBti. 
3) got to : http://flybase.org/static_pages/downloads/ID.html
4) Enter the list of DE names in the "Enter IDs" box.
5) check "as tab-seprated file" instead of "as HTML table"
5) click on "select fields"
6) check "symbol"
7) click Get field data
8) copy contents of the result page and save it as a transposableElementsNames.tsv 

>> human.R: Tests the hypotheses on TEs using the DE results from human
   AD data. 2017-08-31 --Habil.

>> cerebellum_cortex.R: Compares the DE TEs in different tissues of
	human AD and PSP.  2017-09-03 --Habil.

>> human_pheatmaps_for_paper.R creates heatmaps for human data(2 sheets, 3 disease, 3 different heatmaps). It should be source from 
pheatmaps_for_paper.R.  2017-09-08 --Hanie

>> Nanostring.R plots the TEs fold change measured using RNA-Seq
   vs. Nanostring. This plot is needed in the revised Nature
   Neuroscience paper. 2018-04-25 --Habil.

