## Hanie wrote this script on 2017-09-07 
## It creates pheatmaps for human data.

library(Pigengene)
print("All conditions and tissues in the Mayo dataset. The following answers will be ignored.")
source("../Habil/settings.R") ## To know deResultFolderName.

##time
print(paste("Creating heatmaps for human started at : " ,Sys.time()))

##base input path 
inputPath <- "~/proj/alzheimer/result/Mayo_2017-06-28/Taner_U01_Human_RNAseq"

## cortex or cerebellum
tissues<-dir(path = inputPath,pattern = "Taner_U01_Human")
names(tissues)<-gsub(tissues,pattern = "Taner_U01_Human_",replacement = "")
inputPath<-file.path(inputPath,tissues,"mapped/de", deResultFolderName)
names(inputPath)<-names(tissues)

for(t1 in names(inputPath)){
    ## psp-ctrl or ad-ctrl
    conds <- dir(inputPath[t1],pattern ="^AD-vs-Control$|^PSP-vs-Control$")
    for(c1 in conds){
        ## files
        load(file.path(inputPath[t1],c1,"counts.RData"),v=TRUE)## counts
        load(file.path(inputPath[t1],c1,"resAdjPvalue.RData"),v=TRUE) ##resAdjPvalue
        load(file.path(inputPath[t1],c1,"colData.RData"),v=TRUE) ##colData
        
        ##up or downregulated TEs
        upRegulate<-rownames(resAdjPvalue[which(resAdjPvalue$log2FoldChange>0),])
        upRegulate<-upRegulate[order(upRegulate)]
        downRegulate<-rownames(resAdjPvalue[which(resAdjPvalue$log2FoldChange<0),])
        downRegulate<-downRegulate[order(downRegulate)]
        all<-c(upRegulate,downRegulate)
        tegroups<-list("upRegulate"=upRegulate,"downRegulate"=downRegulate,"all"=all)
        
        ##res folder
        resPath<-file.path(inputPath[t1],c1,"heatmaps")
        dir.create(resPath,recursive = TRUE,showWarnings = FALSE)
        
        ## creates heatmap for each of tegroups elements(up,down,all)
        for(teNames in names(tegroups)){
            ##annotation
            annRow<-as.data.frame(colData[colnames(counts),])
            rownames(annRow)<-colnames(counts)
            colnames(annRow)<-"condition"
            
            ## order samples based on what Dr. Frost asked: ctrl,disease
            samplesConditions <- unique(annRow[,"condition"])
            diseaseName<-samplesConditions[samplesConditions!="Control"]
            levels(colData)<-c("Control",diseaseName)
            ctrlSamples <- rownames(colData)[which(colData[,"condition"]=="Control")]
            diseaseSamples <- rownames(colData)[which(colData[,"condition"]!="Control")]
            orderSamples<-c(ctrlSamples,diseaseSamples)
            counts<-counts[,orderSamples]
            
            annRow<-as.data.frame(annRow[orderSamples,])
            rownames(annRow)<-orderSamples
            colnames(annRow)<-"condition"
            
            ##png
            pngFile <-file.path(resPath,paste0(teNames,".png"))
            png(filename =pngFile,width = 1000,height = 2400)
            pheatmap.type(scale(t(counts[tegroups[[teNames]],])),cellwidth = 20,cellheight = 20,
                          border_color="black",
                          fontsize= 17,annRow =annRow,cluster_cols = FALSE, conditions=c("Control","AD","PSP"))
            dev.off()
            print(paste("A heatmap with the following numbers was saved at:", pngFile))
            print(table(annRow))
        }
    }
}

##time
print(paste("Heatmaps creation finished at : " , Sys.time()))
