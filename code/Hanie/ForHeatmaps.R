## Plots the heatmaps of gene expressions for the selected genes.
library(DESeq2)
library(MASS)
library(pheatmap)
library(RColorBrewer)
library(gplots)

## Files:
pvalueFile <- "~/proj/alzheimer/result/Drosophila_2016-09-15/RNA/mapped/de/resAdjPvalue.RData"
tpmFile <- "~/proj/alzheimer/result/Drosophila_2016-09-15/RNA/mapped/quant.sf_TPM.RData"
resPath <- dirname(pvalueFile)

## Data:
load(pvalueFile) ## resAdjPvalue
load(tpmFile) ## sailfish
tmp <- sailfish
num <- nrow(resAdjPvalue)


## for top ten genes
jpeg(file.path(resPath,'TopTen.jpg'))
selected <- rownames(resAdjPvalue)
TopTen<-selected[1:10]
pheatmap( scale(t(tmp[TopTen,])))
dev.off()

## for ten bottom genes
jpeg(file.path(resPath,'TenBottom.jpg'))
TenBottom<-selected[(num-10):num]
pheatmap( scale(t(tmp[TenBottom,])))
dev.off()

## for all genes
jpeg(file.path(resPath,'AllGenes.jpg'))
pheatmap( scale(t(tmp[selected,])),show_colnames=FALSE)
dev.off()

