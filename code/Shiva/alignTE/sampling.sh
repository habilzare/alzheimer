## Shiva wrote this script to get a sample from 96 fastq zipped files on
## 2020-31-01. She is going to get 2500 reads from each file.

cd ~/proj/alzheimer/data/human/Frost/snDNA_TE_2020-01-24/Frost_B_12182019/
for i in *.fastq.gz; do
zcat < "$i"|head -n 10000 > ~/proj/alzheimer/result/human_TE/sampling/2020-01-31/"$i"_10K.fastq
done
##remove fastq.gz from file name
for filename in *.fastq; do
    [ -f "$filename" ] || continue;
    mv "$filename" "${filename//fastq.gz/}"; done
##creat index
cd ~/bwa
bwa index -a bwtsw ~/proj/alzheimer/data/human/fasta/hg19.fa 

##make shortcut of each read and QC
cd  ~/proj/alzheimer/result/human_TE/sampling/2020-01-31/QC
ln -s ../*.fastq .
fastqc *.fastq


## Trimmomatic
cd ../TR
ln -s ../*.fastq .
for f in *_R1*; do
java -jar ~/Trimmomatic-0.39/trimmomatic-0.39.jar PE $f ${f/_R1/_R2}
-baseout ${f/_R*/.fastq} HEADCROP:30 LEADING:30 TRAILING:30 MINLEN:80



## align with human reference genome
