#!/bin/bash
## Mohsen modified this script in 2022-07-23 a little.

usage()
{
    cat << EOF

usage: $0 [options]  <R1/fastq/file> <sam/output/path> <reference1> [<reference2>]
-----------------------------
Shiva Kazempour wrote this script to align fastq files to the reference.  
bwa and samtools must be available.
On the TACC clusters, they can be loaded by sourcing oncinfo_settings before running this script.
INPUT:
The first argument includes the absoloute path to, and the full name
of the forward fastq file. The file name must contain exactly one
occurance of R1.
The reference1 is human reference FASTA file.
The reference2 is TE FASTA file. 
The FASTA file must have been indexed using "bwa index" prior to
running this script.
OUTPUT:
The two aligned files from reference1 and reference2 will be saved in
"sam" directory.
 -----------------------------
OPTIONS:
   -b	Make bed files from the aligned reads.
   -s   Are the reads single-end?
   -t 	The number of threads. If not provided, the total number of threads 
        on the machine minus 1 will be automatically used. 
   -c   Clean the intermediate sam file after fixing the barcode issue. Set to false to disable using '-c false'.	
EOF
}

## Check the command:
if [ "$#" -le 2 ]; then
    usage    
    exit 1
fi

## Default arguments:
DOBED=false
SINGLEND=false
THREADS=$(lscpu -p | grep -c "^[0-9]")
THREADS=$((THREADS-1))
DOCLEAN=true

## Reading the input:
while getopts ":b:s:h:t:c:" OPT; do
    case "$OPT" in
    b) DOBED=$OPTARG;;
    s) SINGLEND=$OPTARG;;
    t) THREADS=$OPTARG;;
    t) DOCLEAN=$OPTARG;;
    h) usage;;
    *) usage;; 
    esac
done
shift "$((OPTIND-1))"

## Timing:
echo "align.sh started at: $(date)"

##basename
FILENAME1="$( basename "$1" )"
echo "FileName: $FILENAME1"
BASENAME1="${FILENAME1/_R1*/}"
echo "I detected the basename to be: $BASENAME1"
BASENAME2="${FILENAME1/_L00*/}"
echo "I detected the basename without lane number to be: $BASENAME2"
echo "Number of threads: $THREADS"
echo "Clean (i.e., delete) intermediate files? $DOCLEAN"

## Path:
##go to the first argument which is the fastq files path
cd "$( dirname "$1" )"
echo "I am at:" && pwd
echo "Making bed files? $DOBED"
REFERENCE1=$3
echo "Reference1 is at: $REFERENCE1"

##basename to reference1
FASTANAME1="$( basename "$3" )"
FASTANAME1="${FASTANAME1/.fasta/}"
FASTANAME1="${FASTANAME1/.fa/}"
echo "I detected the FASTA name to be: $FASTANAME1"

##Path to sam, bam and bed
SAM1=$2/${BASENAME1}_${FASTANAME1}.sam
echo "I will write the reads aligned to reference1 in: $SAM1"
SAMFIX1=$2/${BASENAME1}_${FASTANAME1}_fixed.sam
echo $SAMFIX1
##SAMAPPED1=$2/${BASENAME1}_${FASTANAME1}_mapped.sam
##BAR1=$2/${BASENAME1}_${FASTANAME1}_bar.txt
BAM1=$2/${BASENAME1}_${FASTANAME1}.bam

echo "$(date): Alignment with reference1 started..."
if [ "$SINGLEND" = true ]; then
    echo "Working on single-end reads..."
    bwa mem -t $THREADS -C $REFERENCE1 $1 > $SAM1
else
    echo "Working on paired-end reads..."
    bwa mem -t $THREADS -C $REFERENCE1 $1 ${1/_R1/_R2} > $SAM1
fi

##Correct the barcode sequence(BC) in sam file!!!
echo "$(date): Correcting barcodes using sed ..."
sed 's/\([1-2]\:N\:0\:[A-Z]*\)/BC\:Z\:\1/' $SAM1 > $SAMFIX1
echo "Was the first sam file created?"
echo "$( ls $SAM1 )" 
##samtools view -F4 $SAMFIX1 > $SAMAPPED1
## Exclude barcodes of aligned reads
##cat $SAMAPPED1 | awk -F: '{print $NF}' > $BAR1
echo "$(date): samtools ..."
samtools view --threads $THREADS -h -bS $SAMFIX1> $BAM1
echo "Was the first bam file created?"
echo "$( ls $BAM1 )"
echo "Removing original sam files..."
if [ "$DOCLEAN" = true ]; then
    echo "Removing: $SAM1"
    rm $SAM1
fi

if [ "$DOBED" = true ]; then
    echo "Making bed files..."
    BED1=$2/${BASENAME1}_${FASTANAME1}.bed
    echo "BED files are in: $BED1"
    BED1NOR=$2/${BASENAME1}_${FASTANAME1}_noR.bed
    echo "$(date): bamToBed ..."
    bamToBed -i $BAM1 > $BED1
    echo "Was the first bed file created?"
    echo "$( ls $BED1 )"
    sed 's/\/1//g' $BED1|sed 's/\/2//g' > $BED1NOR
    ##paste -d' ' $BED1NOR $BAR1 > $BEDBAR1
fi

## if reference 2 exists do the following.
if [ "$4" ]; then
    REFERENCE2=$4
    echo "Reference2 is at: $REFERENCE2"
    ##basename to reference2
    FASTANAME2="$( basename "$4" )"
    FASTANAME2="${FASTANAME2/.fasta/}"
    FASTANAME2="${FASTANAME2/.fa/}"
    echo "I detected the FASTA name to be: $FASTANAME2"

    ##Path to sam 
    SAM2=$2/${BASENAME1}_${FASTANAME2}.sam
    echo "I will write the reads aligned to reference2 in: $SAM2"
    SAMFIX2=$2/${BASENAME1}_${FASTANAME2}_fixed.sam
    echo $SAMFIX2
    ##SAMAPPED2=$2/${BASENAME1}_${FASTANAME2}_mapped.sam
    ##BAR2=$2/${BASENAME1}_${FASTANAME2}_bar.txt
    BAM2=$2/${BASENAME1}_${FASTANAME2}.bam
    BED2=$2/${BASENAME1}_${FASTANAME2}.bed
    ##BEDBAR2=$2/${BASENAME1}_${FASTANAME2}_barcoded.bed
    echo "BED files are in: $BED2"
    BED2NOR=$2/${BASENAME1}_${FASTANAME2}_noR.bed

    echo "$(date): Alignment with reference2 started using bwa mem ..."
    if [ "$SINGLEND" = true ]; then
        echo "Working on single-end reads ..."
        bwa mem -t $THREADS -C $REFERENCE2 $1 > $SAM2
    else
        echo "Working on paired-end reads ..."
        bwa mem -t $THREADS -C $REFERENCE2 $1 ${1/_R1/_R2} > $SAM2
    fi
    
    ##Correct the barcode sequence(BC) in sam file
    echo "$(date): Correcting barcodes using sed ..."
    sed 's/\([1-2]\:N\:0\:[A-Z]*\)/BC\:Z\:\1/' $SAM2 > $SAMFIX2
    echo "Was the first sam file created?"
    echo "$( ls $SAM2 )" 
    ##samtools view -F4 $SAMFIX2 > $SAMAPPED2
    ## Exclude barcodes of aligned reads
    ##cat $SAMAPPED2 | awk -F: '{print $NF}' > $BAR2
    echo "$(date): samtools ..."
    samtools view --threads $THREADS -h -bS $SAMFIX2> $BAM2
    echo "$(date): bamToBed ..."
    bamToBed -i $BAM2 > $BED2
    echo "Was the first bed file created?"
    echo "$( ls $BED2 )"
    echo "$(date): sed ..."
    sed 's/\/1//g' $BED2 |sed 's/\/2//g' > $BED2NOR
    ##paste -d' ' $BED2NOR $BAR2 > $BEDBAR2
    ## BED format is as follow:chrom, chrom start, chrom end, name, score,
    ## starnd and barcode sequence.

    ##Getting the intersection of two barcoded bed files:
    ##join -1 4 -1 4 $BED1NOR $BED2NOR > $JOINhg
    JOIN=$2/${BASENAME1}_intersect.txt
    UNIQUEJOIN=$2/${BASENAME1}_uniqueInter.txt
    echo "$(date): Intersecting using join ..."
    join -1 4 -2 4 -o "1.1,1.2,1.3,1.4,1.5,1.6,2.1,2.2,2.3,2.4,2.7" $BED1NOR $BED2NOR > $JOIN

    echo "$(date): Sorting ..."
    sort -u -t : -k 1,2 $JOIN > $UNIQUEJOIN
fi
## End time:
echo "align.sh finished at: $(date)"
