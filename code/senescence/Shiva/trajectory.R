## Shiva wrote this script to perform trajectory o senescent cells
## based three pathways of CSP, SIP, and SRP.
## 2024-06-27.
senTime <- Sys.time()
print(paste("Trajectory analysis started at", senTime))

if (dataset == "ROSMAP") {
    snRnaCountsFile <- rosPath$files["snRnaCountsFile"]
}
browser()
load(snRnaCountsFile, verbose=TRUE) ##snRnaCounts
sampleName <- colData(snRnaCounts)[,"TAG"]
colnames(snRnaCounts) <- sampleName
cellTypes <- colData(snRnaCounts)[, "broad.cell.type"]
names(cellTypes) <- sampleName
print(table(cellTypes))
print("Getting expression values...")


pathways <- paramsCom$pathways
senResults <- paramsCom$senResults
senResultsPath <- file.path(resPath, "Senescence", senResults,
                            dataset, "senescence/senescent")
##genes
genes <- seNftGenes[, "SRP"]
genes <- unlist(genes)
senGenes <- genes[genes!= ""]
senGenes <- senGenes[!is.na(senGenes)]
if(!is.null(paramsCom$deFile)){
    log_info("Including DE genes in trajectory analysis...")
    de <- data.frame(read_excel(file.path(resPath, paramsCom$deFile)))
    deGenes <- de[ ,"primerid"]
    senGenes <- c(senGenes, deGenes)
}
intsc <- intersect(senGenes, rownames(snRnaCounts))
log_info()
##cells
labelS <- get.intersect(ds=dataset, geneListPath=senResultsPath,
                           pathways=pathways,
                        getNeuron=paramsCom$doNeurons)$label
pureLabels <- labelS[!is.na(labelS)]

subSn <- snRnaCounts[, names(pureLabels)]
Data <- as.matrix(assay(subSn))
phenoData <- DataFrame(colData(subSn))
phenoData[, "labels"] <- pureLabels[rownames(phenoData)]
phenoData <- as.data.frame(phenoData)
## Create an AnnotatedDataFrame using the base data.frame
phenoAnn <- new("AnnotatedDataFrame", data = phenoData)
cds <- newCellDataSet(cellData=Data, phenoData=phenoAnn,
                      expressionFamily=negbinomial.size())
cds <- estimateSizeFactors(cds)

cds <- estimateDispersions(cds)
detGenes <- detectGenes(cds, min_expr=0.01)##only genes that are
##actively expressed above the background noise level are considered
print(dim(fData(detGenes)))
dispTab <- dispersionTable(detGenes)
orderingGenes <- subset(dispTab, dispersion_empirical > 1 &
                                 mean_expression >= 0.04)
cds <- setOrderingFilter(cds, orderingGenes$gene_id)
#orderingGenes <- subset(dispTab, gene_id %in% intsc)
##orderingGenes <- subset(dispTab, mean_expression >= 0.01)
##orderFilt <- setOrderingFilter(detGenes, orderingGenes) ##Sets the
##features (e.g. genes) to be used for ordering cells in pseudotime

reDim <- reduceDimension(cds, max_components=2, method='DDRTree')
orderedCells <- orderCells(reDim)

plot_cell_trajectory(orderedCells, color_by="State")
plot_genes_in_pseudotime(orderedCells, gene_list=c("CDKN2D", "MAP2K1", "RB1"))



