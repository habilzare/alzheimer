This folder contains different scripts for analyzing and validating ROSMAP snRna-Seq dataset at the Oncinfo lab. Using these scripts you can either run a subset of individual tasks of your choice (see A for an example), or alternatively (B) reproduce the exact results presented in:
  Dehkordi, Shiva Kazempour, et al. "Profiling senescent cells in human brains reveals neurons with 
  CDKN2D/p19 and tau neuropathology." Nature Aging (2021): 1-10.


A:

-To reproduce the unpublished decision trees useful for identification of senescent markers, some preliminaries are needed:
	a. Clone the alzheimer and dementia repositories in ~/proj/ (git clone https://bitbucket.org/habilzare/alzheimer.git).
	b. If you don't have access to the private dementia repository, sign up a bitbucket account and send your email to Habil for his consideration.
	c. It is expected that user have some knowledge of yaml language. 

Follow these steps:
	1. The corresponding experiment for reproducing decision trees is in "~/proj/alzheimer/code/senescence/Shiva/experiments/2024-06-24_rep_senTrees.yaml".
	2. If you need to change the settings of this experiment please make a copy to a new yaml file and modify the new file. You can find instructions and meaning of each setting in the yaml file.
	3. In order to be able to generate decision trees, you need to run tasks "downloaData", "prep", and "senescence". These tasks are already defined in 2024-06-24_rep_senTrees.yaml.
	4. This experiment is designed for ROSMAP dataset. In order to be able to run task downloaData successfully, make sure you have access to this data through synapse.
	5. Please open R in this directory (~/proj/alzheimer/code/senescence/Shiva directory) and source("runall.R"). 
	6. When the command prompt asks you to choose an experiment, please choose 2024-06-24_rep_senTrees or your new yaml file.
	7. The prompt command also asks which tasks you wish to run. Please select it accordingly.
	8. The results will be saved in ~/proj/alzheimer/result/Senescence/2024-06-24_rep_senTrees/ROSMAP



B:
  
- To reproduce the manuscript results,
  you first need to download and install the senescence_0.0.7.tar.gz R package, which is available
  in supplementary materials of the paper. We have tested the code in macOS using the package versions
  recorded in the 2021-07-12_21_41_58.RData file in this folder. The code should work fine 
  on a Unix machine. To use the code, we recommend this repository be cloned in ~/proj/.

  

- Running this analysis can take around 15 hours with a 2.9 GHz Intel Core i9, need about 100GB
  memory, and 8GB of storage space. You can check your memory by typing gc()[2,"limit (Mb)"] in your
  R session. If it's less than 102400 Mbs, please follow these instructions to increase your memory
  before starting the analysis:

	1. Open terminal and type: echo 'R_MAX_VSIZE=100Gb' >> ~/.Renviron
	2. Quit R sessions
	3. Please remove .Renviron after all the analysis is done by: rm ~/.Renviron


- In order to reproduce ALL the results, please open an R session in the senescence folder directory, and simply type the following command:
source("reproduce_p19.R")

- This command will do the analysis on four different datasets:
	ROSMAP: Mathy's dataset (discovery dataset). Make sure you have the rights to access ROSMAP data from synapse.org.
	WU: Zhou et al 
	scRna: Grubman et al from entorhinal 
	scEmbryo: Fan X et al from cerebral cortex 
	The last three datasets are validation datasets. The command mentioned above
	(i.e., source("reproduce_p19.R")) will download all the dataset automatically.

- What aspects does this analysis cover?
	1. Identifying senescent cells and the enriched cell types
	2. Identifying NFT cells and the enriched cell types.
	3. Investigates if senescent cells are NFT or if NFT cells are senescent.

- How to access the results?
	1. The results will be saved in "senescence/result/2021-07-06_reproduce folder.

	2. In this folder, there should be four sub folders for each dataset mentioned above. 

	3. If you want to access the results related to senescent cell types, please follow this
	path senescence/2021-07-06_reproduce/dataset/senescence/senescent/gene list/.
	
	4. cellType.png and numbers_across_cell_types.csv contain information about senescent cells
	and enriched cell types. weights.png and membership.csv show weights information for each
	gene in the list.
	
	5. If you want to access the results related to NFT cell types, please follow this path
	Senescence/2021-07-06_reproduce/dataset/senescence/nft/gene list/. The definition of files
	are the same as #3.
	
 
