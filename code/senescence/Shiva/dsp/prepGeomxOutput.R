prepGeomxOutput <- function(rawD, savePath, exc=NULL, sep=",",
                            mapping, logOffset){
    ## Shiva wrote this script to exctract important columns of geomx
    ## output for making target file.
    ## rawD: a named list of geomx data
    ## mapping: A named vector for separating values in the Segment.Tags row.
    ##^ Example:c(AT8="AT8[+-]$", BrainRegion="EC|CA1")
    ## sep : if you need to separate any rows based on "," , "|", etc.
    ## Set to NULL to disable separation. Otherwise, provide mapping.
    ## exc: which ROIs to exclude before making the target

    expData <- logExp <- targets <- c()
    for(r1 in names(rawD)){
        raw1 <- rawD[[r1]]
        rowExpr <- which(raw1[,"Custom.Segment.Name"] == "#Target Group")
        segment <- raw1[1:(rowExpr-1), ]
        rownames(segment) <- segment[,1]
        segment <- segment[, -c(1:4)] ##mainly empty columns
        rowsInc <- c("Custom Segment Name", "ROI (Label)",
                     "Segment (Name/ Label)", "Segment Tags",
                     "Scan Name", "ROI X Coordinate",
                     "ROI Y Coordinate")
        ##segment <- segment[rowsInc, ]
        rownames(segment)[rownames(segment) == "Scan Name"] <- "Cases"
        ##replace Scan Name with Cases
        segment["Cases",] <- sapply(segment["Cases",], function(x)
            gsub(" ", "_", x))
        segment["Segment (Name/ Label)",] <-
            sapply(segment["Segment (Name/ Label)",],
                   function(x)
                       gsub(" ", "_", x))
        ##browser()
        log_info("Creating suitable column names...")
        cols <- paste0(segment["Cases", ], "_", segment["ROI (Label)", ],
                       "_",
                       segment["Segment (Name/ Label)", ])
        ##cols <- sapply(cols, function(x) gsub(" ", "_", x))
        cols <- unname(cols) ##remove names or dinames
        print(paste("In total there are", unique(length(cols)),
                    "cells (AOIs)"))
        ##cols <- cols[-c(1:4)]
        target <- data.frame(t(segment))
        rownames(target) <- cols
        print(target[1:3,])
        if(any(grepl("ring1", rownames(target)))){
            rownames(target) <- gsub("ring1", "Ring", rownames(target))
        }

        ## Below till 72 can be a function: deComma.R
        ## named or unamed vector, named vector with column name and value
        ## of regular expression.
        ## parsing the Segment.Tags based on commas
        target[, "Segment.Tags"] <- sapply(target[, "Segment.Tags"], function(x)
            gsub(" ", ",", x))

        if(!is.null(sep)){
            log_info("Separating started...")
            if(is.null(mapping))
                stop("mapping cannot be NULL when sep is not NULL!")
            inSep <- target[, "Segment.Tags"]
            names(inSep) <- rownames(target)
            ## Adding columns made from Segment.Tags
            sep1 <- BiggSeq::separate(input=inSep, mapping=mapping, sep=sep)
            target <- cbind(target, sep1[rownames(target),])

            ## Relabeling the values of AT8 column
            target[rownames(target[target[, "AT8"] %in% "AT8-",]),
                   "AT8"] <- "At8Neg"
            target[rownames(target[target[, "AT8"] %in% "AT8+",]),
                   "AT8"] <- "At8Pos"
        }

        ## Making the expression matrix

        rowExpr <- which(raw1[,"Custom.Segment.Name"] == "#Target Group")
        colExpr <- which(raw1[rowExpr, ] == "Target name (display name)")
        expr <- raw1[(rowExpr+1):nrow(raw1), (colExpr+1):ncol(raw1)]

        rownames(expr) <- raw1[(rowExpr+1):nrow(raw1), colExpr]
        ##expr <- expr[, -which(colnames(expr)== "Target name (display name)" )]
        colnames(expr) <- cols

        ##expData1 <- as.data.frame(lapply(expr, as.numeric))
        expData1 <- as.matrix(expr)
        class(expData1) <- "numeric"
        ##colnames(expData1) <- colnames(expr)
        ##rownames(expData1) <- rownames(expr)
        print("Proteins*samples:")
        print(dim(expData1))
        print("targets: (samples*meta data)")
        print(dim(target))
        ##browser()
        expData <- rbind(t(expData1), expData[, colnames(expData1)])
        ##logExp <- rbind(t(logExp1), logExp[, colnames(logExp1)])
        targets <- rbind(target, targets[rownames(target),])
    }
    logExp <- log10(expData+logOffset)

    expData <- t(expData)
    logExp <- t(logExp)
    colnames(expData) <- gsub("ring1", "Ring", colnames(expData))
    colnames(logExp) <- gsub("ring1", "Ring", colnames(logExp))
    if(!is.null(exc)){
        rowsExclude <- rep(FALSE, nrow(targets))
        for(pattern in exc) {
            matches <- grepl(pattern, rownames(targets), fixed=TRUE)
            rowsExclude <- rowsExclude | matches
        }
        targets <- targets[!rowsExclude,]
        expData <- expData[, rownames(targets)]
        logExp <- logExp[, rownames(targets)]
    }

    ## QC:
    if(ncol(expData) != nrow(targets))
        stop("Number of samples in expression, ", ncol(expData), " does not agree with targets, ",
             nrow(targets))

    ## Save:
    save.if(expData, file=file.path(savePath, "expData.RData"))
    save.if(logExp, file=file.path(savePath, "logExp.RData"))
    save.if(targets, file=file.path(savePath, "targets.RData"))

    ## Output:
    return(list(targets=targets, expData=expData, logExp=logExp))
}
