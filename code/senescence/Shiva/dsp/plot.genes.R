plot.genes <- function(Data, Target, savePath, colm){
    ## This function generates violin plots of your desired genes/proteins
    ##colm: name of the column of interest for plotting
    print(paste("Plotting top DE of", colm))
    demT <- Target[Target[, "Condition"] %in% "Dementia",]
    nodemT <- Target[Target[, "Condition"] %in% "NoDementia",]
    demDat <- Data[, rownames(demT)]
    nodemDat <- Data[, rownames(nodemT)]
    par(mfrow = c(1,2))
    for(n1 in 1:nrow(Data)){
        g1 <- as.data.frame(demDat[n1,])
        g2 <- as.data.frame(nodemDat[n1,])
        colnames(g1) <- colnames(g2) <- "Expression"
        g1 <- cbind(g1, colm=demT[rownames(g1), colm])
        g2 <- cbind(g2, colm=nodemT[rownames(g2), colm])
        w1 <- wilcox.test(g1$Expression ~ g1[,"colm"])
        w2 <- wilcox.test(g2$Expression ~ g2[,"colm"])
        ##browser()
        rounds <- round(w1$p.value, digits=200)
        v1 <- ggplot(g1, aes(x=g1[, "colm"], y=Expression,
                             fill=g1[, "colm"]))+geom_violin()+
            ggtitle(paste("Dementia,",rownames(demDat)[n1] ,
                          ",\n pv=", scientific(rounds)))
        v1$labels$fill <- "Categories"
        v1 <- v1+ theme(plot.title= element_text(size=15, face="bold"),
                        axis.title.x=element_blank(),
                        axis.text.x=element_text(size=12, face="bold"),
                        axis.title.y = element_text(size=15, face="bold"))

        v2 <- ggplot(g2, aes(x=g2[, "colm"], y=Expression,
                             fill=g2[, "colm"]))+geom_violin()+
            ggtitle(paste("No Dementia,",
                          rownames(demDat)[n1],
                          ",\n pv=", scientific(round(w2$p.value, 200))))
        v2$labels$fill <- "Categories"
        v2 <- v2+ theme(plot.title= element_text(size=15, face="bold"),
                        axis.title.x=element_blank(),
                        axis.text.x=element_text(size=12, face="bold"),
                        axis.title.y = element_text(size=15, face="bold"))

        png.if(doPng=exCon$doPng, pngFile=file.path(savePath,
                      paste0(r1, "_", rownames(demDat)[n1], ".png")),
            res=200, width=10, height=10, units="in")
        grid.arrange(v1, v2, ncol= 2)
        dev.off.if(doDevOff=exCon$doDevOff)
        par(mfrow=c(1,1))
    }
}
