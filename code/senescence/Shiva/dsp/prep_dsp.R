## Shiva wrote this script to analyze new dsp data which includes more
## ROI. Original set of questions: 1) which proteins are
## differentially expressed comparing a vs c?
##2) which proteins are differentially expressed comparing b vs d?

##New questions:1) which proteins are differentially expressed comparing a vs e?
##2) which proteins are differentially expressed comparing c vs e?
##3) which proteins are differentially expressed comparing b vs f?
##4) which proteins are differentially expressed comparing d vs f?
##Shiva modified this script with new excel sheet that Jamie sent
##her. The exression of proteins differ slightly. If you wish to use
##the latest version of expression table set doExperiment to TRUE.
print("Loading csv file...")
if(doExperiment2){
    erccDsp <- data.frame(read_excel(file.path(dataPath,
                                        "DSP_Experiment2_data_10-9-2020_updated.xlsx"),
                                     sheet="ERCC normalized all data"))
    erccDsp[, "Sample.ID"] <- gsub("A4", "1803",
                                                erccDsp[,
                                                        "Sample.ID"])
     erccDsp[, "ROI.ID"] <- gsub("A4", "1803",
                                 erccDsp[, "ROI.ID"])
    erccDsp[, "UniqueID"] <-  paste(erccDsp[, "Sample.ID"],
                                    erccDsp[, "ROI"], erccDsp[,
                                                              "ROI_group"],
                                    sep="_")
    case <- erccDsp[, "Sample.ID"]
    dsp <- subset(erccDsp, select=-c(Experiment, NSTG.ID,
                                     Sample.ID, ROI, ROI_group,
                                     ROI_type, ROI.ID))
    rownames(dsp) <- dsp[, "UniqueID"]
    dsp <- dsp[, !colnames(dsp) %in% "UniqueID"]
}else{
    erccDsp <- read.csv(dspErccFile, stringsAsFactors=FALSE)
    colnames(erccDsp)[which(names(erccDsp) == "ROI_type")] <-
        "ROI_group"
    case <- sub(".*\\s", "", erccDsp[, "Sample.ID"])
    erccDsp[, "Sample.ID"] <- gsub("A4", "1803", erccDsp[,
                                                         "Sample.ID"])
    num <- parse_number(erccDsp$ROI.ID)
    erccDsp[, "Sample.ID"] <- paste(erccDsp[, "Sample.ID"], num,
                                    erccDsp[, "ROI_group"],
                                    sep="_")
    erccDsp[, "Sample.ID"] <- gsub("WF ", "",erccDsp[, "Sample.ID"])
    erccDsp[, "Sample.ID"] <- sub(".+? ", "", erccDsp[, "Sample.ID"])
     ## ^ remove anything before the first space
    case <- cbind(case, ROID=erccDsp[, "Sample.ID"])
    ##erccDsp[, "Sample.ID"] <- sub("^(.{6})", "\\1_", erccDsp[,
                                                       ##  "Sample.ID"])
    dsp <- subset(erccDsp, select=-c(ROI.ID, ROI_group,
                                      HK.geomean, HK.norm.factor,
                                     IgG.geomean))
    rownames(dsp) <- dsp[, "Sample.ID"]
    dsp <- dsp[, !colnames(dsp) %in% "Sample.ID"]
}
print("Dim of ercc normalized data")
print(dim(erccDsp))

case <- data.frame(cbind(case=case, ROI=erccDsp[, "ROI_group"],
                         ROID=erccDsp[, "UniqueID"]))
print("Saving cases information...")
save(case, file=caseFile)
##browser()

##for(n1 in 1:nrow(erccDsp)){
  ##  erccDsp[n1, "Sample.ID"] <- sub("_([^_]*)$",  erccDsp[n1,
    ##                                                      "ROI_group"],
      ##                              erccDsp[n1, "Sample.ID"])
##}
print("Removing columns containing only NA from dsp object:")
dsp <- dsp[colSums(!is.na(dsp))>0]
dsp <- t(dsp)
print(dsp[1:5, 1:5])
save(dsp, file=dspNewRoiFile)
