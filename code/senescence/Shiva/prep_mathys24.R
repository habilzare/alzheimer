#### Shiva wrote this script to prepare the mathys24 dataset which
#### includes snRna-seq data of 48 subjects from regions of EC,
#### hippocampus (HC), anterior thalamus (TH), angular gyrus (AG),
#### midtemporal cortex (MT) and prefrontal cortex (PFC)
## 2024-08-08
log_info("Preparing mathys24 started...")
if(!file.exists(snRnaCountsFile)){
    log_info("Preparing the h5ad must be run in TACC!!")
    dataFile <- file.path(genExPath,
                          "all_brain_regions_filt_preprocessed_scanpy_fullmatrix.h5ad")
    log_info("Reading the h5ad file as anndata object...")
    anndata <- import("anndata")
    adata <- anndata$read_h5ad(dataFile)
    log_info("Dim of adata is (cell*gene):", paste(dim(adata), collapse="*"))

    allGenes <- c()
    for(c1 in colnames(seNftGenes)){
        allGenes <- c(allGenes, seNftGenes[, c1])
    }
    allGenes <- allGenes[allGenes != ""]
    if(!is.null(taskParams$deFile)){
        de <- data.frame(read_excel(file.path(resPath, "Senescence",
                                                  taskParams$deFile)))
        deGenes <- de[ ,"primerid"]
        allGenes <- c(allGenes, deGenes)
    }
    ##browser()
    matchedGeneSn <- intersect(allGenes, colnames(adata))
    log_info("Length of overlap of seNftGenes with adata is: ",
             length(matchedGeneSn))

    adatasub <- adata[, matchedGeneSn]
    sce <- AnnData2SCE(adatasub)
    ##sce <- readH5AD(dataFile, use_hdf5=TRUE) leads to zero expression
    log_info("Dim of sce is:", paste(dim(sce), collapse="*"))
    ##save(snRnaCounts, file=snRnaCountsFile)

    ##meta data
    metaData <- read_tsv(file.path(genExPath,
                                   "all_brain_regions_filt_preprocessed_scanpy_norm.final_noMB.cell_labels.tsv"))
    metaDf <- as.data.frame(metaData)
    log_info("Dim of meta data is:", paste(dim(metaDf), collapse="*"))
    rownames(metaDf) <- metaDf[, "barcode"]
    print(metaDf[1,])

    ##loading all the clinical data
    suppTab1 <- data.frame(read_tsv(file.path(rosPath$paths["clinicalPath"],
                                              "Supplementary_Table_1_sample_metadata.tsv")))
    suppTab1 <- suppTab1[, c("subject", "pathAD")]
    suppTab1 <- suppTab1[!duplicated(suppTab1$subject), ]
    ##save(clinical, file=clinicalFile)
    rosmapClinc <- read.csv(file.path(rosPath$paths["clinicalPath"],
                                      "ROSMAP_clinical.csv"),
                            stringsAsFactors=FALSE)
    indId <- read.csv(file.path(rosPath$paths["clinicalPath"],
                                "MIT_ROSMAP_Multiomics_individual_metadata.csv"),
                      stringsAsFactors=FALSE)
    rosmapClincSub <- rosmapClinc[rosmapClinc[, "projid"] %in%
                                  metaDf[, "projid"],]
    indIdSub <- indId[indId[, "individualID"] %in%
                      rosmapClincSub[,"individualID"],
                      c("individualID", "subject")]
    indIdSub <- indIdSub[!duplicated(indIdSub$individualID), ]
    indClinc <- merge(rosmapClincSub, indIdSub, by='individualID')
    clincsTab1 <- merge(indClinc, suppTab1, by="subject")

    fullMeta <- merge(metaDf, clincsTab1, by="projid")
    rownames(fullMeta) <- fullMeta[, "barcode"]
    colnames(fullMeta)[which(names(fullMeta) == "major.celltype")] <-
        "broad.cell.type"
    if(ncol(sce) != nrow(fullMeta))
        log_info("Number of cells in sce and fullMeta differs!!")

    snRnaCounts <- sce[, rownames(fullMeta)] ##don't like it
    log_info("Dim of new sce is:", paste(dim(snRnaCounts), collapse="*"))
    colData(snRnaCounts) <- cbind(colData(snRnaCounts),
                                  fullMeta[rownames(colData(snRnaCounts)),])
   colnames(colData(snRnaCounts))[which(colnames(colData(snRnaCounts))
    == "obsnames")] <- "TAG"
    log_info("Saving as .RData...")
    save(snRnaCounts, file=snRnaCountsFile)
}else{
    log_info("snRnaCounts already exists. Download skipped.")
}

load(snRnaCountsFile, v=TRUE) ##snRnaCounts
cold <- colData(snRnaCounts)
if(paramsCom$doThalamus){
    log_info("Saving the thalamus only...")
    cold <- cold[cold[, "region"] %in% "TH",]
    snRnaCounts <- snRnaCounts[, rownames(cold)]
    save(snRnaCounts, file=file.path(senPath, "snRnaCouts.RData"))
    }
pathology <- as.character(cold$pathAD)
names(pathology) <- rownames(cold)
##mirStages <- list()
##mirStages[["Pathology"]] <- pathology
##stages <- list(mirStages=mirStages)
##save(stages, file=stageFile)
##browser()
subFullMeta <- cold[, c( "braaksc", "ceradsc", "cogdx",
                            "dcfdx_lv", "region", "apoe_genotype")]
mirStages <- list()
for (colName in names(subFullMeta)) {
    namedVector <- setNames(subFullMeta[[colName]], nm=rownames(subFullMeta))
    mirStages[[colName]] <- namedVector
}
stages <- list(mirStages=mirStages)

age <- cold[, "age_death"]
names(age) <- rownames(cold)
rosId <- cold[, "subject"]
names(rosId) <- rownames(cold)
adPathology <- cold[, "pathAD"]
names(adPathology) <- rownames(cold)
subClin <- list(rosId=rosId, age=age, adPathology=adPathology)

## Save:
save(stages, file=stageFile)
save(subClin, file=subClinFile)



