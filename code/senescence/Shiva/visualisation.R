##Shiva wrote this script after she realized scaling on cells hasn't
##been done.
load(as.character(rosPath$files["snRnaCountsFile"]),
     verbose=TRUE) ##snRnaCounts
cellTypes <- as.character(colData(snRnaCounts)[, "broad.cell.type"])
names(cellTypes) <- as.character(colData(snRnaCounts)[, "TAG"])
print(table(cellTypes))
cn <- t(as.matrix(assay(snRnaCounts)))
rownames(cn) <- as.character(colData(snRnaCounts)[, "TAG"])
rsum <- rowSums(cn)
png(file.path(senPath, "snRnaCounts_cells_sum.png"))
plot(log(sort(rsum)))
dev.off()
cellMean <- rsum/ncol(cn) ##number of reads for each
##cell on averag
totalMean <- sum(rsum)/(nrow(cn)*ncol(cn))
data1 <- cn*totalMean/cellMean
data1[1:4,1:4]

csp <- unique(senGenes[, "Canonical Senescence Pathway"])
csp <- csp[csp != ""]
load("/Users/kazempour/proj/dementia/result/2021-01-29_rush/ROSMAP/senescence/senescent/Canonical\ Senescence\ Pathway/tag2amyloid.group/pop.RData",
     v=T)
sen <- pop[pop[, "Population"] %in% TRUE, ]
png(file.path(senPath, "senCells_raw_400.png"), width=5*480,
            height=3*480, res=200)
pheatmap(cn[rownames(sen), csp][400:800,], show_rownames=FALSE)
dev.off()
png(file.path(senPath, "colsum_allCells_raw.png"), width=5*480,
            height=3*480, res=200)
plot(sort(log10(colSums(cn[, csp]))),
     ylab="Expression of CSP genes over all cells")
text(sort(log10(colSums(cn[, csp]))),
     labels=names(sort(log10(colSums(cn[, csp])))), cex=0.7, pos=1)
dev.off()

p19raw <- cn[, "CDKN2D"]
p19raw <- data.frame(Count=p19raw)
p19raw <- cbind(p19raw, TAG=rownames(p19raw))
popraw <- cbind(pop, TAG=rownames(pop))
rawdat <- merge(p19raw, popraw, by.x="TAG", by.y="TAG")
rawdat <- rawdat[, c("TAG", "Count", "Population", "Cell_type",
                     "Expression")]
neu <- rawdat[rawdat[, "Cell_type"] %in% c("In", "Ex"), "TAG"]
png(file.path(senPath, "colsum_neur.png"), width=5*480,
            height=3*480, res=200)
plot(log10(colSums(cn[neu, csp[o1]])),
     ylab="Expression of CSP genes over neurons")
text(log10(colSums(cn[neu, csp[o1]])), labels=csp[o1], cex=0.7, pos=1)
dev.off()

noNeu <- rawdat[!rawdat[, "Cell_type"] %in% c("In", "Ex"), "TAG"]
png(file.path(senPath, "colsum_noNeur.png"), width=5*480,
            height=3*480, res=200)
plot(log10(colSums(cn[noNeu, csp[o1]])),
     ylab="Expression of CSP genes over non-neuron cells")
text(log10(colSums(cn[noNeu, csp[o1]])), labels=csp[o1], cex=0.7, pos=1)
dev.off()

png(file.path(senPath, "noNeur_vs_neur_raw_numbers.png"), width=5*480,
            height=3*480, res=200)
plot(log10(colSums(cn[noNeu, csp[o1]]>0)/length(noNeu)),
     log10(colSums(cn[neu, csp[o1]]>0)/length(neu)),
     xlab="Expression of CSP genes over non-neuron cells (raw)",
     ylab="Expression of CSP genes over neurons (raw)")
text(log10(colSums(cn[noNeu, csp[o1]]>0)/length(noNeu)),
     log10(colSums(cn[neu, csp[o1]]>0)/length(neu)),
     labels=csp[o1], cex=0.7, pos=1)
abline(b=1, a=0)
dev.off()

png(file.path(senPath, "noNeur_vs_neur_scaled.png"), width=5*480,
            height=3*480, res=200)
plot(log10(colSums(data1[noNeu, csp[o1]])/length(noNeu)),
     log10(colSums(data1[neu, csp[o1]])/length(neu)),
     xlab="Expression of CSP genes over non-neuron cells (scaled)",
     ylab="Expression of CSP genes over neurons (scaled)")
text(log10(colSums(data1[noNeu, csp[o1]])/length(noNeu)),
     log10(colSums(data1[neu, csp[o1]])/length(neu)),
     labels=csp[o1], cex=0.7, pos=1)
abline(b=1, a=0)
dev.off()

load("/Users/kazempour/proj/dementia/result/2021-01-29_rush/ROSMAP/senescence/senescent/Canonical\ Senescence\ Pathway/eigengenes.RData",
     v=T)
png(file.path(senPath, "exp_vs_eigengene.png"), width=5*480,
            height=3*480, res=200)
plot(log10(rowSums(cn[names(e1),])), e1 , pch=".",
     xlab="Total expression of each cell among all genes",
     ylab="eigengene value")
abline(h=mean(e1)+3*sd(e1))
dev.off()




p19scale <- subDat[, "CDKN2D"]
senCell <- rownames(sen)
sc <-
    get(load("/Users/kazempour/proj/dementia/result/2021-02-02_cellScale/ROSMAP/senescence/senescent/Canonical\ Senescence\ Pathway/tag2braaksc/pop.RData",
             v=T))
scSen <- sc[sc[, "Population"] %in% TRUE, ]
plot(p19raw, p19scale, col=ifelse(names(p19raw) %in% senCell, "black",
                                  "red"), pch=ifelse(names(p19raw)
                                                     %in% senCell, 17,
                                                     1))

ggplot(rawdat, aes(x=as.factor(Population), y=Count,
                   fill=as.factor(Population)))+geom_violin()

p19scale <- data.frame(Count=p19scale)
p19scale <- cbind(p19scale, TAG=rownames(p19scale))
popscale <- cbind(sc, TAG=rownames(sc))
scdat <- merge(p19scale, popscale, by.x="TAG", by.y="TAG")
scdat <- scdat[, c("TAG", "Count", "Population", "Cell_type",
                     "Expression")]
ggplot(scdat, aes(x=as.factor(Population), y=Count,
                   fill=as.factor(Population)))+geom_violin()
