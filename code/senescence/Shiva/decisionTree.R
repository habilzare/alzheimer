#' Shiva wrote this function to perform decision tree C5.0 algorithm, 2024-06-15.
#' @param Data A dataframe containing the dataset to be used for
#building the decision tree. Rows must be cells/samples and columns
#are genes/features.
#' @param minCase A vector of integers specifying the minimum number of cases that a node must have to make a split.
#' @param weight Optional; A vector of weights to be applied to each case in the dataset, if non-null.
#' @param labs A vector of class labels corresponding to each row in Data.
#' @param exclude Optional; A vector of gene names to be excluded from the analysis.
#' @param positive Optional; Specifies the factor level of the 'labs' that corresponds to the positive class in binary classification.
#' @param savePath The directory path where the plot of the decision tree will be saved.
#'
#' @return the confusion matrix for each minCase, selected features,
#'     and plot of final decision tree with accuracy, sensitivity, and
#'     percision included.
#'
#' @export
#'

decisionTree <- function(Data, minCase, weight=NULL, labs,
                         exclude=NULL, positive=NULL,
                         savePath, prediction=FALSE){
    trees <- list()
    labs <- as.factor(labs)
    genes <- colnames(Data)
    log_info("The Data has ", length(genes), " genes.")
    if(!is.null(exclude)){
        log_info("Excluding gene ", paste0(exclude, collapse=","))
        genes <- genes[!genes %in% exclude]
        Data <- Data[, genes]
        log_info("Dim of data after removing ", paste0(exclude,
                                                       collapse=",")
               , " is :")
        print(dim(Data))
    }

    ##Define the colors for the tree: red for the first class, grey
    ##for the second
    mycols <- c("#FF0000", "#808080")  ##Red and Grey
    if(!is.null(positive)){
        neg <- levels(labs)[levels(labs) != positive]
        names(mycols) <- c(positive, neg)
    } else{
        names(mycols) <- levels(labs)
        }
    ##treeColsc <- mycols[levels(labs)]

    for(m1 in minCase) {
        print(paste("minCases:", m1))
        minCase <- C5.0Control(minCases=m1)
        if (!is.null(weight)) {
            ##wRatio <- round(max(table(labs))/min(table(labs)))
            tr <- C5.0(as.factor(labs)~., data=data.frame(Data),
                       control=minCase, weights=weight)
            log_info("Weighting distribution is:")
            print(table(weight))
        } else {
            tr <- C5.0(as.factor(labs)~., data=data.frame(Data),
                       control=minCase, weights=NULL)
        }
        pr <- predict(tr, data.frame(Data))

        info <- confusionMatrix(data=as.factor(pr),
                         reference=as.factor(labs), positive=positive)
        print(info)
        confMat <- info$table
        ft <- get.used.features(tr) ##features
        log_info("Selected features:")
        print(ft)
        ## building all useful info in legend
        infoLeg <- c(info$overall["Accuracy"],
                     info$byClass[c("Sensitivity", "Specificity")])
        leg <- c()
        for (i in seq_along(infoLeg)) {
            leg[i] <- paste0(names(infoLeg)[i], ":", round(infoLeg[i], 6))
        }
        ##browser()
        leg2 <- c(paste0("False Pos:", confMat[2,1]),
                  paste0("False Neg:", confMat[1,2]))

        ## Plotting
        if(!is.null(exclude)){
            fileName <- paste0("tree_", m1, "_", paste0(exclude,
                                                   collapse="_"), ".png")
        } else{
            fileName <- paste0("tree_", m1, ".png")
        }
        ##par(mar = c(5.1, 5, 4.1, 2.1))
        png(file.path(savePath, fileName),
            width=4*480, height=4*480, res=150)
        plot.new()
        plot(tr, tp_args=list("fill"=mycols), cex=2)
        legend("topleft",, legend=c(leg, leg2), cex=1.5)
               ##inset=c(-0.1, 0))

        legend("topright", legend=names(mycols), fill= mycols,
               title="Label", cex=1.8, bty="n")
               ##inset = c(-0.1, 0))
        dev.off()

        ##browser()
        resName <- paste0("minCase_", m1)
        trees[[resName]][["tree"]] <- tr
        trees[[resName]][["confusion"]] <- info
        trees[[resName]][["features"]] <- ft

    }
    return(trees)
}

