# README #
This README is for the Alzheimer repository (Version: 1.5). It is written using
[Markdown](https://bitbucket.org/tutorials/markdowndemo).


### What is this repository for? ####

* Quick summary: 
 Publicly available code, documents, and small data for data analysis for several published studies on Alzheimer in the Oncinfo Lab.
* We have tested the code in macOS and it should work fine in a Unix machine. 
 To use the code, we recommend this repository be cloned in `~/proj/`.

### Publications ###
1. All R scripts that we used to analyze RNA-Seq data for the following paper are available in
    the `code/Habil` and `code/Hanie` folders. See the readme files in each folder and the project
    [page](https://oncinfo.org/molecular_mechanisms_ofalzheimers_disease) for more information.

    Sun W., Samimi H., Gamez M., Zare H., Frost B., Pathogenic tau-induced piRNA depletion promotes
    neuronal death through transposable element dysregulation in neurodegenerative tauopathies,
    Nature Neuroscience 2018, Impact factor: 18.
    

2. All R scripts that we used to analyze snRNA-Seq data for the following paper are available in
    the `code/senescence/Shiva` folder. See the readme file in this folder and the project
    [page](https://oncinfo.org/multiomics_analysis_for_dementia) for more information.

    Kazempour S., Walker J., ..., Zare H., Orr M., Profiling senescent cells in human brains
    reveals neurons with CDKN2D/p19 and tau neuropathology,
    Nature Aging 2021.
    
3. All R and python scripts that we used to make our deep ZiPo model, published in the following paper, are available in
    the `code/deep/ZiPo/` folder. See the readme file in this folder for detailed information on how to use this methodology 
    to analyze your data.

    Sharifitabar M., Kazempour S., ..., Zare H., A deep neural network to de-noise single-cell RNA sequencing data,
    IEEE-ACM Transactions on Computational Biology and Bioinformatics, Under review, 2024.
    
### Before you start adding code ###

* Create a directory at code/<your name>.
* Avoid branching unless get authorized by the PI.
* After you run and check your code, when you think it is ready to be used by others, ask
  a lab member to review your code.
* Do not include large data or result files. For large data, explain in a readme.txt file,
  how to download them, e.g. by `wget` command, or write some code to get the data in place.

### How do I get set up? ###

Any code that may be used by others should have a README file. 
These comments may be accumulated in 1 readme file for each folder.

### Who do I talk to? ###

* For data analysis: The Oncinfo PI, Dr. Habil Zare.

License: GPL (>=2)

