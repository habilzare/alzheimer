- On 2018/06/16, Habil moved the quant.sf_NumReads.RData file to Oncinfo. It can be downloaded using the following link:

http://oncinfo.org/file/view/Drosophila_2017-03-03_smallRNA_FROST_B_01052017_mapped_quant.sf_NumReads.RData/630499197/Drosophila_2017-03-03_smallRNA_FROST_B_01052017_mapped_quant.sf_NumReads.RData

Habil removed the files using git filter-branch as instructed below:

https://help.github.com/articles/removing-sensitive-data-from-a-repository/

