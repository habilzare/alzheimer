- Habil downloaded the SraAccList_SRP125768.txt file on 2018/07/06 from:
https://www.ncbi.nlm.nih.gov/sra?term=SRP125768
Top-right, click to "Send To", "File", "Accession List".

He downloaded SraAccList_SRP125768.csv similarly.

- On 2018/07/23, Habil downloaded Aerts_Fly_AdultBrain_Filtered_57k from:
http://scope.aertslab.org/#/d9b4a0b4-81bd-4373-a7b5-4e6c05c71061/Aerts_Fly_AdultBrain_Filtered_57k.loom/gene
