Data should be put in this folder but avoid adding them to the repository unless for small data (<1MB). 
For large data, explain in a readme.txt file, how to download them, e.g. by wget command, or write some code to get the data in place.
