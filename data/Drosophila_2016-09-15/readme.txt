- Habil downloaded Frost_S_07292016.tar.bz2 from  cbbi-sftp.uthscsa.edu on 2016-09-15. He uncompressed it in the RNA folder on the Lonestar5 cluster,
and put a symbolic link here. I.e., 

ln -s /work/03270/zare/lonestar/proj/alzheimer/data/Drosophila_2016-09-15/RNA data/Drosophila_2016-09-15/RNA

- He also uncompressed the fastq.gz files and simplified their names, e.g., CTRL_RNA_1_S21_L006_R1_001.fastq.gz -> CTRL_1_R1.fastq

OBls RNA/*.gz
CTRL_RNA_1_S21_L006_R1_001.fastq.gz	Tau_RNA_1_S24_L006_R1_001.fastq.gz
CTRL_RNA_1_S21_L006_R2_001.fastq.gz	Tau_RNA_1_S24_L006_R2_001.fastq.gz
CTRL_RNA_2_S22_L006_R1_001.fastq.gz	Tau_RNA_2_S25_L006_R1_001.fastq.gz
CTRL_RNA_2_S22_L006_R2_001.fastq.gz	Tau_RNA_2_S25_L006_R2_001.fastq.gz
CTRL_RNA_3_S23_L006_R1_001.fastq.gz	Tau_RNA_3_S26_L006_R1_001.fastq.gz
CTRL_RNA_3_S23_L006_R2_001.fastq.gz	Tau_RNA_3_S26_L006_R2_001.fastq.gz

- Copying from Habil's lano note on 2016/9/22,
I downloaded transcriptome and transposons fasta file from FlyBase to Lonestar. I combined them together and indexed the resulting file:
pwd
/home1/03270/zare/proj/alzheimer/data/Drosophila_2016-09-15/RNA/fasta
wget ftp://flybase.net/genomes/Drosophila_melanogaster/current/fasta/dmel-all-transposon-r6.12.fasta.gz
wget ftp://flybase.net/genomes/Drosophila_melanogaster/current/fasta/dmel-all-transcript-r6.12.fasta.gz

Update 2019-06-13: The now older versions of faster files can still be downloaded from FlyBase.net, i.e., 
ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r6.12_FB2016_04/fasta/

## gunzip and cat, then:
salmon index -t ./dmel-all-transcript-transposon-r6.12.fasta -i transcripts_index --type fmd

Update 2019-06-13: Newer versions of Salmon do not support fmd type, so instead for long reads I must use:
--type quasi -k 31

- Hanie downloaded labels for mRNA from http://flybase.org/static_pages/downloads/ID.html.
The file name is transposableElementsNames.tsv and it is accesible from oncinfo. 
Read ~/proj/alzheimer/code/Hanie/Readme.txt for more details about it.

