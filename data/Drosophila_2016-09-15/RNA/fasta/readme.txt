- Habil manually created prospero_twintron_U12.fasta based on the “Prospero twintron.docx” file that Adrian emailed to him on 17 April 2017. He should add transcripts to this to get some estimates of the total number of mapped reads (coverage) for each sample. Alternatively, the resulting counts can be compared vs. the the counts from the mRNA analysis.
- Similarly, prospero_twintron3.fasta has the 5' end of U12 befor
start of U2, U2, and the 3' end of U12 after the end of U12. But the
3' and 5' ends are too short to be mapped to by reads of length 100.
— 2017-09-20.
