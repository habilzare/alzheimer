These are paired-end RNA-Seq data of Drosophila melanogaster. Whole RNA-seq was done in Bess Frost’s lab on 3 control and 3 tau transgenic samples. Each sample is composed of heads of 6 male and 6 female flies, all 10 days old.

Habil Zare, 2017-08-17.


— Habil got FTbr2location.csv from Ensembl-Biomart AFTER DE analysis. See the project page.  2017-09-12.


- See Hanie’s lano on 2017-08-12,13 on how to prepare the transposableElementsNames.csv file.

- Habil downloaded fb_synonym_fb_2017_06.tsv.gz from FlyBase on 2018/01/25:
wget ftp://ftp.flybase.org/releases/FB2017_06/precomputed_files/synonyms/fb_synonym_fb_2017_06.tsv.gz