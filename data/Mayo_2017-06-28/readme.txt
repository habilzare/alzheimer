Habil copied Taner_U01_Human_RNAseq from old Ranch to the the Genetwork path in new Ranch, i.e.,
/stornext/ranch_01/ranch/projects/Genetwork/proj/alzheimer/data/Mayo_2017-06-28.
It took 2-3 days to copy these ~4 TBs.
2020-09-03.


Mayo clinic human raw bam files were copied in this folder on Ranch file storage. 
See the email with subject "AMP-AD raw files".
--Habil, 2017-07-01.

===
On Mon, Aug 21, 2017 at 11:44 AM, Younkin, Mariet A., Ph.D. <Allen.Mariet@mayo.edu> wrote:
Hi Habil,
… 
The reads are paired end, unstranded. I will have to check with our core as to the direction of the reads.
 
Also, the numbers of files has been changed to:
  204 Taner_U01_Human_CER_RNAseq 
  Including Mayo Clinic Brain Bank samples, excluding Banner Sun Health samples.
  199 Taner_U01_Human_TCX_RNAseq
  403 total
Mariet.


===
On Mon, Aug 21, 2017 at 3:51 PM, Younkin, Mariet A., Ph.D. <Allen.Mariet@mayo.edu> wrote:
Hi Habil,
 
I suggest using Synapse for metadata rather than our sending files.
 
You can find covariate files
CER: https://www.synapse.org/#!Synapse:syn5049298  (syn6126119)
TCX: https://www.synapse.org/#!Synapse:syn3163039 (syn6126114)
 
And any QC considerations and exclusions:
CER: https://www.synapse.org/#!Synapse:syn5049298 (syn5223705)
TCX: https://www.synapse.org/#!Synapse:syn3163039 (syn6126114)
 
These get updated periodically as we work with the data,  so I suggest you check back for any updates.
Mariet.
