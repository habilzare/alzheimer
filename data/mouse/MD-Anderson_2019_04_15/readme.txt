- On Ranch, this folder contains a subfolder named "RNAseq-for-Genevia-v2". These are AD mouse fast files from MD Anderson that William Ray shared with Habil in an email on 2019-04-08 through Box. 

- Habil tried to download these data by FileZilla on 2019-04-15, but download was too slow and he ran out of space on his computer. Then he used the following command to download 562 GBs of data in ~2 hours on his scratch folder on Lonestar: 

mirror -c --parallel=16 RNAseq\ for\ Genevia\ v2/

For more details, see:
https://www.cyberciti.biz/faq/lftp-mirror-example/

- On 2019-04-17, Habil removed the spaces in the folder name. He will copy the folder to Ranch using:

- On 2019-05-13, Habil uncompressed the files (symbolic links) in the tau folder using:

gunzip -kf *.fastq.gz

See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=916135
