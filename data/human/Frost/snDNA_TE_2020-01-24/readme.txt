- Shiva created ls.txt which contains the name of all fastq files. She moved the Frost_B_12182019 to ranch under ~/zare/proj on 2020-08-27.

- Shiva downloaded hg19.chrom.sizes.txt. It contains the size of each chromosome based on hg19. It can be downloaded using:
wget https://hgdownload.cse.ucsc.edu/goldenPath/hg19/bigZips/hg19.chrom.sizes

- Shiva downloaded repeatMasker.bed from UCSC.To download the repetitive regions use the link below:
https://genome.ucsc.edu/cgi-bin/hgTables?hgsid=847696735_PTxr10PTaISZuc8LCKUvm6TmlWAO&clade=mammal&org=Human&db=hg19&hgta_group=rep&hgta_track=ct_L5_737&hgta_table=0&hgta_regionType=genome&position=chr22%3A1-51%2C304%2C566&hgta_outputType=primaryTable&hgta_outFileName=

- Wenyan sent WGA_Sample_Submission_Form_96_Samples-Wenyan-Lai_Zhao.xlsx on 2020-03-19 and Shiva converted it to csv file.

- Shiva downloaded TE_list_from_human_RNA-seq data-Wenyan.xlsx on 2020-03-16 and saved the third column (TE list) in TE_list_from_human_RNA-seq data-Wenyan_2020-03-16.csv . She changed "PRIMA4_LTR (=PRIMA4_I)" cell with "PRIMA4_LTR".


- From Dr. Lai's email regarding fastq files on 05/02/2020:
Each sample will have 4 separate files. We pooled 48 samples together for 2 lanes of sequencing,  then another 48 samples together for 2 lanes of sequencing. Since it’s paired end, then you will see R1 and R2 two files. 2 files (R1 and R2) X 2 (lanes) = 4 files. 

- Shiva downloaded FastQ.pptx from Dr. Lai's email on 05/02/2020. From her email:
@J00167:249:HFVVWBBXY:7:1101:7811:1209 1:N:0:NTTCCT. NTTCCT is the sample barcode. 

-From Dr. Lai's email to Habil on 04/02/2020:
The 150bp framgent length you received for each reads is:

human DNA (with or without TE sequence) + (possible adapter, if the read length < 150bp , it will hit to adapter region).

- From Dr. Lai's email to Shiva on 02/03/2020: 
Barcode is the same as Illumina TruSeq. 

TruSeq Universal Adapter:
5 AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT 3
TruSeq Indexed Adapter
5 GATCGGAAGAGCACACGTCTGAACTCCAGTCAC‐NNNNNN‐ATCTCGTATGCCGTCTTCTGCTTG 3


-Shiva copied the zipped fastq files from the hard drive that Dr. Lai gave her on 01/26/2020.


-Dr. Lai sent Frost_TargetedCap_150PE_Stats.xlsx to Shiva and Habil. Shiva downloaded and converted it to csv file on 01/27/2020. Column of "name3" shows the cell barcode.

