- Habil downloaded these bam files on 2018/04/24 from Box through the following link:

https://wustl.app.box.com/s/12id1m3qxcm05tuj4rjfo8wr8vp0rcj1

The files with the mid-fix F11362.1d1B6 correspond to MAPT WT/WT (CRISPR-corrected isogenic corrected, controls).
The files with the mid-fix F11362.1d1F10 correspond to MAPT R406W/WT (iPSCs from patients with tauopathy).

- See emails with the titles "JAMA Neuro paper" and "iPSCs from patients with tauopathy - TE analysis".

On 2018/05/17, Habil copied each of the 6 bam files to the Lonestar5 cluster using a command like the following:
rsync -vhz --partial --inplace -e ssh /Users/habil/proj/alzheimer/data/human/iPSC-derived-neurons_2018-04-24/H_VY-F11362.1d1B6_Neuron_1_S1512500_VII.O.45.bam zare@ls5.tacc.utexas.edu:/work/03270/zare/lonestar/proj/alzheimer/data/human/iPSC-derived-neurons_2018-04-24/

- On 2021-12-11, Habil removed these files from his external backup storage because they are already on the Ranch.