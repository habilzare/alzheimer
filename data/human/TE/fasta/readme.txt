- Shiva downloaded the newest version of TE sequences from repbase on 2020-01-29 as text file and saved it manually as te_repbase_extract_2020-01-30.fasta . Documentation on how to download the sequences:

https://oncinfo.org/_media/screen_shot_2020-01-29_at_4.56.25_pm.png?t=1580338661&w=500&h=349&tok=e5623c

- Use the following command to make index for this file:
 bwa index te_repbase_extract_2020-01-30.fasta

