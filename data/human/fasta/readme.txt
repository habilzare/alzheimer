- Shiva downloaded human reference genome (hg19) on 2020-01-28 in the same folder (~/alzhemeir/data/human/fasta) using wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/bigZips/hg19.fa.gz .

- Use the following command to make index for this file:
 bwa index hg19.fa