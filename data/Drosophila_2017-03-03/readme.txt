-These are small RNa-Seq data.

-Hanie downloaded the tRNA fasta file on 2017-07-26 to ~/proj/alzheimer/data/Drosophila_2017-03-03/smallRNA/FROST_B_01052017/fasta/ using :
wget ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/current/fasta/dmel-all-tRNA-r6.16.fasta.gz -O ~/proj/alzheimer/data/Drosophila_2017-03-03/smallRNA/FROST_B_01052017/fasta/dmel-all-tRNA-r6.16.fasta.gz
and then unzipped using : gunzip dmel-all-tRNA-r6.16.fasta.gz


- Habil copied the fastq files from the dropbox folder in Zhao's email to the work directory on the Lonestar5 cluster. He created a symbolic link to them from his home directory.

scp FROST_B_01052017.tar zare@ls5.tacc.utexas.edu:/work/03270/zare/lonestar/proj/alzheimer/data/Drosophila_2017-03-03/smallRNA

- Habil put a symbolic link as follows:
login1.ls5(58)$ ls -lrt data/Drosophila_2017-03-03/
lrwxrwxrwx 1 zare G-816158  77 Mar  5 08:53 smallRNA -> /work/03270/zare/lonestar/proj/alzheimer/data/Drosophila_2017-03-03/smallRNA/
...

- Habil also uncompressed the fastq.gz files and simplified their names, e.g., Tau_4_S60_L007_R1_001.fastq -> Tau_4_R1.fastq
Before simplifying:
login1.ls5(63)$ ls
Con_1_S53_L007_R1_001.fastq  Tau_1_S57_L007_R1_001.fastq
Con_2_S54_L007_R1_001.fastq  Tau_2_S58_L007_R1_001.fastq
Con_3_S55_L007_R1_001.fastq  Tau_3_S59_L007_R1_001.fastq
Con_4_S56_L007_R1_001.fastq  Tau_4_S60_L007_R1_001.fastq

- Habil downloaded the following files on 2017-03-09 to 
~/proj/alzheimer/data/Drosophila_2017-03-03/smallRNA/FROST_B_01052017/fasta/ on the Lonestar cluster:

wget ftp://flybase.net/genomes/Drosophila_melanogaster/current/fasta/dmel-all-miRNA-r6.14.fasta.gz

wget http://pirnabank.ibab.ac.in/downloads/all/drosophila_all.zip
 
and combined them together:

cat dmel-all-miRNA-r6.14.fasta drosophila_pir.fasta > dmel-all-miRNA-r6.14_pir.fasta

where drosophila_pir.fasta is produced usinbg code/Hanie/drosophila_pir.R.

- Zhao sent the following (second) email on 2017-03-14:

“Small RNAseq data will be trimmed before the alignment. We used NEB small RNAkit for your samples. Here is the information for NEB 3' adapter trimming:

3' adapter: 
AGATCGGAAGAGCACACGTCT

You will ask your bioinformatician to trim the sequencing reads from 3' side with this adapter sequence then the trimmed data can be moved forward for the data analysis.

Please let me know if you have any questions. 

NEB protocol aslo provides the adapter information for trimming purpose. I attached the protocol here for your reference.”

The protocol is at doc/manualE7580.pdf.

- Habil downloaded ./dmel-all-transposon-r6.14.fasta from FlyBase on 2017-04-03:
wget ftp://flybase.net/genomes/Drosophila_melanogaster/current/fasta/dmel-all-transposon-r6.14.fasta.gz -o ~/proj/alzheimer/data/Drosophila_2017-03-03/smallRNA/FROST_B_01052017/fasta/dmel-all-transposon-r6.14.fasta

- Habil downloaded the repeatm_si_abundant5000.fa from Oncinfo on 2017/07/18 as follows:
 wget http://oncinfo.org/file/view/repeatm_si_abundant5000.fa/615789805/repeatm_si_abundant5000.fa -O ~/proj/alzheimer/data/Drosophila_2017-03-03/smallRNA/FROST_B_01052017/fasta/repeatm_si_abundant5000.fa
